module.exports = {
  projects: [
    '<rootDir>/apps/frontend',
    '<rootDir>/apps/backend',
    '<rootDir>/libs/shared/util/graphql-typings',
    '<rootDir>/libs/ui',
  ],
}
