import { ChangeEvent } from 'react';
import {
  SelectProps as MuiSelectProp,
  InputProps as MuiInputProps,
  FormControlProps as MuiFormControlProps,
  InputLabelProps as MuiInputLabelProps,
  OutlinedInputProps,
  FormHelperTextProps as MuiFormHelperTextProps,
} from '@mui/material';

export interface ITopicTreeNode {
  children: ITopicTreeNode[];
  id: string;
  title: string;
  href: string;
  hasContentChild: boolean;
}

export interface ITopicOptionsInfo {
  options: ITopicTreeNode[];
  isLoading: boolean;
  isSuccess: boolean;
}

export interface SelectProps extends MuiSelectProp {
  helperText?: string;
  FormControlProps?: Partial<MuiFormControlProps>;
  InputLabelProps?: Partial<MuiInputLabelProps>;
  InputProps?:
    | (Partial<OutlinedInputProps> & {
        onChange: (e: ChangeEvent<HTMLInputElement>) => void;
      })
    | (object & Partial<MuiInputProps>);
  SelectProps?: object | Partial<MuiSelectProp>;
  FormHelperTextProps?: Partial<MuiFormHelperTextProps>;
  color?: 'primary' | 'secondary';
  size?: 'medium' | 'small';
  paperMaxHeight?: number | string;
  error?: boolean;
  hasLabel?: boolean;
  hasShrink?: boolean;
  disabled?: boolean;
  className?: string;
  // eslint-disable-next-line
  otherProps?: any;
}
