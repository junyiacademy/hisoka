/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { createTheme } from '@mui/material/styles';

declare module '@mui/material/styles/createPalette' {
  interface Palette {
    green: {
      primary: Palette['primary'];
      blueGreen: Palette['primary'];
      grassGreen: Palette['primary'];
    };
  }
  interface PaletteOptions {
    green: {
      primary: PaletteOptions['primary'];
      blueGreen: PaletteOptions['primary'];
      grassGreen: PaletteOptions['primary'];
    };
  }
}

// Create a theme instance.
const theme = createTheme({
  typography: {
    fontFamily: ['Noto Sans TC', 'Helvetica', 'Arial', 'sans-serif'].join(','),
  },
  palette: {
    primary: {
      light: '#82C0FF',
      main: '#4990E2',
      dark: '#0063B0',
    },
    secondary: {
      light: '#FFD759',
      main: '#F5A623',
      dark: '#BD7700',
      contrastText: '#FFFFFF',
    },
    green: {
      primary: {
        light: '#9DD49E',
        main: '#5CB85D',
        dark: '#218838',
      },
      blueGreen: {
        main: '#19A696',
      },
      grassGreen: {
        main: '#80BB5A',
      },
    },
  },
});

export default theme;
