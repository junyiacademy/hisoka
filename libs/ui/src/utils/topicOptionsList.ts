import { ITopicOptionsInfo } from '../interfaces';

export const topicOptionsList: ITopicOptionsInfo[] = [
  {
    options: [
      {
        id: 'course-compare',
        children: [
          {
            id: 'math-grade-1-a',
            children: [],
            href: '/course-compare/math-grade-1-a',
            title: '數學 小一',
            hasContentChild: false,
          },
          {
            id: 'math-grade-2-a',
            children: [],
            href: '/course-compare/math-grade-2-a',
            title: '數學 小二',
            hasContentChild: false,
          },
        ],
        href: '/course-compare',
        title: '數學',
        hasContentChild: false,
      },
    ],
    isLoading: false,
    isSuccess: true,
  },
  {
    options: [
      {
        children: [
          {
            children: [],
            href: '/course-compare/math-grade-1-a/v366-new-topic',
            title: '第一單元',
            id: 'v366-new-topic',
            hasContentChild: false,
          },
        ],
        href: '/course-compare/math-grade-1-a',
        title: '數學 小一',
        id: 'math-grade-1-a',
        hasContentChild: false,
      },
    ],
    isLoading: false,
    isSuccess: true,
  },
  {
    options: [
      {
        id: 'g03-menzs3b',
        children: [
          {
            id: 'j-m3ach1-1',
            children: [],
            title: '1-1 認識 10000 以內的數',
            href: '/course-compare/math-grade-3-a/g03-menzs3b/j-m3ach1-1',
            hasContentChild: false,
          },
          {
            id: 'new-topic-856',
            children: [],
            title: '數的大小比較',
            href: '/course-compare/math-grade-3-a/g03-menzs3b/new-topic-856',
            hasContentChild: false,
          },
        ],
        title: '第 1 單元：數到 10000',
        href: '/course-compare/math-grade-3-a/g03-menzs3b',
        hasContentChild: false,
      },
    ],
    isLoading: false,
    isSuccess: true,
  },
  {
    options: [
      {
        id: 'j-m3ach1-1',
        href: '/course-compare/math-grade-3-a/g03-menzs3b/j-m3ach1-1',
        children: [],
        title: '1-1 認識 10000 以內的數',
        hasContentChild: true,
      },
    ],
    isLoading: false,
    isSuccess: true,
  },
];
