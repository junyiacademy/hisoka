import React from 'react';
import { Theme, styled } from '@mui/material/styles';
import {
  InputLabel,
  FormControl,
  Select,
  OutlinedInput,
  FormHelperText,
  InputLabelProps as MuiInputLabelProps,
  OutlinedInputProps as MuiOutlinedInputProps,
} from '@mui/material';
import { formHelperTextClasses } from '@mui/material/FormHelperText';
import { inputLabelClasses } from '@mui/material/InputLabel';
import { outlinedInputClasses } from '@mui/material/OutlinedInput';
import { inputBaseClasses } from '@mui/material/InputBase';
import { SelectProps as ISelectProps } from '../../interfaces';

// self-defined-components
interface StyledInputLabelProps extends Partial<MuiInputLabelProps> {
  theme?: Theme;
}

const StyledInputLabel = styled(InputLabel, {
  shouldForwardProp: (prop: string): boolean => prop !== 'color',
})(({ color, theme }: StyledInputLabelProps) => ({
  color: theme.palette.text.secondary,
  [`&.${inputLabelClasses.outlined}`]: {
    [`&:not(.${inputLabelClasses.disabled}) .${inputLabelClasses.focused}`]: {
      color: theme.palette.action.active,
    },
    [`&.${inputLabelClasses.sizeSmall}:not(.${inputLabelClasses.shrink})`]: {
      transform: 'translate(12px, 12px) scale(1)',
    },
  },
  // eslint-disable-next-line
  [`&.${inputLabelClasses.shrink}:not(.${inputLabelClasses.error}):not(.${inputLabelClasses.disabled}).${inputLabelClasses.focused}`]: {
    color: theme.palette[color].main,
  },
  [`&.${inputLabelClasses.shrink}`]: {
    backgroundColor: '#ffffff',
    padding: '0 2px',
  },
}));

interface StyledOutlinedInputProps extends Partial<MuiOutlinedInputProps> {
  theme?: Theme;
}

const StyledOutlinedInput = styled(OutlinedInput)(
  ({ theme }: StyledOutlinedInputProps) => ({
    [`&.${outlinedInputClasses.root}`]: {
      [`&.${outlinedInputClasses.error}.${outlinedInputClasses.focused} .${outlinedInputClasses.notchedOutline}`]: {
        borderColor: `${theme.palette.error.main}`,
      },
      [`& .${inputBaseClasses.inputSizeSmall}`]: {
        padding: '12.5px 15px 12.5px 12px',
      },
    },
    [`& .${outlinedInputClasses.input}`]: {
      color: theme.palette.text.primary,
      '&:focus': {
        background: 'rgba(0,0,0,0)',
      },
    },
  })
);

export const OutlinedSelect = ({
  label,
  helperText,
  FormControlProps = {},
  InputLabelProps = {},
  InputProps = {},
  SelectProps = {},
  FormHelperTextProps = {},
  className,
  children,
  color = 'primary',
  size = 'small',
  paperMaxHeight = 'auto',
  error = false,
  hasLabel = true,
  hasShrink = false,
  value = '',
  disabled = false,
}: ISelectProps) => {
  const hasHelperText = !!helperText;
  const { sx: formControlSx = [], ...otherFormControlProps } = FormControlProps;
  const {
    sx: formHelperTextSx = [],
    ...otherFormHelperTextProps
  } = FormHelperTextProps;

  return (
    <FormControl
      sx={[
        (theme) => ({
          backgroundColor: 'white',
          '&:hover': {
            // eslint-disable-next-line
            [`& :not(.${outlinedInputClasses.disabled}):not(.${outlinedInputClasses.error}) .${outlinedInputClasses.notchedOutline}`]: {
              borderColor: theme.palette[color].main,
            },
          },
        }),
        ...(Array.isArray(formControlSx) ? formControlSx : [formControlSx]),
      ]}
      size={size}
      disabled={disabled}
      error={error}
      color={color}
      className={className}
      {...otherFormControlProps}
    >
      {hasLabel && (
        <StyledInputLabel
          color={color}
          shrink={hasShrink ? true : undefined}
          {...InputLabelProps}
        >
          {label}
        </StyledInputLabel>
      )}
      <Select
        value={value}
        label={hasLabel ? label : undefined}
        MenuProps={{
          PaperProps: {
            sx: {
              maxHeight: paperMaxHeight,
            },
          },
          disableAutoFocusItem: true,
          anchorOrigin: {
            vertical: 2,
            horizontal: 'left',
          },
          transformOrigin: {
            vertical: 'top',
            horizontal: 'left',
          },
        }}
        input={
          <StyledOutlinedInput
            color={color}
            label={hasLabel ? label : undefined}
            disabled={disabled}
            {...InputProps}
          />
        }
        {...SelectProps}
      >
        {children}
      </Select>
      {hasHelperText && (
        <FormHelperText
          sx={{
            [`&.${formHelperTextClasses.root}`]: {
              marginLeft: 0,
            },
            ...(Array.isArray(formHelperTextSx)
              ? formHelperTextSx
              : [formHelperTextSx]),
          }}
          {...otherFormHelperTextProps}
        >
          {helperText}
        </FormHelperText>
      )}
    </FormControl>
  );
};

export default OutlinedSelect;
