import React, { useState } from 'react';
import { Story, Meta } from '@storybook/react';
import { Theme, styled } from '@mui/material/styles';
import { InputAdornment } from '@mui/material';
import { inputAdornmentClasses } from '@mui/material/InputAdornment';
import { Visibility } from '@mui/icons-material';
import { Select } from './Select';
import SelectMenuItem from '../menu-item/SelectMenuItem';
import { SelectProps } from '../../interfaces';

export default {
  component: Select,
  title: 'Select',
  argTypes: {
    color: {
      description:
        'The color of the component. It supports those theme colors that make sense for this component.',
      table: {
        defaultValue: { summary: 'primary' },
      },
    },
    variant: {
      type: { name: 'string', required: false },
      description: 'The variant to use.',
      table: {
        type: { summary: 'standard | outlined' },
        defaultValue: { summary: 'standard' },
      },
      options: ['standard', 'outlined'],
      control: { type: 'radio' },
    },
    size: {
      type: { name: 'string', required: false },
      description: `Adjust size`,
      table: {
        type: { summary: 'medium | small' },
        defaultValue: { summary: 'small' },
      },
      options: ['small', 'medium'],
      control: { type: 'radio' },
    },
    paperMaxHeight: {
      type: { name: 'number | string', required: false },
      description: `Adjust select menu paper max height.`,
      table: {
        type: { summary: 'number | string' },
        defaultValue: { summary: 'auto' },
      },
      control: { type: 'number' },
    },
    hasLabel: {
      type: { name: 'boolean', required: false },
      description:
        'If true, the label is displayed. Always true on StandardSelect.',
      table: {
        type: { summary: 'boolean' },
        defaultValue: { summary: true },
      },
      control: { type: 'boolean' },
    },
    hasShrink: {
      type: { name: 'boolean', required: false },
      description: 'If true, the label is displayed and shrunk.',
      table: {
        type: { summary: 'boolean' },
        defaultValue: { summary: false },
      },
      control: { type: 'boolean' },
    },
    label: {
      type: { name: 'string', required: true },
      description: `The label title`,
      table: {
        type: { summary: 'string' },
        defaultValue: { summary: '請選擇' },
      },
      control: { type: 'text' },
    },
    value: {
      type: { name: 'any', required: false },
      description: `The input value. Providing an empty string will select no options. This prop is required when the native prop is false (default). Set to an empty string '' if you don't want any of the available options to be selected.
      If the value is an object it must have reference equality with the option in order to be selected. If the value is not an object, the string representation must match with the string representation of the option in order to be selected.`,
      table: {
        type: { summary: 'any' },
        defaultValue: { summary: '' },
      },
    },
    disabled: {
      type: { name: 'boolean', required: false },
      description: 'If true, the input element will be disabled.',
      table: {
        type: { summary: 'boolean' },
        defaultValue: { summary: false },
      },
      control: { type: 'boolean' },
    },
    error: {
      type: { name: 'boolean', required: false },
      description: 'If true, the label will be displayed in an error state.',
      table: {
        type: { summary: 'boolean' },
        defaultValue: { summary: false },
      },
      control: { type: 'boolean' },
    },
    helperText: {
      type: { name: 'string', required: true },
      description: `Display the helper text.`,
      table: {
        type: { summary: 'string' },
        defaultValue: { summary: '' },
      },
      control: { type: 'text' },
    },
    FormControlProps: {
      type: { name: 'any', required: false },
      description: 'Attributes applied to inner FormControl element.',
      table: {
        type: { summary: 'any' },
      },
    },
    InputLabelProps: {
      type: { name: 'any', required: false },
      description: 'Attributes applied to inner InputLabel element.',
      table: {
        type: { summary: 'any' },
      },
    },
    SelectProps: {
      type: { name: 'any', required: false },
      description: 'Attributes applied to inner Select element.',
      table: {
        type: { summary: 'any' },
      },
    },
    InputProps: {
      type: { name: 'any', required: false },
      description:
        'Attributes applied to to inner OutlinedInput element on OutlinedSelect or Input element on StandardSelect.',
      table: {
        type: { summary: 'any' },
      },
    },
    FormHelperTextProps: {
      type: { name: 'any', required: false },
      description: 'Attributes applied to inner FormHelperText element.',
      table: {
        type: { summary: 'any' },
      },
    },
  },
} as Meta;

const LABEL = 'Please select an option';
const ERROR_LABEL = 'Please select an option';

interface StyledInputAdornmentProps {
  disabled: boolean;
  theme?: Theme;
}

const StyledInputAdornment = styled(InputAdornment, {
  shouldForwardProp: (prop: string): boolean => prop !== 'disabled',
})(({ disabled, theme }: StyledInputAdornmentProps) => ({
  height: '100%',
  [`&.${inputAdornmentClasses.root}`]: {
    color: disabled
      ? theme.palette.action.disabled
      : theme.palette.action.active,
  },
}));

// Standard Select start with here.
const SelectWithMenu = (props: SelectProps) => {
  const [item, setItem] = useState<string>('');

  const handleChange = (event) => {
    setItem(event.target.value);
  };

  return (
    <Select
      FormControlProps={{ sx: { width: 330 } }}
      value={item}
      SelectProps={{
        onChange: (e) => {
          handleChange(e);
        },
      }}
      {...props}
    >
      <SelectMenuItem value="" disabled>
        {LABEL}
      </SelectMenuItem>
      <SelectMenuItem value="Option1">
        This is a select menu item
      </SelectMenuItem>
      <SelectMenuItem value="Option2">This is option 2</SelectMenuItem>
    </Select>
  );
};

const ValueOnlyStory: Story<SelectProps> = (args) => (
  <SelectWithMenu {...args} />
);

export const ValueOnly = ValueOnlyStory.bind({});

ValueOnly.args = {
  color: 'primary',
  variant: 'standard',
  size: 'small',
  paperMaxHeight: 300,
  hasShrink: false,
  label: LABEL,
  helperText: 'Pick your choice',
  disabled: false,
};

const SelectWithError = (props: SelectProps) => {
  const [item, setItem] = useState<string>('');

  const handleChange = (event) => {
    setItem(event.target.value);
  };

  return (
    <Select
      FormControlProps={{ sx: { width: 330 } }}
      value={item}
      SelectProps={{
        onChange: (e) => {
          handleChange(e);
        },
      }}
      error={item === ''}
      helperText={item === '' ? ERROR_LABEL : ''}
      {...props}
    >
      <SelectMenuItem value="" disabled>
        {LABEL}
      </SelectMenuItem>
      <SelectMenuItem value={'Test'}>
        This is a select menu item, This is a select menu item
      </SelectMenuItem>
      <SelectMenuItem value={'Example'}>Example</SelectMenuItem>
    </Select>
  );
};

const WithErrorStory: Story<SelectProps> = (args) => (
  <SelectWithError {...args} />
);

export const WithError = WithErrorStory.bind({});

WithError.args = {
  color: 'primary',
  variant: 'standard',
  size: 'small',
  paperMaxHeight: 300,
  hasShrink: false,
  label: LABEL,
  disabled: false,
};

const SelectWithPrefix = (props: SelectProps) => {
  const [item, setItem] = useState<string>('');

  const handleChange = (event) => {
    setItem(event.target.value);
  };

  return (
    <Select
      FormControlProps={{ sx: { width: 330 } }}
      value={item}
      SelectProps={{
        onChange: (e) => {
          handleChange(e);
        },
      }}
      InputProps={{
        startAdornment: (
          <InputAdornment
            sx={(theme) => ({
              height: '100%',
              [`&.${inputAdornmentClasses.root}`]: {
                color: props.disabled
                  ? theme.palette.action.disabled
                  : theme.palette.action.active,
              },
            })}
            position="start"
          >
            <Visibility />
          </InputAdornment>
        ),
      }}
      {...props}
    >
      <SelectMenuItem value="" disabled>
        {LABEL}
      </SelectMenuItem>
      <SelectMenuItem value="Option1">
        This is a select menu item
      </SelectMenuItem>
      <SelectMenuItem value="Option2">This is option 2</SelectMenuItem>
    </Select>
  );
};

const WithPrefixStory: Story<SelectProps> = (args) => (
  <SelectWithPrefix {...args} />
);

export const WithPrefix = WithPrefixStory.bind({});

WithPrefix.args = {
  color: 'primary',
  variant: 'standard',
  size: 'small',
  paperMaxHeight: 300,
  hasShrink: false,
  label: LABEL,
  helperText: 'Pick your choice',
  disabled: false,
};
