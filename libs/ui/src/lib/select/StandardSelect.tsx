import React from 'react';
import { Theme, styled } from '@mui/material/styles';
import {
  InputLabel,
  FormControl,
  Select,
  Input,
  FormHelperText,
  InputLabelProps as MuiInputLabelProps,
  InputProps as MuiInputProps,
} from '@mui/material';
import { inputLabelClasses } from '@mui/material/InputLabel';
import { inputClasses } from '@mui/material/Input';
import { SelectProps as ISelectProps } from '../../interfaces';

// self-defined-components
interface StyledInputLabelProps extends Partial<MuiInputLabelProps> {
  theme?: Theme;
}

const StyledInputLabel = styled(InputLabel, {
  shouldForwardProp: (prop: string): boolean => prop !== 'color',
})(({ color, theme }: StyledInputLabelProps) => ({
  [`&.${inputLabelClasses.root}`]: {
    color: theme.palette.text.disabled,
  },
  [`&.${inputLabelClasses.focused}`]: {
    color: theme.palette[color].main,
  },
  [`&.${inputLabelClasses.error}`]: {
    color: theme.palette.error.main,
  },
}));

interface StyledInputProps extends Partial<MuiInputProps> {
  theme: Theme;
}

const StyledInput = styled(Input, {
  shouldForwardProp: (prop: string): boolean => prop !== 'color',
})(({ color, theme }: StyledInputProps) => ({
  color: theme.palette.text.primary,
  [`& .${inputClasses.input}`]: {
    '&:focus': {
      background: 'rgba(0,0,0,0)',
    },
  },
  [`&.${inputClasses.underline}:not(.${inputClasses.disabled}):not(.${inputClasses.error})`]: {
    '&:after,&:hover:before': {
      borderBottomColor: theme.palette[color].main,
    },
  },
  [`&.${inputClasses.underline}.${inputClasses.error}:not(.${inputClasses.disabled})`]: {
    '&:after,&:hover:before': {
      borderBottomColor: theme.palette.error.main,
    },
  },
}));

export function StandardSelect({
  label,
  helperText,
  FormControlProps = {},
  InputLabelProps = {},
  InputProps = {},
  SelectProps = {},
  FormHelperTextProps = {},
  className,
  children,
  color = 'primary',
  size = 'small',
  paperMaxHeight = 'auto',
  error = false,
  hasShrink = false,
  value = '',
  disabled = false,
}: ISelectProps) {
  const hasHelperText = !!helperText;

  return (
    <FormControl
      variant="standard"
      color={color}
      size={size}
      disabled={disabled}
      error={error}
      className={className}
      {...FormControlProps}
    >
      <StyledInputLabel
        color={color}
        shrink={hasShrink ? true : undefined}
        {...InputLabelProps}
      >
        {label}
      </StyledInputLabel>
      <Select
        label={label}
        value={value}
        MenuProps={{
          PaperProps: {
            sx: {
              maxHeight: paperMaxHeight,
            },
          },
          anchorOrigin: {
            vertical: 2,
            horizontal: 'left',
          },
          transformOrigin: {
            vertical: 'top',
            horizontal: 'left',
          },
        }}
        input={<StyledInput color={color} {...InputProps} />}
        {...SelectProps}
      >
        {children}
      </Select>
      {hasHelperText && (
        <FormHelperText {...FormHelperTextProps}>{helperText}</FormHelperText>
      )}
    </FormControl>
  );
}

export default StandardSelect;
