import React from 'react';
import { OutlinedSelect } from './OutlinedSelect';
import { StandardSelect } from './StandardSelect';
import { SelectProps } from '../../interfaces';

export function Select({ variant, ...props }: SelectProps) {
  if (variant === 'outlined') {
    return <OutlinedSelect {...props} />;
  }
  return <StandardSelect {...props} />;
}

export default Select;
