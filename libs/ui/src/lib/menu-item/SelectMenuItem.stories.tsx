import React from 'react';
import { Story, Meta } from '@storybook/react';
import { Menu } from '@mui/material';
import SelectMenuItem, { SelectMenuItemProps } from './SelectMenuItem';

export default {
  component: SelectMenuItem,
  title: 'SelectMenuItem',
  argTypes: {
    value: {
      type: { name: 'any', required: false },
      description: `The input value. Providing an empty string will select no options. This prop is required when the native prop is false (default). Set to an empty string '' if you don't want any of the available options to be selected.
      If the value is an object it must have reference equality with the option in order to be selected. If the value is not an object, the string representation must match with the string representation of the option in order to be selected.`,
      table: {
        type: { summary: 'any' },
        defaultValue: { summary: '' },
      },
    },
  },
} as Meta;

const Template: Story<SelectMenuItemProps> = (args) => (
  <Menu open={true}>
    <SelectMenuItem {...args}>Test</SelectMenuItem>
    <SelectMenuItem {...args}>Example</SelectMenuItem>
  </Menu>
);

export const Primary = Template.bind({});

Primary.args = {
  value: '',
};
