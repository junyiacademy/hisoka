import React from 'react';
import { MenuItem, MenuItemProps } from '@mui/material';
import { menuItemClasses } from '@mui/material/MenuItem';

// self-defined-components

export interface SelectMenuItemProps extends MenuItemProps {
  // eslint-disable-next-line
  value?: any;
}

const SelectMenuItem = ({
  children,
  value = '',
  ...otherProps
}: SelectMenuItemProps) => {
  const { sx: menuItemSx, ...otherMenuItemProps } = otherProps;
  return (
    <MenuItem
      sx={[
        (theme) => ({
          whiteSpace: 'unset',
          color: theme.palette.text.primary,
          [`&.${menuItemClasses.selected}`]: {
            backgroundColor: theme.palette.grey[300],
            '&:hover': {
              backgroundColor: theme.palette.grey[200],
            },
          },
        }),
        ...(Array.isArray(menuItemSx) ? menuItemSx : [menuItemSx]),
      ]}
      value={value}
      {...otherMenuItemProps}
    >
      {children}
    </MenuItem>
  );
};

export default SelectMenuItem;
