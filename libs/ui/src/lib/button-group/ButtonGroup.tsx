import React from 'react';
import { styled, alpha } from '@mui/material/styles';
import {
  ButtonGroup as MuiButtonGroup,
  ButtonGroupProps as MuiButtonGroupProps,
} from '@mui/material';
import { buttonGroupClasses } from '@mui/material/ButtonGroup';

// utils
import capitalize from '../../utils/Capitalize';

// self-defined-components
const StyledButtonGroup = styled(MuiButtonGroup)(
  ({ color = 'primary', theme }) => ({
    [`& .${buttonGroupClasses.groupedOutlined}${capitalize(color)}.${
      buttonGroupClasses.grouped
    }.${buttonGroupClasses.disabled}`]: {
      borderColor: alpha(theme.palette[color].main, 0.5),
      color: alpha(theme.palette[color].main, 0.5),
    },
  })
);

export const ButtonGroup = ({
  children,
  ...otherProps
}: MuiButtonGroupProps) => (
  <StyledButtonGroup {...otherProps}>{children}</StyledButtonGroup>
);

export default ButtonGroup;
