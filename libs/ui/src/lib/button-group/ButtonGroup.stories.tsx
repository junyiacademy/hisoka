import React, { useState } from 'react';
import { Story, Meta } from '@storybook/react';
import { ButtonGroupProps as MuiButtonGroupProps } from '@mui/material';
import { ButtonGroup } from './ButtonGroup';
import { Button } from '../button/Button';

const ButtonGroupWithState = (props: MuiButtonGroupProps) => {
  const [activeId, setActiveId] = useState(null);

  const buttons = ['JUI', 'MUI', 'Unstyled UI'];

  return (
    <ButtonGroup {...props}>
      {buttons.map((id) => (
        <Button
          key={id}
          onClick={() => setActiveId(id)}
          active={id === activeId}
        >
          {id}
        </Button>
      ))}
    </ButtonGroup>
  );
};

export default {
  component: ButtonGroup,
  title: 'ButtonGroup',
  argTypes: {
    variant: {
      table: {
        defaultValue: { summary: 'contained' },
      },
    },
    color: {
      table: {
        defaultValue: { summary: 'primary' },
      },
    },
    disabled: {
      table: {
        defaultValue: { summary: false },
      },
    },
  },
} as Meta;

const Template: Story<MuiButtonGroupProps> = (args) => (
  <ButtonGroupWithState {...args} />
);

export const Basic = Template.bind({});

Basic.args = {
  variant: 'contained',
  color: 'primary',
  disabled: false,
};
