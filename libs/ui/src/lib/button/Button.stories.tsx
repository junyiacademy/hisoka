import React from 'react';
import { Story, Meta } from '@storybook/react';
import { Button, ButtonProps } from './Button';

export default {
  component: Button,
  title: 'Button',
  argTypes: {
    active: {
      description: 'The Button is active or not.',
      table: {
        defaultValue: { summary: false },
      },
    },
    variant: {
      table: {
        defaultValue: { summary: 'contained' },
      },
    },
    color: {
      table: {
        defaultValue: { summary: 'primary' },
      },
    },
    disabled: {
      table: {
        defaultValue: { summary: false },
      },
    },
    size: {
      table: {
        defaultValue: { summary: 'medium' },
      },
    },
  },
} as Meta;

const Template: Story<ButtonProps> = (args) => (
  <Button {...args}>I'm a Button</Button>
);

export const TextOnly = Template.bind({});

TextOnly.args = {
  active: false,
  variant: 'contained',
  color: 'primary',
  disabled: false,
  size: 'medium',
};
