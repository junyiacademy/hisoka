import React from 'react';
import { styled, alpha, Theme } from '@mui/material/styles';
import {
  Button as MuiButton,
  ButtonProps as MuiButtonProps,
} from '@mui/material';
import { buttonClasses } from '@mui/material/Button';

// utils
import capitalize from '../../utils/Capitalize';

// self-defined-components
interface StyleFuncProps {
  active?: boolean;
  color?: string;
  theme?: Theme;
}

const StyledButton = styled(MuiButton, {
  shouldForwardProp: (prop: string): boolean => prop !== 'active',
})<StyleFuncProps>(({ active, color = 'primary', theme }: StyleFuncProps) => ({
  // For variant: outlined
  [`&.${buttonClasses.outlined}${capitalize(color)}`]: {
    backgroundColor: active
      ? theme.palette[color].main
      : theme.palette.common.white,
    color: active ? theme.palette.common.white : theme.palette[color].main,
    borderColor: theme.palette[color].main,
    '&:hover': {
      backgroundColor: active
        ? theme.palette[color].main
        : alpha(theme.palette[color].main, 0.1),
    },

    [`&.${buttonClasses.disabled}`]: {
      backgroundColor: theme.palette.common.white,
      borderColor: theme.palette.action.disabledBackground,
      color: theme.palette.action.disabled,
    },
  },
  // For variant: text
  [`&.${buttonClasses.text}${capitalize(color)}`]: {
    '&:hover': {
      backgroundColor: alpha(theme.palette[color].main, 0.1),
    },
  },
}));

export interface ButtonProps extends MuiButtonProps {
  active?: boolean;
}

export const Button = ({ active, children, ...otherProps }: ButtonProps) => (
  <StyledButton active={active} {...otherProps}>
    {children}
  </StyledButton>
);

export default Button;
