import React, { useEffect } from 'react';
import { Story, Meta } from '@storybook/react';
import { InputAdornment, TextFieldProps } from '@mui/material';
import { Visibility } from '@mui/icons-material';
import { TextField } from './TextField';

export default {
  component: TextField,
  title: 'TextFiled',
  argTypes: {
    color: {
      type: { name: 'string', required: false },
      description:
        'The color of the component. It supports those theme colors that make sense for this component.',
      table: {
        type: { summary: 'primary | secondary' },
        defaultValue: { summary: 'primary' },
      },
      options: ['primary', 'secondary'],
      control: { type: 'radio' },
    },
    variant: {
      type: { name: 'string', required: false },
      description: 'The variant to use.',
      table: {
        type: { summary: 'standard | filled | outlined' },
        defaultValue: { summary: 'standard' },
      },
      options: ['standard', 'filled', 'outlined'],
      control: { type: 'radio' },
    },
    size: {
      type: { name: 'string', required: false },
      description: 'The size of the text field.',
      table: {
        type: { summary: 'small | medium' },
        defaultValue: { summary: 'small' },
      },
      options: ['small', 'medium'],
      control: { type: 'radio' },
    },
    disabled: {
      type: { name: 'boolean', required: false },
      description: 'If true, the input element will be disabled.',
      table: {
        type: { summary: 'boolean' },
        defaultValue: { summary: false },
      },
      control: { type: 'boolean' },
    },
    error: {
      type: { name: 'boolean', required: false },
      description: 'If true, the label will be displayed in an error state.',
      table: {
        type: { summary: 'boolean' },
        defaultValue: { summary: false },
      },
      control: { type: 'boolean' },
    },
    label: {
      type: { name: 'string', required: false },
      description: 'The label content.',
    },
    InputLabelProps: {
      type: { name: 'any', required: false },
      description: 'Attributes applied to inner InputLabel element.',
      table: {
        type: { summary: 'any' },
      },
    },
    InputProps: {
      type: { name: 'any', required: false },
      description: 'Attributes applied to to inner Input element.',
      table: {
        type: { summary: 'any' },
      },
    },
    FormHelperTextProps: {
      type: { name: 'any', required: false },
      description: 'Attributes applied to inner FormHelperText element.',
      table: {
        type: { summary: 'any' },
      },
    },
  },
} as Meta;

const ValueOnlyStory: Story<TextFieldProps> = (args) => <TextField {...args} />;

export const ValueOnly = ValueOnlyStory.bind({});

ValueOnly.args = {
  variant: 'standard',
  color: 'primary',
  disabled: false,
  error: false,
  size: 'small',
  label: 'Which UI?',
};

const TextFieldWithError = (props: TextFieldProps) => {
  const [value, setValue] = React.useState<string>('');
  const [isError, setIsError] = React.useState<boolean>(false);

  const handleChange = (event) => {
    setValue(event.target.value);
  };

  useEffect(() => {
    if (value.length > 3) {
      setIsError(true);
      return;
    }
    setIsError(false);
    return;
  }, [value]);

  return <TextField error={isError} onChange={handleChange} {...props} />;
};

const WithErrorStory: Story<TextFieldProps> = (args) => (
  <TextFieldWithError {...args} />
);

export const WithError = WithErrorStory.bind({});

WithError.args = {
  variant: 'standard',
  color: 'primary',
  disabled: false,
  size: 'small',
  label: 'No more than 3 words',
  helperText: 'No more than 3 words',
};

const WithSuffixStory: Story<TextFieldProps> = (args) => (
  <TextField
    {...args}
    InputProps={{
      endAdornment: (
        <InputAdornment position="end">
          <Visibility />
        </InputAdornment>
      ),
    }}
  />
);

export const WithSuffix = WithSuffixStory.bind({});

WithSuffix.args = {
  variant: 'standard',
  color: 'primary',
  disabled: false,
  error: false,
  size: 'small',
  label: 'Which UI?',
};

const WithPrefixStory: Story<TextFieldProps> = (args) => (
  <TextField
    {...args}
    InputProps={{
      startAdornment: (
        <InputAdornment disableTypography position="start">
          Kg
        </InputAdornment>
      ),
    }}
  />
);

export const WithPrefix = WithPrefixStory.bind({});

WithPrefix.args = {
  variant: 'standard',
  color: 'primary',
  disabled: false,
  error: false,
  size: 'small',
  label: 'Which UI?',
};
