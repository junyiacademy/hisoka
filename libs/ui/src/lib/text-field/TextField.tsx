import React from 'react';
import { styled } from '@mui/material/styles';
import {
  TextField as MuiTextField,
  TextFieldProps as MuiTextFieldProps,
} from '@mui/material';
import { textFieldClasses } from '@mui/material/TextField';
import { inputClasses } from '@mui/material/Input';
import { inputLabelClasses } from '@mui/material/InputLabel';
import { outlinedInputClasses } from '@mui/material/OutlinedInput';
import { formHelperTextClasses } from '@mui/material/FormHelperText';

// utils
import capitalize from '../../utils/Capitalize';

// self-defined-components
const StyledTextField = styled(MuiTextField)(
  ({ label, color = 'primary', theme }) => ({
    [`& .${inputLabelClasses.error}`]: {
      [`&:not(.${inputLabelClasses.shrink})`]: {
        color: theme.palette.text.secondary,
      },
      [`&.${inputLabelClasses.disabled}`]: {
        color: theme.palette.text.disabled,
      },
    },
    [`& .${inputLabelClasses.error}.${inputLabelClasses.focused}`]: {
      color: theme.palette.error.main,
    },
    // For variant: standard | filled
    [`& .${inputClasses.underline}:not(.${inputClasses.disabled})`]: {
      [`&.MuiInputBase-color${capitalize(color)}:hover:before`]: {
        borderColor: theme.palette[color].main,
      },
    },
    // For variant: outlined
    [`& .${inputClasses.disabled} .${outlinedInputClasses.notchedOutline}`]: {
      borderStyle: 'dotted',
    },
    [`& .${inputClasses.error}.${inputClasses.focused} .${outlinedInputClasses.notchedOutline}`]: {
      borderColor: theme.palette.error.main,
    },
    [`&.${textFieldClasses.root} :not(.${inputClasses.disabled}):not(.${inputClasses.error})`]: {
      [`&.MuiInputBase-color${capitalize(color)}:hover .${
        outlinedInputClasses.notchedOutline
      }`]: {
        borderColor: theme.palette[color].main,
      },
    },
    [`& .${outlinedInputClasses.notchedOutline}`]: {
      '& > legend': {
        maxWidth: label === '' && 0,
      },
    },
  })
);

export const TextField = (props: MuiTextFieldProps) => {
  const hasEndAdornment = !!props?.InputProps?.endAdornment;
  const {
    InputLabelProps,
    InputProps,
    FormHelperTextProps = {},
    ...otherProps
  } = props;

  const {
    sx: formHelperTextSx = [],
    ...otherFormHelperTextProps
  } = FormHelperTextProps;

  return (
    <StyledTextField
      InputLabelProps={{
        ...InputLabelProps,
        shrink: hasEndAdornment ? true : undefined,
      }}
      FormHelperTextProps={{
        sx: [
          (theme) => ({
            [`&.${formHelperTextClasses.root}`]: {
              marginLeft: 0,
              [`&.${inputClasses.error}`]: {
                color: theme.palette.error.main,
              },
            },
          }),
          ...(Array.isArray(formHelperTextSx)
            ? formHelperTextSx
            : [formHelperTextSx]),
        ],
        ...otherFormHelperTextProps,
      }}
      InputProps={{ ...InputProps }}
      {...otherProps}
    />
  );
};

export default TextField;
