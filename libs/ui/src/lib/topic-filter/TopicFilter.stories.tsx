import React from 'react';
import { Story, Meta } from '@storybook/react';
import {
  TopicFilter as JuiTopicFilter,
  TopicFilterProps as JuiTopicFilterProps,
} from './TopicFilter';
import { topicOptionsList as mockTopicOptionsList } from '../../utils/topicOptionsList';

const mockSelectedTopicIds = [
  'root',
  'course-compare',
  'math-grade-1-a',
  'g03-menzs3b',
  'j-m3ach1-1',
];

export default {
  component: JuiTopicFilter,
  title: 'TopicFilter',
  argTypes: {
    topicOptionsList: {
      type: { name: 'array', required: true },
      description: 'Array contains topic options',
      table: {
        type: { summary: 'ITopicOptionsInfo[]' },
      },
      control: { type: 'array' },
    },
    selectedTopicIds: {
      type: { name: 'array', required: true },
      description: 'Array contains selected topic id',
      table: {
        type: { summary: 'string[]' },
      },
      control: { type: 'array' },
    },
    onTopicSelected: {
      type: { name: 'function', required: true },
      description: 'Callback fired when the topic selected.',
      table: {
        type: {
          summary: `(
          selectedTopicId: string,
          selectedInfo: { layerNumber: number }
        ) => void`,
        },
      },
      control: { type: 'function' },
    },
    onResetClick: {
      type: { name: 'function', required: true },
      description: 'Callback fired when reset option is selected.',
      table: {
        type: {
          summary: '() => void',
        },
      },
      control: { type: 'function' },
    },
    hasResetOption: {
      type: { name: 'boolean', required: true },
      description: 'Control the reset option display state.',
      table: {
        type: { summary: 'boolean' },
      },
      control: { type: 'boolean' },
    },
    hasArrow: {
      type: { name: 'boolean', required: true },
      description: 'Control the arrow display state.',
      table: {
        type: { summary: 'boolean' },
      },
      control: { type: 'boolean' },
    },
  },
} as Meta;

const TopicFilterWithContainer = (props: JuiTopicFilterProps) => {
  const [selectedTopicIds, setSelectedTopicIds] = React.useState(
    mockSelectedTopicIds.slice(0, 1)
  );
  const [topicOptionsList, setTopicOptionsList] = React.useState(
    mockTopicOptionsList.slice(0, 1)
  );

  const handleResetClick = () => {
    setSelectedTopicIds(mockSelectedTopicIds.slice(0, 1));
    setTopicOptionsList(mockTopicOptionsList.slice(0, 1));
  };

  const handleTopicSelected = (selectedTopicId: string, { layerNumber }) => {
    setSelectedTopicIds(mockSelectedTopicIds.slice(0, layerNumber + 2));
    setTopicOptionsList(mockTopicOptionsList.slice(0, layerNumber + 2));
  };

  return (
    <JuiTopicFilter
      {...props}
      onTopicSelected={handleTopicSelected}
      onResetClick={handleResetClick}
      topicOptionsList={topicOptionsList}
      selectedTopicIds={selectedTopicIds}
    />
  );
};

const TopicFilterStory: Story<JuiTopicFilterProps> = (args) => (
  <TopicFilterWithContainer {...args} />
);

export const TopicFilter = TopicFilterStory.bind({});

TopicFilter.args = {
  hasResetOption: true,
  hasArrow: true,
};
