import React, { useState } from 'react';
import ArrowRightRoundedIcon from '@mui/icons-material/ArrowRightRounded';
import { Box } from '@mui/material';
import Select from '../select/Select';
import SelectMenuItem from '../menu-item/SelectMenuItem';

// types
import type { ITopicOptionsInfo } from '../../interfaces';

// self-defined-configs
const LABEL = '請選擇';
const OPTION_FOR_RESET = 'option-for-reset';

// self-defined-components

export interface TopicFilterProps {
  topicOptionsList: ITopicOptionsInfo[];
  onTopicSelected: (
    selectedTopicId: string,
    selectedInfo: { layerNumber: number }
  ) => void;
  onResetClick: () => void;
  hasResetOption: boolean;
  hasArrow: boolean;
  selectedTopicIds: string[];
}

export const TopicFilter = ({
  topicOptionsList,
  onTopicSelected,
  onResetClick,
  hasResetOption,
  hasArrow,
  selectedTopicIds,
}: TopicFilterProps) => {
  const [isFocusedList, setIsFocusedList] = useState([]);

  const handleChange = (e, layerNumber) => {
    if (e.target.value === OPTION_FOR_RESET) {
      onResetClick();
      return;
    }

    onTopicSelected(e.target.value, {
      layerNumber,
    });

    setIsFocusedList((prevList) => [
      ...prevList.slice(0, layerNumber + 1),
      false,
    ]);
  };

  return (
    <Box
      sx={{
        display: 'flex',
        alignItems: 'center',
        flexWrap: 'wrap',
      }}
    >
      {topicOptionsList.map((topicOptions, layerNumber) => {
        const hasLabel =
          isFocusedList[layerNumber] || !selectedTopicIds[layerNumber + 1];

        return (
          <Box
            sx={{
              display: 'flex',
              alignItems: 'center',
            }}
            key={layerNumber}
          >
            {topicOptions.isLoading ? (
              <Select
                variant="outlined"
                size="small"
                label="載入資料中..."
                disabled
                FormControlProps={{
                  sx: (theme) => ({
                    width: 220,
                    margin: theme.spacing(1),
                  }),
                }}
              ></Select>
            ) : (
              <Select
                variant="outlined"
                size="small"
                label={LABEL}
                paperMaxHeight={412}
                hasLabel={hasLabel}
                value={selectedTopicIds?.[layerNumber + 1] || ''}
                SelectProps={{
                  'data-testid': `layered-topic-${layerNumber}`,
                }}
                InputProps={{
                  inputProps: {
                    'aria-label': `layered-topic-${layerNumber}`,
                  },
                  onChange: (e) => {
                    handleChange(e, layerNumber);
                  },
                  onFocus: () => {
                    setIsFocusedList((prevList) => {
                      const newList = [...prevList];
                      newList[layerNumber] = true;
                      return newList;
                    });
                  },
                  onBlur: () => {
                    setIsFocusedList((prevList) => {
                      const newList = [...prevList];
                      newList[layerNumber] = false;
                      return newList;
                    });
                  },
                }}
                FormControlProps={{
                  sx: (theme) => ({
                    width: 220,
                    margin: theme.spacing(1),
                  }),
                }}
              >
                <SelectMenuItem disabled value="">
                  {LABEL}
                </SelectMenuItem>
                {hasResetOption && layerNumber === 0 && (
                  <SelectMenuItem value={OPTION_FOR_RESET}>全部</SelectMenuItem>
                )}
                {topicOptions.options.map((option) => (
                  <SelectMenuItem
                    key={option.id}
                    value={option.id}
                    data-testid={`layered-menuitem-${layerNumber}`}
                    data-is-content-topic={option.hasContentChild}
                  >
                    {option.title}
                  </SelectMenuItem>
                ))}
              </Select>
            )}
            {hasArrow && layerNumber !== topicOptionsList.length - 1 && (
              <ArrowRightRoundedIcon
                sx={(theme) => ({
                  margin: theme.spacing(-1, -1.5),
                  fontSize: theme.spacing(7),
                  color: '#444',
                })}
                fontSize="large"
                data-testid="topic-filter-arrow"
              ></ArrowRightRoundedIcon>
            )}
          </Box>
        );
      })}
    </Box>
  );
};

export default TopicFilter;
