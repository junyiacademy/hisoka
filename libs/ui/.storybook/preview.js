import { ThemeProvider } from '@mui/material/styles';
import { ThemeProvider as Emotion10ThemeProvider } from 'emotion-theming';
import theme from '../src/styles/theme';

export const decorators = [
  (Story) => (
    // Workaround: refrenced from https://mui.com/material-ui/guides/migration-v4/#storybook-emotion-with-v5
    <Emotion10ThemeProvider theme={theme}>
      <ThemeProvider theme={theme}>
        <Story />
      </ThemeProvider>
    </Emotion10ThemeProvider>
  ),
];
