module.exports = {
    "ignorePatterns": ["!**/*", "**/*.stories.tsx"],
    overrides: [
      {
        files: ['*.ts', '*.tsx'],
        env: { "browser": true, "es6": true, "node": true },
        parser: "@typescript-eslint/parser",
        "parserOptions": {
          "project": 'libs/ui/tsconfig.json',
        },
        plugins: [
          "@typescript-eslint",
          'react',
          'jest',
          'react-hooks',
          'testing-library',
          'jest-dom',
          'prettier',
        ],
        extends: [
          'airbnb-base',
          'airbnb-typescript/base',
          'plugin:@nrwl/nx/react',
          'plugin:import/react',
          'plugin:react/recommended',
          'plugin:testing-library/react',
          'plugin:jest-dom/recommended',
  
          '../../.eslintrc.json',
          'prettier',
        ],
        rules: {
          "import/extensions": "off",
          'import/prefer-default-export': 'off',
          'import/no-extraneous-dependencies': ['error', { devDependencies: true }],
          // allow debugger during development
          'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
          'no-underscore-dangle': 0,
          'no-console': ['warn', { allow: ['warn', 'error'] }],
          'react/jsx-uses-react': 'error',
          'react/jsx-uses-vars': 'error',
          radix: ['error', 'as-needed'],
          'max-len': [
            1,
            120,
            2,
            {
              ignorePattern: '^import\\s.+\\sfrom\\s.+;$',
              ignoreUrls: true,
              ignoreStrings: true,
            },
          ],
          'jest/no-disabled-tests': 'warn',
          'jest/no-focused-tests': 'error',
          'jest/no-identical-title': 'error',
          'jest/prefer-to-have-length': 'warn',
          'jest/valid-expect': 'error',
          'jsx-quotes': ['error', 'prefer-double'],
          'react-hooks/rules-of-hooks': 'error',
          'react-hooks/exhaustive-deps': 'warn',
          'no-unused-vars': [
            'error',
            {
              varsIgnorePattern: '[iI]gnored',
              argsIgnorePattern: '^_',
            },
          ],
          'no-param-reassign': [
            'error',
            { props: true, ignorePropertyModificationsFor: ['state'] },
          ],
          'max-classes-per-file': 'off',
          'no-shadow': 0,
          camelcase: ['error', { allow: ['^UNSAFE_'] }],
        },
      },
    ],
  }
  
