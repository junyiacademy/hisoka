
/*
 * ------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */
export enum Role {
    ADMIN = "ADMIN",
    MEMBER = "MEMBER",
    GUEST = "GUEST"
}

export class SignupInput {
    email: string;
    password: string;
    roles: Role[];
}

export class SigninInput {
    email: string;
    password: string;
}

export class UpdateMeInput {
    email?: string;
    avatar?: string;
    verified?: boolean;
}

export class Badge {
    description: string;
    icon: string;
    isOwned: boolean;
    lastEarnedDate?: string;
    safeExtendedDescription: string;
}

export class BadgeCollection {
    category: number;
    categoryIcon: string;
    typeLabel: string;
    badges: Badge[];
    userBadges?: Badge[];
    categoryDescription: string;
}

export class SelectedBadge {
    description: string;
    iconUrl: string;
    safeExtendedDescription: string;
    typeLabel: string;
}

export abstract class IQuery {
    abstract badgeMenu(): BadgeCollection[] | Promise<BadgeCollection[]>;

    abstract me(): User | Promise<User>;
}

export class User {
    id: string;
    email: string;
    avatar: string;
    verified: boolean;
    createdAt: string;
    roles: Role[];
}

export class AuthUser {
    token: string;
    user: User;
}

export abstract class IMutation {
    abstract signup(signupInput: SignupInput): AuthUser | Promise<AuthUser>;

    abstract signin(signinInput: SigninInput): AuthUser | Promise<AuthUser>;

    abstract updateMe(updateMeInput: UpdateMeInput): User | Promise<User>;
}
