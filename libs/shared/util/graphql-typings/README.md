# shared-util-graphql-typings

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test shared-util-graphql-typings` to execute the unit tests via [Jest](https://jestjs.io).
