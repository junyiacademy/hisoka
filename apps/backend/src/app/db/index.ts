const low = require('lowdb') // eslint-disable-line
const FileSync = require('lowdb/adapters/FileSync') // eslint-disable-line

import createModel from './models'

const adapter = new FileSync('apps/backend/src/app/db/db.json')
const db = low(adapter)

db.defaults({ users: [], badges: [] })

const models = {
  User: createModel(db, 'users'),
  Badge: createModel(db, 'badges'),
}

export { db, models }
