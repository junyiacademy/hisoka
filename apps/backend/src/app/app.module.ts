import { Module } from '@nestjs/common'
import { GraphQLModule } from '@nestjs/graphql'

import { db, models } from './db'
import { getUserFromToken, createToken } from './auth'
import { AuthDirective } from './directives'

import { AppController } from './app.controller'
import { AppService } from './app.service'

import { UsersModule } from './modules/users/users.module'
import { BadgeModule } from './modules/badge/badge.module'

const graphqlModule = GraphQLModule.forRoot({
  typePaths: ['./**/*.graphql'],
  cors: { origin: true, credentials: true },
  // mocks: true,
  context: function ({ req, connection }) {
    const context = { db, models }

    if (connection) {
      return { ...context, ...connection.context }
    }

    const user = getUserFromToken(req.cookies?.access_token)
    return { ...context, user, createToken }
  },
  schemaDirectives: {
    auth: AuthDirective,
  },
})

@Module({
  imports: [graphqlModule, UsersModule, BadgeModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
