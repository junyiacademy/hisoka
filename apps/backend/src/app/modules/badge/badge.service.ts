import { Injectable } from '@nestjs/common'

import { BadgeCollection } from '@hisoka/shared/util/graphql-typings'

@Injectable()
export class BadgeService {
  findBadgeMenu({ models }): BadgeCollection[] {
    return models.Badge.findMany()
  }
}
