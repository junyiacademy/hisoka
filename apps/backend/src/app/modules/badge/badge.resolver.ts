import { Resolver, Query, Context } from '@nestjs/graphql'
import { BadgeService } from './badge.service'

@Resolver('Badge')
export class BadgeResolver {
  constructor(private readonly badgeService: BadgeService) {}

  @Query('badgeMenu')
  findBadgeMenu(@Context() ctx) {
    return this.badgeService.findBadgeMenu(ctx)
  }
}
