import { GraphQLDefinitionsFactory } from '@nestjs/graphql'
import { join } from 'path'

const definitionsFactory = new GraphQLDefinitionsFactory()
definitionsFactory.generate({
  typePaths: ['./apps/backend/src/app/modules/**/*.graphql'],
  path: join(
    process.cwd(),
    './libs/shared/util/graphql-typings/src/lib/graphql.schema.ts'
  ),
  outputAs: 'class',
})
