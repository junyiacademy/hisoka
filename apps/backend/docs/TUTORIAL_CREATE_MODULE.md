# Introduction

We can generate GraphQL module by NestJS.

# Example

1. `$ npm i -g @nestjs/cli`
1. cd apps/backend/src/app/modules
1. nest g resource <module_name>
1. choose schema based so we can defined the schema first instead of writing the classes first
1. define your GraphQL schema in `apps/backend/src/app/modules/<module_name>/<module_name>.graphql`
1. add a new property `<module_name>` in `apps/backend/src/app/db/db.json.example`
1. add a new property `<module_name>` and some mock data in `apps/backend/src/app/db/db.json`
1. add a new model in the `apps/backend/src/app/db/index.ts`
1. define your DTO in `apps/backend/src/app/modules/<module_name>/dto/`
1. modify the resolvers in `apps/backend/src/app/modules/<module_name>/<module_name>.resolver.ts`
1. modify the services in `apps/backend/src/app/modules/<module_name>/<module_name>.service.ts`
1. include your new module in the `apps/backend/src/app/app.module.ts`

(the testing files can be ignored because this backend application is only used to provide mock API)
