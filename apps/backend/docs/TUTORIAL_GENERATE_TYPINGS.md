# Introduction

After GraphQL schema is defined, we can use it to generate typings. Therefore, we don't need to define the typings ourselves.

# Setup

In `apps/backend/src/generate-typings.ts`. It will read all `<name>.graphql` files inside the modules then generate `graphql.schema.ts` into `libs/shared/util/graphql-typings`

```js
import { GraphQLDefinitionsFactory } from '@nestjs/graphql'
import { join } from 'path'

const definitionsFactory = new GraphQLDefinitionsFactory()
definitionsFactory.generate({
  typePaths: ['./apps/backend/src/app/modules/**/*.graphql'],
  path: join(
    process.cwd(),
    './libs/shared/util/graphql-typings/src/lib/graphql.schema.ts'
  ),
  outputAs: 'class',
})
```
