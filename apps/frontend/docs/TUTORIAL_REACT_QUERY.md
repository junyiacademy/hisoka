# Introduction

We use React Query to handle fetching, caching and updating data from the server without touching the "global state".

# Example

We may use React Query to deal with both RESTful and GraphQL API.

## RESTful API

```js
import { useQuery } from 'react-query'

export const getSearchQueryKey = (keyword) => ['search', keyword]

export const fetchSearchResult = (keyword) =>
  fetch(`/search?keyword=${keyword}`).then((x) => x.json())

export default function useSearch(keyword) {
  return useQuery(getSearchQueryKey(keyword), () => fetchSearchResult(keyword))
}
```

## GraphQL API

Need to use `gql` to define the GraphQL query first, then put this query into `graphQLClient`.

```js
import { gql } from 'graphql-request'
import { useQuery } from 'react-query'
import { graphQLClient } from '@hisoka/frontend/utils/graphQLClient'

export const permissionQueryKey = 'permission'

export const PERMISSION = gql`
  query Permission {
    me {
      verified
      roles
    }
  }
`

export const fetchPermission = () => graphQLClient.request(PERMISSION).me

export default function usePermission() {
  return useQuery(permissionQueryKey, fetchPermission)
}
```

# Unit Test

Use `@testing-library/react-hooks` to render our custom hooks wrapped in our custom `ReactQueryWrapper`. Mock Service Worker can mock the RESTful and GraphQL API as demo below.

## RESTful API

```js
import { renderHook } from '@testing-library/react-hooks'
import { rest } from 'msw'
import server from '../../tests/mswServer'
import createReactQueryWrapper from '../../tests/createReactQueryWrapper'
import useSearch from '../useSearch'

test('should get search result', async () => {
  server.use(
    rest.get(`<your_endpoint>`, (_req, res, ctx) => {
      return res(ctx.json(`<your_mock_response>`))
    })
  )

  const { result, waitFor } = renderHook(() => useSearch('some keyword'), {
    wrapper: createReactQueryWrapper(),
  })

  await waitFor(() => result.current.isSuccess)

  expect(result.current.data).toEqual(`<your_mock_response>`)
})

test('should get error', async () => {
  server.use(
    rest.get(`<your_endpoint>`, (_req, res, ctx) => {
      return res(ctx.status(500))
    })
  )

  const { result, waitFor } = renderHook(() => useSearch('some keyword'), {
    wrapper: createReactQueryWrapper(),
  })

  await waitFor(() => result.current.isError)

  expect(result.current.error).toBeDefined()
})
```

## GraphQL API

```js
import { renderHook } from '@testing-library/react-hooks'
import { graphql } from 'msw'
import { server } from '../../tests/setupTests'
import createReactQueryWrapper from '../../tests/createReactQueryWrapper'
import usePost from '../usePost'

test('should get post', async () => {
  const MOCK_POST_ID = 'fake-id'
  const MOCK_USER = generateRandomUser()
  const CURRENT_TIME_STAMP = Date.now().toString()

  server.use(
    graphql.query('Post', (req, res, ctx) => {
      const { input } = req.variables

      return res(
        ctx.data({
          post: generateRandomPost({
            id: input,
            author: MOCK_USER,
            createdAt: CURRENT_TIME_STAMP,
          }),
        })
      )
    })
  )

  const { result, waitFor } = renderHook(() => usePost(MOCK_POST_ID), {
    wrapper: createReactQueryWrapper(),
  })

  await waitFor(() => result.current.isSuccess)

  expect(result.current.data).toEqual(
    generateRandomPost({
      id: MOCK_POST_ID,
      author: MOCK_USER,
      createdAt: CURRENT_TIME_STAMP,
    })
  )
})
```
