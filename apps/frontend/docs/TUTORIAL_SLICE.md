# Introduction

To simplify this process, Redux Toolkit includes a `createSlice` function that will auto-generate the action types and action creators for you, based on the names of the reducer functions you provide.

Check [Simplifying Slices with createSlice](https://redux-toolkit.js.org/usage/usage-guide#simplifying-slices-with-createslice) for more information.

# Example

## A simple Slice

1. define the interface

   ```ts
   // src/interfaces/cat.ts

   export default interface ICat {
     name: string
     birthday: string
     location: string
   }
   ```

   ```ts
   // src/redux/types.ts

   import type ICat from '../interfaces/cat'

   export interface IState {
     cats: ICat[]
     isFetchingCats: boolean
   }
   ```

2. create slice

   ```ts
   // src/redux/types.ts

   import { createSlice, PayloadAction } from '@reduxjs/toolkit'
   import type ICat from '../interfaces/cat'
   import type { IState } from './types'

   export const namespace = 'adoption'

   export const initialState: IState = {
     // server side
     cats: [],

     // client side
     isFetchingCats: false,
   }

   const adoptionSlice = createSlice({
     name: namespace,
     initialState,
     reducers: {
       // Add an `Async` suffix if the action will send an API request through Epic
       fetchCatsAsync: (state) => {
         state.isFetchingCats = true
       },
       // Use the PayloadAction type to declare the contents of `action.payload`
       fetchCatsSuccess: (state, action: PayloadAction<{ cats: ICat[] }>) => {
         state.isFetchingCats = false
         state.cats = action.payload.cats
       },
       fetchCatsFailure: (state) => {
         state.isFetchingCats = false
         state.cats = initialState.cats
       },
     },
   })

   export const {
     fetchCatsAsync,
     fetchCatsSuccess,
     fetchCatsFailure,
   } = adoptionSlice.actions

   export const reducer = adoptionSlice.reducer
   ```

# Unit Test

Use the simple slice above as example.

```js
import {
  reducer,
  initialState,
  fetchCatsAsync,
  fetchCatsSuccess,
  fetchCatsFailure,
} from '../slice'

test('should return the initial state on first run', () => {
  // Arrange
  const nextState = initialState

  // Act
  const result = reducer(undefined, {})

  // Assert
  expect(result).toEqual(nextState)
})

test('should properly set the state when a fetch cats request is made', () => {
  // Arrange

  // Act
  const nextState = reducer(initialState, fetchCatsAsync())

  // Assert
  expect(nextState.isFetchingCats).toEqual(true)
})

test('should properly set the state when a fetch cats request succeeds', () => {
  // Arrange
  const mockInitialState = {
    ...initialState,
    isLoading: true,
  }
  const payload = {
    cats: [
      {
        name: 'Miller',
        birthday: '2021/03/01',
        location: 'Taipei',
      },
      {
        name: 'Carly',
        birthday: '2021/03/05',
        location: 'Taipei',
      },
    ],
  }

  // Act
  const nextState = reducer(mockInitialState, fetchCatsSuccess(payload))

  // Assert
  expect(nextState.isFetchingCats).toEqual(false)
  expect(nextState.cats).toEqual(payload.cats)
})

test('should properly set the state when a fetch cats request fails', () => {
  // Arrange
  const mockInitialState = {
    ...initialState,
    isLoading: true,
    cats: [
      {
        name: 'Miller',
        birthday: '2021/03/01',
        location: 'Taipei',
      },
      {
        name: 'Carly',
        birthday: '2021/03/05',
        location: 'Taipei',
      },
    ],
  }

  // Act
  const nextState = reducer(mockInitialState, fetchCatsFailure())

  // Assert
  expect(nextState.isFetchingCats).toEqual(false)
  expect(nextState.cats).toEqual(initialState.cats)
})
```
