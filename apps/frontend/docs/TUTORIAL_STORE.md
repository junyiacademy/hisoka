# Introduction

A store holds the whole state tree of your application. The only way to change the state inside it is to dispatch an action on it.

Check [Store](https://redux.js.org/api/store) for more information.

We follow redux example in next.js to create our store.

Check [Redux example](https://github.com/vercel/next.js/tree/canary/examples/with-redux) for more information.
