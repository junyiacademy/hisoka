# Introduction

An Epic is the core primitive of redux-observable.

It is a function which takes a stream of actions and returns a stream of actions. Actions in, actions out.

It has roughly this type signature:

`function (action$: Observable<Action>, state$: StateObservable<State>): Observable<Action>`

Check [Epics](https://redux-observable.js.org/docs/basics/Epics.html) for more information.

# Example

## A simple **get** Epic

Lets fetch cats through `/api/cats` together.

If response ok, `getCatsEpic` will dispatch a `fetchCatsSuccess` action.

Otherwise, it will dispatch a `fetchCatsFailure` action.

```ts
// src//redux/epics.ts

import { combineEpics, Epic, ofType } from 'redux-observable'
import { of, from } from 'rxjs'
import { map, switchMap, catchError } from 'rxjs/operators'

// type guards
import { assertIsSuccessResponse } from '@/utils'
import { assertIsCatsResponse } from './types'

// action creators
import { fetchCatsAsync, fetchCatsSuccess, fetchCatsFailure } from './slice'

export const getCatsEpic: Epic = (action$, _state$, { get }) =>
  action$.pipe(
    ofType(fetchCatsAsync.type),
    switchMap(() =>
      // Use switchMap because we only care the last api request
      from(get('/api/cats', {})).pipe(
        map(assertIsSuccessResponse), // general type guard
        map(assertIsCatsResponse), // specific type guard
        map((response) => response.data),
        map(fetchCatsSuccess),
        catchError(() => of(fetchCatsFailure()))
      )
    )
  )
```

## Get with query

```ts
// src//redux/epics.ts

import { combineEpics, Epic, ofType } from 'redux-observable'
import { of, from } from 'rxjs'
import { map, switchMap, catchError } from 'rxjs/operators'

// type guards
import { assertIsSuccessResponse } from '@/utils'
import { assertIsCatsResponse } from './types'

// action creators
import { fetchCatsAsync, fetchCatsSuccess, fetchCatsFailure } from './slice'

export const getCatsEpic: Epic = (action$, _state$, { get }) =>
  action$.pipe(
    ofType(fetchCatsAsync.type),
    switchMap(() =>
      // Use switchMap because we only care the last api request
      from(get('/api/cats', { category: action$.category })).pipe(
        map(assertIsSuccessResponse), // general type guard
        map(assertIsCatsResponse), // specific type guard
        map((response) => response.data),
        map(fetchCatsSuccess),
        catchError(() => of(fetchCatsFailure()))
      )
    )
  )
```

## Output multiple actions

This is useful when you want to dispatch actions across different slices.

```ts
import { combineEpics, Epic, ofType } from 'redux-observable'
import { mergeMap } from 'rxjs/operators'

// action creators
import { inputAction, outputAction1, outputAction2 } from './slice'

export const outputMultipleActionsEpic: Epic = (action$, _state$, {}) =>
  action$.pipe(
    ofType(inputAction.type),
    mergeMap(() => [outputAction1(), outputAction2()])
  )
```

# Unit Test

Writing tests for epics is very interesting.

You can draw the marble diagram and test with `TestScheduler`.

Check [Using the RxJS TestScheduler](https://redux-observable.js.org/docs/recipes/WritingTests.html) for more information.
