/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import axios from 'axios'

export const fetchPermission = (cookies: string) =>
  axios
    .get('http://localhost:3000/api/permission', {
      headers: { Cookie: cookies },
    })
    .then((res) => {
      if (res.status >= 200 && res.status < 300) {
        return res.data
      }
      return {
        roles: [],
      }
    })
    .catch(() => ({
      roles: [],
    }))
