import { GraphQLClient } from 'graphql-request'

const endpoint = 'http://localhost:3333/graphql'
const graphQLClient = new GraphQLClient(endpoint, {
  credentials: 'include',
  mode: 'cors',
})
// graphQLClient.setHeader('authorization', token);

export { graphQLClient }
