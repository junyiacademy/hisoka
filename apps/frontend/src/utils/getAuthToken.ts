/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import cookie from 'cookie'

export const getAuthToken = (cookies: string): string => {
  const parsedCookies = cookie.parse(cookies)
  return parsedCookies._junyi_session || ''
}
