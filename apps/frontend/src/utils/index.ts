/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

export { fetchPermission } from './fetchPermission'
export { getAuthToken } from './getAuthToken'
export { assertIsTyped, assertIsSuccessResponse } from './typeGuard'
export { graphQLClient } from './graphQLClient'
export type { ISuccessResponse } from './typeGuard'
