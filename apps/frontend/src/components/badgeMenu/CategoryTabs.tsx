/**
 * Copyright (c) 2021 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Tabs } from '@material-ui/core'

// custom types
import { BadgeCollection } from '@hisoka/shared/util/graphql-typings'

// utils

// assets

// components
import CategoryTab from './CategoryTab'

// self-defined-configs

// self-defined-components
const useStyles = makeStyles({
  tabs: { height: '165px' },
})

const CategoryTabs = ({
  badgeCollections,
  categoryIndex,
  setCategoryIndex,
}: Props) => {
  const classes = useStyles()
  return (
    <Tabs
      className={classes.tabs}
      value={categoryIndex}
      variant='scrollable'
      scrollButtons='off'
      TabIndicatorProps={{ style: { backgroundColor: 'transparent' } }}
    >
      {badgeCollections.map(
        ({ category, categoryIcon, typeLabel, userBadges }) => (
          <CategoryTab
            key={`category-button-${category}`}
            title={typeLabel}
            ownedBadgeCount={userBadges.length}
            icon={categoryIcon}
            isSelected={categoryIndex === category}
            onCategoryTabClick={() => setCategoryIndex(category)}
          />
        )
      )}
    </Tabs>
  )
}

type Props = {
  badgeCollections: BadgeCollection[]
  categoryIndex: number
  setCategoryIndex: (arg: number) => void
}

export default CategoryTabs
