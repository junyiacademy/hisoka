/**
 * Copyright (c) 2021 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React, { useState } from 'react'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'
import { Box } from '@material-ui/core'

// hooks
import { useBadge } from '@hisoka/frontend/hooks'

// custom types

// utils

// assets

// components
import { Container, Item } from '@hisoka/frontend/components/Grid'
import CategoryDetail from './CategoryDetail'
import CategoryTabs from './CategoryTabs'
import BadgeDetailDrawer from './BadgeDetailDrawer'
import Loading from './Loading'

// self-defined-configs

// self-defined-components
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      padding: theme.spacing(2),
      overflow: 'auto',
    },
  })
)

const BadgeMenu = () => {
  const classes = useStyles()
  const { data, isLoading, isError } = useBadge()
  const [categoryIndex, setCategoryIndex] = useState(0)
  const [isDrawerOpen, setIsDrawerOpen] = useState(false)
  const [selectedBadge, setSelectedBadge] = useState(data)

  if (isError) {
    return <div>Error!</div>
  }

  if (isLoading) {
    return <Loading />
  }

  return (
    <Box className={classes.root} data-testid='badge-menu-root'>
      <Container>
        <Item xs={12}>
          <CategoryTabs
            badgeCollections={data}
            categoryIndex={categoryIndex}
            setCategoryIndex={setCategoryIndex}
          />
        </Item>
        <Item xs={12}>
          <CategoryDetail
            badgeCollection={data[categoryIndex]}
            setIsDrawerOpen={setIsDrawerOpen}
            setBadgeDetail={setSelectedBadge}
          />
        </Item>
      </Container>
      <BadgeDetailDrawer
        selectedBadge={selectedBadge}
        isDrawerOpen={isDrawerOpen}
        setIsDrawerOpen={setIsDrawerOpen}
      />
    </Box>
  )
}

export default BadgeMenu
