/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'
import { CircularProgress } from '@material-ui/core'

// utils

// assets

// components

// self-defined-configs

// self-defined-components
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      justifyContent: 'center',
      marginTop: theme.spacing(10),
    },
  })
)

const Loading = () => {
  const classes = useStyles()
  return (
    <div data-testid='loading-root' className={classes.root}>
      <CircularProgress />
    </div>
  )
}

export default Loading
