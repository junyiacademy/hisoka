/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import { Box, SwipeableDrawer } from '@material-ui/core'

// custom types
import { SelectedBadge } from '@hisoka/shared/util/graphql-typings'

// utils

// assets

// components
import BadgeDetail from './BadgeDetail'

// self-defined-configs

// self-defined-components

const BadgeDetailDrawer = ({
  isDrawerOpen,
  setIsDrawerOpen,
  selectedBadge,
}: Props) => {
  return (
    <SwipeableDrawer
      ModalProps={{
        // @ts-expect-error haven't find a way to make it pass TypeScript check
        'data-testid': 'badge-detail-drawer-modal',
      }}
      anchor='top'
      open={isDrawerOpen}
      onOpen={() => {
        setIsDrawerOpen(true)
      }}
      onClose={() => {
        setIsDrawerOpen(false)
      }}
      transitionDuration={{
        enter: 500,
        exit: 500,
      }}
    >
      <Box data-testid='badge-detail-drawer-content' height='450px'>
        <BadgeDetail selectedBadge={selectedBadge} />
      </Box>
    </SwipeableDrawer>
  )
}

type Props = {
  isDrawerOpen: boolean
  selectedBadge: SelectedBadge
  setIsDrawerOpen: (arg: boolean) => void
}

export default BadgeDetailDrawer
