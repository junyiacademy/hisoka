/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'
import { Tab, Avatar, Typography } from '@material-ui/core'

// custom types

// utils

// assets

// components

// self-defined-configs

// self-defined-components
const useStyles = makeStyles<
  Theme,
  {
    isTransparent: boolean
    isSelected: boolean
  }
>((theme: Theme) =>
  createStyles({
    root: ({ isTransparent, isSelected }) => ({
      textAlign: 'center',
      width: 'auto',
      minWidth: '16%',
      height: '100%',
      paddingTop: theme.spacing(1.25),
      opacity: isTransparent ? '.5' : '1',
      position: isSelected ? 'relative' : 'static',
      top: isSelected ? '25px' : 0,
      zIndex: isSelected ? 5 : 'auto',
      '&:hover': {
        opacity: 1,
        '& $label': {
          background: '#ddd',
          border: '1px solid #bbb',
          borderBottom: '1px solid #999',
          boxShadow: '0 1px 1px #ccc',
        },
      },
    }),
    avatar: {
      height: 'auto',
      width: 'auto',
      maxHeight: '100px',
      maxWidth: '100px',
      display: 'flex',
      margin: 'auto',
    },
    label: ({ isSelected }) => ({
      fontFamily: 'roboto',
      fontSize: '75%',
      fontWeight: 'bold',
      textShadow: '0 1px 0 #fff',
      textAlign: 'center',
      padding: theme.spacing(0.3),
      border: `1px solid ${isSelected ? '#bbb' : 'transparent'}`,
      borderRadius: '5px',
      borderBottom: isSelected ? '1px solid #999' : '0px',
      color: '#555555',
      display: isSelected ? 'inline' : '',
      background: isSelected ? '#ddd' : 'transparent',
      boxShadow: isSelected ? '0 1px 1px #ccc' : 'none',
    }),
  })
)

const getLabelText = (title: string, ownedBadgeCount: number): string => {
  if (ownedBadgeCount) {
    return `${title}徽章 × ${ownedBadgeCount}`
  }
  return `${title}徽章`
}

const CategoryTab = ({
  title,
  icon,
  ownedBadgeCount,
  isSelected,
  onCategoryTabClick,
}: Props) => {
  const classes = useStyles({
    isTransparent: !isSelected && ownedBadgeCount === 0,
    isSelected,
  })

  return (
    <Tab
      data-testid='category-button-root'
      className={classes.root}
      onClick={onCategoryTabClick}
      disableRipple
      label={
        <>
          <Avatar
            data-testid='category-button-avatar'
            className={classes.avatar}
            src={icon}
          />
          <Typography className={classes.label}>
            {getLabelText(title, ownedBadgeCount)}
          </Typography>
        </>
      }
    />
  )
}

type Props = {
  title: string
  icon: string
  ownedBadgeCount: number
  isSelected: boolean
  onCategoryTabClick: () => void
}

export default CategoryTab
