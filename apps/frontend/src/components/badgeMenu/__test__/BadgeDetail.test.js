/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'

// utils
import { render, screen } from '@testing-library/react'
import { createMatchMedia } from '@hisoka/frontend/tests/createMatchMedia'

// components
import BadgeDetail from '../BadgeDetail'

// self-defined-config
const SCREEN_WIDTH_DESKTOP = 1280
const SCREEN_WIDTH_MOBILE = 599
const TEST_AVATAR_ID = 'badge-detail-icon'
const TEST_SMALL_AVATAR_ID = 'badge-detail-small-icon'

// self-defined-mocks
const mockSelectedBadge = {
  description: 'fake description',
  safeExtendedDescription: 'fake safe extended description',
  iconUrl: 'fake icon url',
  typeLabel: 'fake type label',
}

test('should be rendered correctly with the provided props (desktop)', () => {
  // Arrange
  window.matchMedia = createMatchMedia(SCREEN_WIDTH_DESKTOP)

  render(<BadgeDetail selectedBadge={mockSelectedBadge} />)

  // Act

  // Assert
  expect(screen.getByText(mockSelectedBadge.description)).toBeInTheDocument()
  expect(
    screen.getByText(mockSelectedBadge.safeExtendedDescription)
  ).toBeInTheDocument()
  expect(
    screen.getByText(`${mockSelectedBadge.typeLabel}徽章`)
  ).toBeInTheDocument()
  expect(screen.queryByTestId(TEST_SMALL_AVATAR_ID)).not.toBeInTheDocument()
  expect(screen.getByTestId(TEST_AVATAR_ID)).toBeInTheDocument()
  expect(screen.getByTestId(TEST_AVATAR_ID).firstChild).toHaveAttribute(
    'src',
    mockSelectedBadge.iconUrl
  )
})

test('should be rendered correctly with the provided props (mobile)', () => {
  // Arrange
  window.matchMedia = createMatchMedia(SCREEN_WIDTH_MOBILE)

  render(<BadgeDetail selectedBadge={mockSelectedBadge} />)

  // Act

  // Assert
  expect(screen.getByText(mockSelectedBadge.description)).toBeInTheDocument()
  expect(
    screen.getByText(mockSelectedBadge.safeExtendedDescription)
  ).toBeInTheDocument()
  expect(
    screen.getByText(`${mockSelectedBadge.typeLabel}徽章`)
  ).toBeInTheDocument()
  expect(screen.queryByTestId(TEST_AVATAR_ID)).not.toBeInTheDocument()
  expect(screen.getByTestId(TEST_SMALL_AVATAR_ID)).toBeInTheDocument()
  expect(screen.getByTestId(TEST_SMALL_AVATAR_ID).firstChild).toHaveAttribute(
    'src',
    mockSelectedBadge.iconUrl
  )
})
