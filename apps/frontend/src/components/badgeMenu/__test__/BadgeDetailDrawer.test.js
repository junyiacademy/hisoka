/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import userEvent from '@testing-library/user-event'

// utils
import { render, screen } from '@testing-library/react'

// components
import BadgeDetailDrawer from '../BadgeDetailDrawer'

// self-defined-config
const TEST_ID_DRAWER_CONTENT = 'badge-detail-drawer-content'
const TEST_ID_MODAL = 'badge-detail-drawer-modal'

// self-defined-mock
const mockSetIsDrawerOpen = jest.fn()
const mockSelectedBadge = {
  description: 'fake description',
  safeExtendedDescription: 'fake safe extended description',
  iconUrl: 'fake icon url',
  typeLabel: 'fake type label',
}

test('should set isDrawerOpen as false when clicking background modal', () => {
  // Arrange
  render(
    <BadgeDetailDrawer
      isDrawerOpen={true}
      setIsDrawerOpen={mockSetIsDrawerOpen}
      selectedBadge={mockSelectedBadge}
    />
  )

  // Assert
  expect(screen.queryByTestId(TEST_ID_DRAWER_CONTENT)).toBeInTheDocument()

  // Act
  userEvent.click(screen.getByTestId(TEST_ID_MODAL).firstChild)

  // Assert
  expect(mockSetIsDrawerOpen).toBeCalledTimes(1)
  expect(mockSetIsDrawerOpen).toBeCalledWith(false)
})

test('should not be in the document when isDrawerOpen is false', () => {
  // Arrange
  render(
    <BadgeDetailDrawer
      isDrawerOpen={false}
      setIsDrawerOpen={mockSetIsDrawerOpen}
      selectedBadge={mockSelectedBadge}
    />
  )

  // Act

  // Assert
  expect(screen.queryByTestId(TEST_ID_DRAWER_CONTENT)).not.toBeInTheDocument()
})
