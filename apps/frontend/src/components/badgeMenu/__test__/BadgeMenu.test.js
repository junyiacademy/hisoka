/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import { QueryClient, QueryClientProvider } from 'react-query'

// utils
import { render } from '@testing-library/react'

// components
import BadgeMenu from '../BadgeMenu'

test('should be rendered correctly with the provided props', () => {
  // Arrange
  const queryClient = new QueryClient()

  render(
    <QueryClientProvider client={queryClient}>
      <BadgeMenu />
    </QueryClientProvider>
  )

  // Act

  // Assert
})
