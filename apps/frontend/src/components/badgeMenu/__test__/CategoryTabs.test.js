/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import userEvent from '@testing-library/user-event'

// utils
import { render, screen } from '@testing-library/react'

// components
import CategoryTabs from '../CategoryTabs'

test('should be rendered correctly with the provided props', () => {
  // Arrange
  const mockBadgeCollection = Array.from([1, 2, 3, 4, 5, 6], (x) => ({
    badges: [
      {
        description: 'fake description',
        icon: '',
        isOwned: false,
        safeExtendedDescription: 'fake safe extended description',
      },
    ],
    category: x,
    categoryDescription: `fake category description ${x}`,
    typeLabel: `fake-type-label-${x}`,
    categoryIcon: 'localhost:3000/mock',
    userBadges: [],
  }))
  const mockSetCategoryIndex = jest.fn()

  render(
    <CategoryTabs
      badgeCollections={mockBadgeCollection}
      categoryIndex={0}
      setCategoryIndex={mockSetCategoryIndex}
    />
  )

  // Act

  // Assert
  mockBadgeCollection.forEach(({ category, typeLabel }) => {
    const categoryTabEL = screen.getByText(`${typeLabel}徽章`)
    expect(categoryTabEL).toBeInTheDocument()
    userEvent.click(categoryTabEL)
    expect(mockSetCategoryIndex).toBeCalledWith(category)
  })
  expect(mockSetCategoryIndex).toBeCalledTimes(mockBadgeCollection.length)
})
