/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import Link from 'next/link'
import { styled } from '@material-ui/core/styles'

// custom types

// utils

// assets

// actions

// components

// self-defined-components
const BaseNextLink = styled(Link)({
  color: 'inherit',
  textDecoration: 'none',
})

export default BaseNextLink
