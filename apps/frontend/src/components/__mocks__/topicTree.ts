import type { ITopicTreeNode } from '@hisoka/frontend/interfaces'

const data: ITopicTreeNode = {
  href: '/root',
  children: [
    {
      href: '/partner',
      children: [
        {
          href: '/partner/cooc',
          children: [
            {
              href: '/partner/cooc/cooc2019-math',
              children: [],
              id: 'cooc2019-math',
              title: '數學',
            },
            {
              href: '/partner/cooc/cooc-nature',
              children: [],
              id: 'cooc-nature',
              title: '自然-理化',
            },
            {
              href: '/partner/cooc/bio',
              children: [],
              id: 'bio',
              title: '自然-生物',
            },
          ],
          id: 'cooc',
          title: '酷課雲專區',
        },
        {
          href: '/partner/adl',
          children: [
            {
              href: '/partner/adl/adl-cht',
              children: [],
              id: 'adl-cht',
              title: '國語文',
            },
            {
              href: '/partner/adl/adl-eng',
              children: [],
              id: 'adl-eng',
              title: '英文',
            },
            {
              href: '/partner/adl/adl-math',
              children: [],
              id: 'adl-math',
              title: '數學',
            },
            {
              href: '/partner/adl/adl-math-pre',
              children: [],
              id: 'adl-math-pre',
              title: '數學（出版社）',
            },
          ],
          id: 'adl',
          title: '因材網專區',
        },
        {
          href: '/partner/cool-english',
          children: [
            {
              href: '/partner/cool-english/ce-basic-learning',
              children: [],
              id: 'ce-basic-learning',
              title: '基本學習',
            },
          ],
          id: 'cool-english',
          title: '酷英專區',
        },
        {
          href: '/partner/deltamoocx',
          children: [
            {
              href: '/partner/deltamoocx/deltamoocx-math-s',
              children: [],
              id: 'deltamoocx-math-s',
              title: '高中數學',
            },
            {
              href: '/partner/deltamoocx/deltamoocx-bio-s',
              children: [],
              id: 'deltamoocx-bio-s',
              title: '高中生物',
            },
            {
              href: '/partner/deltamoocx/deltamoocx-che-s',
              children: [],
              id: 'deltamoocx-che-s',
              title: '高中化學',
            },
            {
              href: '/partner/deltamoocx/deltamoocx-phy-s',
              children: [],
              id: 'deltamoocx-phy-s',
              title: '高中物理',
            },
          ],
          id: 'deltamoocx',
          title: '台達磨課師專區',
        },
        {
          href: '/partner/nani-project',
          children: [
            {
              href: '/partner/nani-project/nani-project-math',
              children: [],
              id: 'nani-project-math',
              title: '數學',
            },
            {
              href: '/partner/nani-project/nani-project-english',
              children: [],
              id: 'nani-project-english',
              title: '英語',
            },
            {
              href: '/partner/nani-project/nani-project-ns',
              children: [],
              id: 'nani-project-ns',
              title: '自然',
            },
            {
              href: '/partner/nani-project/nani-project-civic',
              children: [],
              id: 'nani-project-civic',
              title: '公民與社會',
            },
            {
              href: '/partner/nani-project/nani-project-ge',
              children: [],
              id: 'nani-project-ge',
              title: '地理',
            },
            {
              href: '/partner/nani-project/nani-project-history',
              children: [],
              id: 'nani-project-history',
              title: '歷史',
            },
          ],
          id: 'nani-project',
          title: '南一專區',
        },
        {
          href: '/partner/kang-project',
          children: [
            {
              href: '/partner/kang-project/kang-project-math',
              children: [],
              id: 'kang-project-math',
              title: '數學',
            },
            {
              href: '/partner/kang-project/kang-project-mandarin',
              children: [],
              id: 'kang-project-mandarin',
              title: '國語文',
            },
            {
              href: '/partner/kang-project/kang-project-science',
              children: [],
              id: 'kang-project-science',
              title: '自然',
            },
            {
              href: '/partner/kang-project/kang-project-civilsocial',
              children: [],
              id: 'kang-project-civilsocial',
              title: '公民',
            },
          ],
          id: 'kang-project',
          title: '康軒專區',
        },
        {
          href: '/partner/hanlin-project',
          children: [
            {
              href: '/partner/hanlin-project/hanlin-project-math',
              children: [],
              id: 'hanlin-project-math',
              title: '國小數學',
            },
            {
              href: '/partner/hanlin-project/hanlin-project-juniormath',
              children: [],
              id: 'hanlin-project-juniormath',
              title: '國中數學',
            },
            {
              href: '/partner/hanlin-project/hanlin-project-nature',
              children: [],
              id: 'hanlin-project-nature',
              title: '國小自然',
            },
            {
              href: '/partner/hanlin-project/hanlin-project-juniornature',
              children: [],
              id: 'hanlin-project-juniornature',
              title: '國中自然',
            },
            {
              href: '/partner/hanlin-project/hanlin-project-mandarin',
              children: [],
              id: 'hanlin-project-mandarin',
              title: '國語文',
            },
            {
              href: '/partner/hanlin-project/hanlin-project-english',
              children: [],
              id: 'hanlin-project-english',
              title: '英語文',
            },
            {
              href: '/partner/hanlin-project/hanlin-project-civilsocio',
              children: [],
              id: 'hanlin-project-civilsocio',
              title: '公民與社會',
            },
            {
              href: '/partner/hanlin-project/hanlin-project-geography',
              children: [],
              id: 'hanlin-project-geography',
              title: '地理',
            },
            {
              href: '/partner/hanlin-project/hanlin-project-history',
              children: [],
              id: 'hanlin-project-history',
              title: '歷史',
            },
          ],
          id: 'hanlin-project',
          title: '翰林專區',
        },
        {
          href: '/partner/numeracy',
          children: [
            {
              href: '/partner/numeracy/numeracy-jun',
              children: [],
              id: 'numeracy-jun',
              title: '國中小',
            },
            {
              href: '/partner/numeracy/numeracy-sen',
              children: [],
              id: 'numeracy-sen',
              title: '高中',
            },
            {
              href: '/partner/numeracy/numeracy-oth',
              children: [],
              id: 'numeracy-oth',
              title: '其他',
            },
          ],
          id: 'numeracy',
          title: '數感實驗室',
        },
        {
          href: '/partner/tw_bar',
          children: [
            {
              href: '/partner/tw_bar/tw_history',
              children: [],
              id: 'tw_history',
              title: '歷史',
            },
            {
              href: '/partner/tw_bar/tw_civics',
              children: [],
              id: 'tw_civics',
              title: '公民',
            },
            {
              href: '/partner/tw_bar/tw_science',
              children: [],
              id: 'tw_science',
              title: '理化',
            },
            {
              href: '/partner/tw_bar/tw_dancing',
              children: [],
              id: 'tw_dancing',
              title: '舞蹈',
            },
          ],
          id: 'tw_bar',
          title: '臺灣吧專區',
        },
        {
          href: '/partner/mby',
          children: [
            {
              href: '/partner/mby/v784-new-topic-3',
              children: [],
              id: 'v784-new-topic-3',
              title: '博幼前測',
            },
            {
              href: '/partner/mby/mbyg1',
              children: [],
              id: 'mbyg1',
              title: '博幼一年級',
            },
            {
              href: '/partner/mby/mbyg2',
              children: [],
              id: 'mbyg2',
              title: '博幼二年級',
            },
            {
              href: '/partner/mby/mbyg3',
              children: [],
              id: 'mbyg3',
              title: '博幼三年級',
            },
            {
              href: '/partner/mby/mbyg4',
              children: [],
              id: 'mbyg4',
              title: '博幼四年級',
            },
            {
              href: '/partner/mby/mbyg5',
              children: [],
              id: 'mbyg5',
              title: '博幼五年級',
            },
            {
              href: '/partner/mby/mbyg6',
              children: [],
              id: 'mbyg6',
              title: '博幼六年級',
            },
            {
              href: '/partner/mby/mbym4s',
              children: [],
              id: 'mbym4s',
              title: '國中四則',
            },
            {
              href: '/partner/mby/mbym4a',
              children: [],
              id: 'mbym4a',
              title: '國中代數',
            },
            {
              href: '/partner/mby/mbym4g',
              children: [],
              id: 'mbym4g',
              title: '國中幾何',
            },
          ],
          id: 'mby',
          title: '博幼專區',
        },
        {
          href: '/partner/vea01',
          children: [
            {
              href: '/partner/vea01/vea01-u1',
              children: [],
              id: 'vea01-u1',
              title: '單元一  介紹字母動物園',
            },
            {
              href: '/partner/vea01/vea01-u2',
              children: [],
              id: 'vea01-u2',
              title: '單元二  打招呼禮儀',
            },
            {
              href: '/partner/vea01/vea01-u3',
              children: [],
              id: 'vea01-u3',
              title: '單元三  五果盤意義',
            },
            {
              href: '/partner/vea01/vea01-u4',
              children: [],
              id: 'vea01-u4',
              title: '單元四  越南沒有星期一',
            },
            {
              href: '/partner/vea01/vea01-u5',
              children: [],
              id: 'vea01-u5',
              title: '單元五  越南早餐河粉',
            },
            {
              href: '/partner/vea01/vea01-u6',
              children: [],
              id: 'vea01-u6',
              title: '單元六  計程三輪車',
            },
            {
              href: '/partner/vea01/vea01-u7',
              children: [],
              id: 'vea01-u7',
              title: '單元七  越南國花蓮花',
            },
            {
              href: '/partner/vea01/vea01-u8',
              children: [],
              id: 'vea01-u8',
              title: '單元八  越南首都河內',
            },
            {
              href: '/partner/vea01/vea01-u9',
              children: [],
              id: 'vea01-u9',
              title: '單元九  氏果的由來',
            },
            {
              href: '/partner/vea01/vea01-u10',
              children: [],
              id: 'vea01-u10',
              title: '單元十  越南國服長衫',
            },
          ],
          id: 'vea01',
          title: '認識越南語字母',
        },
      ],
      id: 'partner',
      title: '均一好朋友',
    },
    {
      href: '/teacherpreneur',
      children: [
        {
          href: '/teacherpreneur/doctor-huang',
          children: [
            {
              href: '/teacherpreneur/doctor-huang/doctor-huang-01',
              children: [],
              id: 'doctor-huang-01',
              title: '新冠病毒麻瓜課',
            },
            {
              href: '/teacherpreneur/doctor-huang/doctor-huang-02',
              children: [],
              id: 'doctor-huang-02',
              title: '在家防疫不用怕',
            },
            {
              href: '/teacherpreneur/doctor-huang/doctor-huang-03',
              children: [],
              id: 'doctor-huang-03',
              title: '新手爸媽必修課',
            },
            {
              href: '/teacherpreneur/doctor-huang/doctor-huang-07',
              children: [],
              id: 'doctor-huang-07',
              title: '陪孩子健康長大',
            },
            {
              href: '/teacherpreneur/doctor-huang/doctor-huang-05',
              children: [],
              id: 'doctor-huang-05',
              title: '聽聽專業怎麼說',
            },
            {
              href: '/teacherpreneur/doctor-huang/doctor-huang-06',
              children: [],
              id: 'doctor-huang-06',
              title: '故事中的醫學',
            },
          ],
          id: 'doctor-huang',
          title: '黃瑽寧醫師健康講堂：防疫、育兒、健康',
        },
        {
          href: '/teacherpreneur/tfgcoocs',
          children: [
            {
              href: '/teacherpreneur/tfgcoocs/tfgcoocs-chinese-11',
              children: [],
              id: 'tfgcoocs-chinese-11',
              title: '高一國文',
            },
            {
              href: '/teacherpreneur/tfgcoocs/tfgcoocs-math-10',
              children: [],
              id: 'tfgcoocs-math-10',
              title: '高一數學',
            },
            {
              href: '/teacherpreneur/tfgcoocs/tfgcoocs-math-11',
              children: [],
              id: 'tfgcoocs-math-11',
              title: '高二數學',
            },
            {
              href: '/teacherpreneur/tfgcoocs/tfgcoocs-physics-10',
              children: [],
              id: 'tfgcoocs-physics-10',
              title: '高一物理',
            },
            {
              href: '/teacherpreneur/tfgcoocs/tfgcoocs-physics-11',
              children: [],
              id: 'tfgcoocs-physics-11',
              title: '高二物理',
            },
            {
              href: '/teacherpreneur/tfgcoocs/tfgcoocs-chemistry-10',
              children: [],
              id: 'tfgcoocs-chemistry-10',
              title: '高一化學',
            },
            {
              href: '/teacherpreneur/tfgcoocs/tfgcoocs-chemistry-11',
              children: [],
              id: 'tfgcoocs-chemistry-11',
              title: '高二化學',
            },
            {
              href: '/teacherpreneur/tfgcoocs/tfgcoocs-biology-11',
              children: [],
              id: 'tfgcoocs-biology-11',
              title: '高二生物',
            },
            {
              href: '/teacherpreneur/tfgcoocs/tfgcoocs-history-10',
              children: [],
              id: 'tfgcoocs-history-10',
              title: '高一歷史',
            },
            {
              href: '/teacherpreneur/tfgcoocs/tfgcoocs-geography-11',
              children: [],
              id: 'tfgcoocs-geography-11',
              title: '高二地理',
            },
            {
              href: '/teacherpreneur/tfgcoocs/tfgcoocs-civics-11',
              children: [],
              id: 'tfgcoocs-civics-11',
              title: '高二公民',
            },
            {
              href: '/teacherpreneur/tfgcoocs/tfgcoocs-geoscience',
              children: [],
              id: 'tfgcoocs-geoscience',
              title: '地科總整',
            },
            {
              href: '/teacherpreneur/tfgcoocs/tfgcoocs-music',
              children: [],
              id: 'tfgcoocs-music',
              title: '音樂饗宴',
            },
            {
              href: '/teacherpreneur/tfgcoocs/tfgcoocs-lecture-10',
              children: [],
              id: 'tfgcoocs-lecture-10',
              title: '職人講座',
            },
            {
              href: '/teacherpreneur/tfgcoocs/tfgcoocs-civics-10',
              children: [],
              id: 'tfgcoocs-civics-10',
              title: '高一公民',
            },
          ],
          id: 'tfgcoocs',
          title: '北一酷課師 - 高中全科',
        },
        {
          href: '/teacherpreneur/teacherpreneur-001',
          children: [
            {
              href: '/teacherpreneur/teacherpreneur-001/teacherpreneur-001-7a',
              children: [],
              id: 'teacherpreneur-001-7a',
              title: '七年級(上)',
            },
            {
              href: '/teacherpreneur/teacherpreneur-001/teacherpreneur-001-7b',
              children: [],
              id: 'teacherpreneur-001-7b',
              title: '七年級(下)',
            },
            {
              href: '/teacherpreneur/teacherpreneur-001/teacherpreneur-001-8a',
              children: [],
              id: 'teacherpreneur-001-8a',
              title: '八年級(上)',
            },
            {
              href: '/teacherpreneur/teacherpreneur-001/teacherpreneur-001-8b',
              children: [],
              id: 'teacherpreneur-001-8b',
              title: '八年級(下)',
            },
            {
              href: '/teacherpreneur/teacherpreneur-001/teacherpreneur-001-9a',
              children: [],
              id: 'teacherpreneur-001-9a',
              title: '九年級(上)',
            },
          ],
          id: 'teacherpreneur-001',
          title: '劉繼文老師 - 國中數學',
        },
      ],
      id: 'teacherpreneur',
      title: '熱血教師專區',
    },
    {
      href: '/junyi-math',
      children: [
        {
          href: '/junyi-math/men',
          children: [
            {
              href: '/junyi-math/men/menzs',
              children: [],
              id: 'menzs',
              title: '整數',
            },
            {
              href: '/junyi-math/men/menfs',
              children: [],
              id: 'menfs',
              title: '分數',
            },
            {
              href: '/junyi-math/men/mencs',
              children: [],
              id: 'mencs',
              title: '小數',
            },
            {
              href: '/junyi-math/men/menyb',
              children: [],
              id: 'menyb',
              title: '因數與倍數',
            },
            {
              href: '/junyi-math/men/menml',
              children: [],
              id: 'menml',
              title: '長度',
            },
            {
              href: '/junyi-math/men/menmt',
              children: [],
              id: 'menmt',
              title: '時間',
            },
            {
              href: '/junyi-math/men/menmc',
              children: [],
              id: 'menmc',
              title: '容量',
            },
            {
              href: '/junyi-math/men/menmw',
              children: [],
              id: 'menmw',
              title: '重量',
            },
            {
              href: '/junyi-math/men/menma',
              children: [],
              id: 'menma',
              title: '面積',
            },
            {
              href: '/junyi-math/men/menmv',
              children: [],
              id: 'menmv',
              title: '體積',
            },
            {
              href: '/junyi-math/men/menmg',
              children: [],
              id: 'menmg',
              title: '角度',
            },
            {
              href: '/junyi-math/men/menso',
              children: [],
              id: 'menso',
              title: '解題',
            },
            {
              href: '/junyi-math/men/men-history',
              children: [],
              id: 'men-history',
              title: '即將下架',
            },
          ],
          id: 'men',
          title: '國小-數與量',
        },
        {
          href: '/junyi-math/mes',
          children: [
            {
              href: '/junyi-math/mes/mesfl',
              children: [],
              id: 'mesfl',
              title: '平面圖形',
            },
            {
              href: '/junyi-math/mes/mescu',
              children: [],
              id: 'mescu',
              title: '立體形體',
            },
            {
              href: '/junyi-math/mes/mes-history',
              children: [],
              id: 'mes-history',
              title: '即將下架',
            },
          ],
          id: 'mes',
          title: '國小-空間與形狀',
        },
        {
          href: '/junyi-math/mer',
          children: [
            {
              href: '/junyi-math/mer/merys',
              children: [],
              id: 'merys',
              title: '運算性質',
            },
            {
              href: '/junyi-math/mer/merrs',
              children: [],
              id: 'merrs',
              title: '數量模式與推理',
            },
            {
              href: '/junyi-math/mer/mer-history',
              children: [],
              id: 'mer-history',
              title: '即將下架',
            },
          ],
          id: 'mer',
          title: '國小-關係',
        },
        {
          href: '/junyi-math/med',
          children: [
            {
              href: '/junyi-math/med/medta',
              children: [],
              id: 'medta',
              title: '統計表',
            },
            {
              href: '/junyi-math/med/medgr',
              children: [],
              id: 'medgr',
              title: '統計圖',
            },
            {
              href: '/junyi-math/med/med-history',
              children: [],
              id: 'med-history',
              title: '即將下架',
            },
          ],
          id: 'med',
          title: '國小-統計圖表',
        },
        {
          href: '/junyi-math/mjn',
          children: [
            {
              href: '/junyi-math/mjn/mjnzs',
              children: [],
              id: 'mjnzs',
              title: '整數與數線',
            },
            {
              href: '/junyi-math/mjn/mjnfs',
              children: [],
              id: 'mjnfs',
              title: '因數分解與分數運算',
            },
            {
              href: '/junyi-math/mjn/mjnbl',
              children: [],
              id: 'mjnbl',
              title: '比例',
            },
            {
              href: '/junyi-math/mjn/mjnfg',
              children: [],
              id: 'mjnfg',
              title: '平方根與畢氏定理',
            },
            {
              href: '/junyi-math/mjn/mjnsl',
              children: [],
              id: 'mjnsl',
              title: '數列與級數',
            },
            {
              href: '/junyi-math/mjn/mjn-history',
              children: [],
              id: 'mjn-history',
              title: '即將下架',
            },
          ],
          id: 'mjn',
          title: '國中-數與量',
        },
        {
          href: '/junyi-math/mja',
          children: [
            {
              href: '/junyi-math/mja/mjayy',
              children: [],
              id: 'mjayy',
              title: '一元一次方程式',
            },
            {
              href: '/junyi-math/mja/mjaey',
              children: [],
              id: 'mjaey',
              title: '二元一次聯立方程式',
            },
            {
              href: '/junyi-math/mja/mjazb',
              children: [],
              id: 'mjazb',
              title: '直角坐標與二元一次方程式圖形',
            },
            {
              href: '/junyi-math/mja/mjayh',
              children: [],
              id: 'mjayh',
              title: '一次函數',
            },
            {
              href: '/junyi-math/mja/mjayb',
              children: [],
              id: 'mjayb',
              title: '一元一次不等式',
            },
            {
              href: '/junyi-math/mja/mjadx',
              children: [],
              id: 'mjadx',
              title: '多項式與乘法公式',
            },
            {
              href: '/junyi-math/mja/mjayf',
              children: [],
              id: 'mjayf',
              title: '因式分解',
            },
            {
              href: '/junyi-math/mja/mjaye',
              children: [],
              id: 'mjaye',
              title: '一元二次方程式',
            },
            {
              href: '/junyi-math/mja/mjaeh',
              children: [],
              id: 'mjaeh',
              title: '二次函數',
            },
            {
              href: '/junyi-math/mja/mja-history',
              children: [],
              id: 'mja-history',
              title: '即將下架',
            },
          ],
          id: 'mja',
          title: '國中-代數與函數',
        },
        {
          href: '/junyi-math/mjs',
          children: [
            {
              href: '/junyi-math/mjs/v959-new-topic',
              children: [],
              id: 'v959-new-topic',
              title: '幾何圖形與三視圖',
            },
            {
              href: '/junyi-math/mjs/mjstx',
              children: [],
              id: 'mjstx',
              title: '幾何圖形與尺規作圖',
            },
            {
              href: '/junyi-math/mjs/mjssj',
              children: [],
              id: 'mjssj',
              title: '三角形',
            },
            {
              href: '/junyi-math/mjs/mjssb',
              children: [],
              id: 'mjssb',
              title: '平行與四邊形',
            },
            {
              href: '/junyi-math/mjs/mjsxs',
              children: [],
              id: 'mjsxs',
              title: '相似形',
            },
            {
              href: '/junyi-math/mjs/mjsyx',
              children: [],
              id: 'mjsyx',
              title: '圓形',
            },
            {
              href: '/junyi-math/mjs/mjszm',
              children: [],
              id: 'mjszm',
              title: '推理證明與三心',
            },
            {
              href: '/junyi-math/mjs/mjslt',
              children: [],
              id: 'mjslt',
              title: '立體幾何',
            },
            {
              href: '/junyi-math/mjs/mjs-history',
              children: [],
              id: 'mjs-history',
              title: '即將下架',
            },
          ],
          id: 'mjs',
          title: '國中-幾何',
        },
        {
          href: '/junyi-math/mjd',
          children: [
            {
              href: '/junyi-math/mjd/mjdjt',
              children: [],
              id: 'mjdjt',
              title: '統計與機率',
            },
          ],
          id: 'mjd',
          title: '國中-資料與不確定性',
        },
        {
          href: '/junyi-math/v778-new-topic',
          children: [
            {
              href: '/junyi-math/v778-new-topic/v778-new-topic-1',
              children: [],
              id: 'v778-new-topic-1',
              title: '指數與對數',
            },
          ],
          id: 'v778-new-topic',
          title: '高中-函數與方程式',
        },
        {
          href: '/junyi-math/m5s',
          children: [
            {
              href: '/junyi-math/m5s/m5spx',
              children: [],
              id: 'm5spx',
              title: '平面向量',
            },
          ],
          id: 'm5s',
          title: '高中-解析幾何',
        },
      ],
      id: 'junyi-math',
      title: '數學（主題式）',
    },
    {
      href: '/course-compare',
      children: [
        {
          href: '/course-compare/math-1',
          children: [
            {
              href: '/course-compare/math-1/j-m1a',
              children: [],
              id: 'j-m1a',
              title: '均一版',
            },
            {
              href: '/course-compare/math-1/n-m1a',
              children: [],
              id: 'n-m1a',
              title: '南一版',
            },
            {
              href: '/course-compare/math-1/h-m1a',
              children: [],
              id: 'h-m1a',
              title: '翰林版',
            },
            {
              href: '/course-compare/math-1/k-m1a',
              children: [],
              id: 'k-m1a',
              title: '康軒版',
            },
            {
              href: '/course-compare/math-1/math-grade-1-a',
              children: [],
              id: 'math-grade-1-a',
              title: '均一歷代數學',
            },
          ],
          id: 'math-1',
          title: '一年級',
        },
        {
          href: '/course-compare/math-2',
          children: [
            {
              href: '/course-compare/math-2/math-grade-2-a',
              children: [],
              id: 'math-grade-2-a',
              title: '均一版',
            },
            {
              href: '/course-compare/math-2/n-m2a',
              children: [],
              id: 'n-m2a',
              title: '南一版',
            },
            {
              href: '/course-compare/math-2/h-m2a',
              children: [],
              id: 'h-m2a',
              title: '翰林版',
            },
            {
              href: '/course-compare/math-2/k-m2a',
              children: [],
              id: 'k-m2a',
              title: '康軒版',
            },
          ],
          id: 'math-2',
          title: '二年級',
        },
        {
          href: '/course-compare/math-3',
          children: [
            {
              href: '/course-compare/math-3/math-grade-3-a',
              children: [],
              id: 'math-grade-3-a',
              title: '均一版',
            },
            {
              href: '/course-compare/math-3/n-m3a',
              children: [],
              id: 'n-m3a',
              title: '南一版',
            },
            {
              href: '/course-compare/math-3/h-m3a',
              children: [],
              id: 'h-m3a',
              title: '翰林版',
            },
            {
              href: '/course-compare/math-3/k-m3a',
              children: [],
              id: 'k-m3a',
              title: '康軒版',
            },
          ],
          id: 'math-3',
          title: '三年級',
        },
        {
          href: '/course-compare/math-4',
          children: [
            {
              href: '/course-compare/math-4/math-grade-4-a',
              children: [],
              id: 'math-grade-4-a',
              title: '均一版',
            },
            {
              href: '/course-compare/math-4/n-m4a',
              children: [],
              id: 'n-m4a',
              title: '南一版',
            },
            {
              href: '/course-compare/math-4/h-m4a',
              children: [],
              id: 'h-m4a',
              title: '翰林版',
            },
            {
              href: '/course-compare/math-4/k-m4a',
              children: [],
              id: 'k-m4a',
              title: '康軒版',
            },
          ],
          id: 'math-4',
          title: '四年級',
        },
        {
          href: '/course-compare/math-5',
          children: [
            {
              href: '/course-compare/math-5/math-grade-5-a',
              children: [],
              id: 'math-grade-5-a',
              title: '均一版',
            },
            {
              href: '/course-compare/math-5/n-m5a',
              children: [],
              id: 'n-m5a',
              title: '南一版',
            },
            {
              href: '/course-compare/math-5/h-m5a',
              children: [],
              id: 'h-m5a',
              title: '翰林版',
            },
            {
              href: '/course-compare/math-5/k-m5a',
              children: [],
              id: 'k-m5a',
              title: '康軒版',
            },
          ],
          id: 'math-5',
          title: '五年級',
        },
        {
          href: '/course-compare/math-6',
          children: [
            {
              href: '/course-compare/math-6/math-grade-6-a',
              children: [],
              id: 'math-grade-6-a',
              title: '均一版',
            },
            {
              href: '/course-compare/math-6/n-m6a',
              children: [],
              id: 'n-m6a',
              title: '南一版',
            },
            {
              href: '/course-compare/math-6/h-m6a',
              children: [],
              id: 'h-m6a',
              title: '翰林版',
            },
            {
              href: '/course-compare/math-6/k-m6a',
              children: [],
              id: 'k-m6a',
              title: '康軒版',
            },
          ],
          id: 'math-6',
          title: '六年級',
        },
        {
          href: '/course-compare/math-7',
          children: [
            {
              href: '/course-compare/math-7/math-grade-7-a',
              children: [],
              id: 'math-grade-7-a',
              title: '均一版',
            },
            {
              href: '/course-compare/math-7/n-m7a',
              children: [],
              id: 'n-m7a',
              title: '南一版',
            },
            {
              href: '/course-compare/math-7/h-m7a',
              children: [],
              id: 'h-m7a',
              title: '翰林版',
            },
            {
              href: '/course-compare/math-7/k-m7a',
              children: [],
              id: 'k-m7a',
              title: '康軒版',
            },
          ],
          id: 'math-7',
          title: '七年級',
        },
        {
          href: '/course-compare/math-8',
          children: [
            {
              href: '/course-compare/math-8/math-grade-8-a',
              children: [],
              id: 'math-grade-8-a',
              title: '均一版',
            },
            {
              href: '/course-compare/math-8/n-m8a',
              children: [],
              id: 'n-m8a',
              title: '南一版',
            },
            {
              href: '/course-compare/math-8/h-m8a',
              children: [],
              id: 'h-m8a',
              title: '翰林版',
            },
            {
              href: '/course-compare/math-8/k-m8a',
              children: [],
              id: 'k-m8a',
              title: '康軒版',
            },
          ],
          id: 'math-8',
          title: '八年級',
        },
        {
          href: '/course-compare/math-9',
          children: [
            {
              href: '/course-compare/math-9/math-grade-9-a',
              children: [],
              id: 'math-grade-9-a',
              title: '均一版',
            },
            {
              href: '/course-compare/math-9/n-m9a',
              children: [],
              id: 'n-m9a',
              title: '南一版',
            },
            {
              href: '/course-compare/math-9/h-m9a',
              children: [],
              id: 'h-m9a',
              title: '翰林版',
            },
            {
              href: '/course-compare/math-9/k-m9a',
              children: [],
              id: 'k-m9a',
              title: '康軒版',
            },
          ],
          id: 'math-9',
          title: '九年級',
        },
        {
          href: '/course-compare/math-9to10',
          children: [
            {
              href: '/course-compare/math-9to10/jun-sen-u1',
              children: [],
              id: 'jun-sen-u1',
              title: '數與式',
            },
            {
              href: '/course-compare/math-9to10/jun-sen-u2',
              children: [],
              id: 'jun-sen-u2',
              title: '幾何',
            },
            {
              href: '/course-compare/math-9to10/jun-sen-u3',
              children: [],
              id: 'jun-sen-u3',
              title: '代數',
            },
            {
              href: '/course-compare/math-9to10/jun-sen-u4',
              children: [],
              id: 'jun-sen-u4',
              title: '機率與統計',
            },
          ],
          id: 'math-9to10',
          title: '國中升高中銜接',
        },
        {
          href: '/course-compare/math-10',
          children: [
            {
              href: '/course-compare/math-10/j-m10ach1',
              children: [],
              id: 'j-m10ach1',
              title: '108課綱【十上】一、實數的計算',
            },
            {
              href: '/course-compare/math-10/j-m10ach2',
              children: [],
              id: 'j-m10ach2',
              title: '108課綱【十上】二、直線',
            },
            {
              href: '/course-compare/math-10/j-m10ach3',
              children: [],
              id: 'j-m10ach3',
              title: '108課綱【十上】三、圓',
            },
            {
              href: '/course-compare/math-10/j-m10ach4',
              children: [],
              id: 'j-m10ach4',
              title: '108課綱【十上】四、多項式',
            },
            {
              href: '/course-compare/math-10/j-m10bch1',
              children: [],
              id: 'j-m10bch1',
              title: '108課綱【十下】一、三角函數',
            },
            {
              href: '/course-compare/math-10/j-m10bch2',
              children: [],
              id: 'j-m10bch2',
              title: '108課綱【十下】二、數列與級數',
            },
            {
              href: '/course-compare/math-10/j-m10bch3',
              children: [],
              id: 'j-m10bch3',
              title: '108課綱【十下】三、數據分析',
            },
            {
              href: '/course-compare/math-10/j-m10bch4',
              children: [],
              id: 'j-m10bch4',
              title: '108課綱【十下】四、排列組合',
            },
            {
              href: '/course-compare/math-10/j-m10bch5',
              children: [],
              id: 'j-m10bch5',
              title: '108課綱【十下】五、機率',
            },
            {
              href: '/course-compare/math-10/junyi-numbers-and-expressions-a',
              children: [],
              id: 'junyi-numbers-and-expressions-a',
              title: '99課綱【十】數與式',
            },
            {
              href: '/course-compare/math-10/junyi-polynomial-function-a',
              children: [],
              id: 'junyi-polynomial-function-a',
              title: '99課綱【十】多項式函數',
            },
            {
              href: '/course-compare/math-10/junyi-exponential-log-functions1',
              children: [],
              id: 'junyi-exponential-log-functions1',
              title: '99課綱【十】指數與對數函數',
            },
            {
              href: '/course-compare/math-10/sequences-and-series-a',
              children: [],
              id: 'sequences-and-series-a',
              title: '99課綱【十】數列與級數',
            },
            {
              href: '/course-compare/math-10/permutations-and-combinations_a',
              children: [],
              id: 'permutations-and-combinations_a',
              title: '99課綱【十】排列組合',
            },
            {
              href: '/course-compare/math-10/high-school-probability-a',
              children: [],
              id: 'high-school-probability-a',
              title: '99課綱【十】機率',
            },
            {
              href: '/course-compare/math-10/high-school-statistic-a',
              children: [],
              id: 'high-school-statistic-a',
              title: '99課綱【十】統計',
            },
          ],
          id: 'math-10',
          title: '十年級',
        },
        {
          href: '/course-compare/math-11',
          children: [
            {
              href: '/course-compare/math-11/j-m11a-a-ch1',
              children: [],
              id: 'j-m11a-a-ch1',
              title: '108課綱【十一上A類】一、三角函數',
            },
            {
              href: '/course-compare/math-11/j-m11a-a-ch2',
              children: [],
              id: 'j-m11a-a-ch2',
              title: '108課綱【十一上A類】二、指對數函數',
            },
            {
              href: '/course-compare/math-11/j-m11a-a-ch3',
              children: [],
              id: 'j-m11a-a-ch3',
              title: '108課綱【十一上A類】三、平面向量',
            },
            {
              href: '/course-compare/math-11/j-m11a-b-ch1',
              children: [],
              id: 'j-m11a-b-ch1',
              title: '108課綱【十一上B類】一、平面幾何與設計',
            },
            {
              href: '/course-compare/math-11/j-m11a-b-ch2',
              children: [],
              id: 'j-m11a-b-ch2',
              title: '108課綱【十一上B類】二、按比例成長',
            },
            {
              href: '/course-compare/math-11/j-m11a-b-ch3',
              children: [],
              id: 'j-m11a-b-ch3',
              title: '108課綱【十一上B類】三、週期性現象',
            },
            {
              href: '/course-compare/math-11/j-m11b-a-ch1',
              children: [],
              id: 'j-m11b-a-ch1',
              title: '108課綱【十一下A類】一、矩陣',
            },
            {
              href: '/course-compare/math-11/j-m11b-a-ch2',
              children: [],
              id: 'j-m11b-a-ch2',
              title: '108課綱【十一下A類】二、空間中的平面與直線',
            },
            {
              href: '/course-compare/math-11/j-m11b-b-ch1',
              children: [],
              id: 'j-m11b-b-ch1',
              title: '108課綱【十一下B類】一、空間概念與圖形',
            },
            {
              href: '/course-compare/math-11/high-school-triangle',
              children: [],
              id: 'high-school-triangle',
              title: '99課綱【十一】三角',
            },
            {
              href: '/course-compare/math-11/g11-m5spx',
              children: [],
              id: 'g11-m5spx',
              title: '99課綱【十一】平面向量',
            },
            {
              href: '/course-compare/math-11/msskx',
              children: [],
              id: 'msskx',
              title: '99課綱【十一】空間向量',
            },
            {
              href: '/course-compare/math-11/high-school-quadratic-curve',
              children: [],
              id: 'high-school-quadratic-curve',
              title: '99課綱【十一】二次曲線',
            },
          ],
          id: 'math-11',
          title: '十一年級',
        },
        {
          href: '/course-compare/math-12',
          children: [
            {
              href: '/course-compare/math-12/highschool-probability-statistic',
              children: [],
              id: 'highschool-probability-statistic',
              title: '99課綱【十二】機率與統計',
            },
          ],
          id: 'math-12',
          title: '十二年級',
        },
        {
          href: '/course-compare/math-calculus',
          children: [
            {
              href: '/course-compare/math-calculus/mu-cal-ch1',
              children: [],
              id: 'mu-cal-ch1',
              title: '微積分概論',
            },
            {
              href: '/course-compare/math-calculus/shann-wei-chang-calculus',
              children: [],
              id: 'shann-wei-chang-calculus',
              title: '單維彰 - 大學入門之「微積分」',
            },
            {
              href: '/course-compare/math-calculus/feng-chia-calculus',
              children: [],
              id: 'feng-chia-calculus',
              title: '逢甲大學微積分課程',
            },
          ],
          id: 'math-calculus',
          title: '微積分',
        },
        {
          href: '/course-compare/math-linear-algebra',
          children: [
            {
              href: '/course-compare/math-linear-algebra/junyi-matrix_1',
              children: [],
              id: 'junyi-matrix_1',
              title: '矩陣',
            },
            {
              href:
                '/course-compare/math-linear-algebra/junyi-linear-algebra01_01',
              children: [],
              id: 'junyi-linear-algebra01_01',
              title: '用感覺學線代01',
            },
          ],
          id: 'math-linear-algebra',
          title: '線性代數',
        },
      ],
      id: 'course-compare',
      title: '數學',
    },
    {
      href: '/v99_tree',
      children: [
        {
          href: '/v99_tree/v99_g01',
          children: [
            {
              href: '/v99_tree/v99_g01/v99_1-n-01',
              children: [],
              id: 'v99_1-n-01',
              title:
                '1-n-01 能認識100以內的數及「個位」、「十位」的位名，並進行位值單位的換算。',
            },
            {
              href: '/v99_tree/v99_g01/v99_1-n-02',
              children: [],
              id: 'v99_1-n-02',
              title:
                '1-n-02 能認識1元、5元、10元等錢幣幣值，並做1元與10元錢幣的換算。',
            },
            {
              href: '/v99_tree/v99_g01/v99_1-n-03',
              children: [],
              id: 'v99_1-n-03',
              title: '1-n-03 能運用數表達多少、大小、順序。',
            },
            {
              href: '/v99_tree/v99_g01/v99_1-n-04',
              children: [],
              id: 'v99_1-n-04',
              title:
                '1-n-04 能從合成、分解的活動中，理解加減法的意義，使用＋、－、＝做橫式紀錄與直式紀錄，並解決生活中的問題。',
            },
            {
              href: '/v99_tree/v99_g01/v99_1-n-05',
              children: [],
              id: 'v99_1-n-05',
              title: '1-n-05 能熟練基本加減法。',
            },
            {
              href: '/v99_tree/v99_g01/v99_1-n-06',
              children: [],
              id: 'v99_1-n-06',
              title: '1-n-06 能做一位數之連加、連減與加減混合計算。',
            },
            {
              href: '/v99_tree/v99_g01/v99_1-n-07',
              children: [],
              id: 'v99_1-n-07',
              title: '1-n-07 能進行2個一數、5個一數、10個一數等活動。',
            },
            {
              href: '/v99_tree/v99_g01/v99_1-n-08',
              children: [],
              id: 'v99_1-n-08',
              title:
                '1-n-08 能認識常用時間用語，並報讀日期與鐘面上整點、半點的時刻。',
            },
            {
              href: '/v99_tree/v99_g01/v99_1-n-09',
              children: [],
              id: 'v99_1-n-09',
              title: '1-n-09 能認識長度，並做直接比較。',
            },
            {
              href: '/v99_tree/v99_g01/v99_1-n-10',
              children: [],
              id: 'v99_1-n-10',
              title:
                '1-n-10 能利用間接比較或以個別單位實測的方法比較物體的長短。',
            },
            {
              href: '/v99_tree/v99_g01/v99_1-s-01',
              children: [],
              id: 'v99_1-s-01',
              title: '1-s-01 能認識直線與曲線。',
            },
            {
              href: '/v99_tree/v99_g01/v99_1-s-02',
              children: [],
              id: 'v99_1-s-02',
              title: '1-s-02 能辨認、描述與分類簡單平面圖形與立體形體。',
            },
            {
              href: '/v99_tree/v99_g01/v99_1-s-03',
              children: [],
              id: 'v99_1-s-03',
              title: '1-s-03 能描繪或仿製簡單平面圖形。',
            },
            {
              href: '/v99_tree/v99_g01/v99_1-s-04',
              children: [],
              id: 'v99_1-s-04',
              title: '1-s-04 能依給定圖示，將簡單形體做平面舖設與立體堆疊。',
            },
            {
              href: '/v99_tree/v99_g01/v99_1-a-01',
              children: [],
              id: 'v99_1-a-01',
              title: '1-a-01 能在具體情境中，認識加法的交換律。',
            },
            {
              href: '/v99_tree/v99_g01/v99_1-a-02',
              children: [],
              id: 'v99_1-a-02',
              title: '1-a-02 能在具體情境中，認識加減互逆。',
            },
            {
              href: '/v99_tree/v99_g01/v99_1-d-01',
              children: [],
              id: 'v99_1-d-01',
              title: '1-d-01 能對生活中的事件或活動做初步的分類與記錄。',
            },
            {
              href: '/v99_tree/v99_g01/v99_1-d-02',
              children: [],
              id: 'v99_1-d-02',
              title: '1-d-02 能將紀錄以統計表呈現並說明。',
            },
          ],
          id: 'v99_g01',
          title: '一年級',
        },
        {
          href: '/v99_tree/v99_g02',
          children: [
            {
              href: '/v99_tree/v99_g02/v99_2-n-01',
              children: [],
              id: 'v99_2-n-01',
              title:
                '2-n-01 能認識1000以內的數及「百位」的位名，並進行位值單位換算。',
            },
            {
              href: '/v99_tree/v99_g02/v99_2-n-02',
              children: [],
              id: 'v99_2-n-02',
              title: '2-n-02 能認識100元的幣值，並做10元與100元錢幣的換算。',
            },
            {
              href: '/v99_tree/v99_g02/v99_2-n-03',
              children: [],
              id: 'v99_2-n-03',
              title:
                '2-n-03 能用＜、＝與＞表示數量大小關係，並在具體情境中認識遞移律。(同2-a-01)',
            },
            {
              href: '/v99_tree/v99_g02/v99_2-n-04',
              children: [],
              id: 'v99_2-n-04',
              title: '2-n-04 能熟練二位數加減直式計算。',
            },
            {
              href: '/v99_tree/v99_g02/v99_2-n-05',
              children: [],
              id: 'v99_2-n-05',
              title: '2-n-05 能理解三位數加減直式計算(不含兩次退位)。',
            },
            {
              href: '/v99_tree/v99_g02/v99_2-n-06',
              children: [],
              id: 'v99_2-n-06',
              title:
                '2-n-06 能理解乘法的意義，使用×、＝做橫式紀錄與直式紀錄，並解決生活中的問題。',
            },
            {
              href: '/v99_tree/v99_g02/v99_2-n-07',
              children: [],
              id: 'v99_2-n-07',
              title: '2-n-07 能在具體情境中，進行分裝與平分的活動。',
            },
            {
              href: '/v99_tree/v99_g02/v99_2-n-08',
              children: [],
              id: 'v99_2-n-08',
              title: '2-n-08 能理解九九乘法。',
            },
            {
              href: '/v99_tree/v99_g02/v99_2-n-09',
              children: [],
              id: 'v99_2-n-09',
              title:
                '2-n-09 能在具體情境中，解決兩步驟問題(加與減，不含併式)。',
            },
            {
              href: '/v99_tree/v99_g02/v99_2-n-10',
              children: [],
              id: 'v99_2-n-10',
              title:
                '2-n-10 能在具體情境中，解決兩步驟問題(加、減與乘，不含併式)。',
            },
            {
              href: '/v99_tree/v99_g02/v99_2-n-11',
              children: [],
              id: 'v99_2-n-11',
              title: '2-n-11 能做簡單的二位數加減估算。',
            },
            {
              href: '/v99_tree/v99_g02/v99_2-n-12',
              children: [],
              id: 'v99_2-n-12',
              title: '2-n-12 能認識鐘面上的時刻是幾點幾分。',
            },
            {
              href: '/v99_tree/v99_g02/v99_2-n-13',
              children: [],
              id: 'v99_2-n-13',
              title:
                '2-n-13 能認識「年」、「月」、「星期」、「日」，並知道「某月有幾日」、「一星期有七天」。',
            },
            {
              href: '/v99_tree/v99_g02/v99_2-n-14',
              children: [],
              id: 'v99_2-n-14',
              title:
                '2-n-14 能理解用不同個別單位測量同一長度時，其數值不同，並能說明原因。',
            },
            {
              href: '/v99_tree/v99_g02/v99_2-n-15',
              children: [],
              id: 'v99_2-n-15',
              title:
                '2-n-15 能認識長度單位「公分」、「公尺」及其關係，並能做相關的實測、估測與同單位的計算。',
            },
            {
              href: '/v99_tree/v99_g02/v99_2-n-16',
              children: [],
              id: 'v99_2-n-16',
              title: '2-n-16 能認識容量。',
            },
            {
              href: '/v99_tree/v99_g02/v99_2-n-17',
              children: [],
              id: 'v99_2-n-17',
              title: '2-n-17 能認識重量。',
            },
            {
              href: '/v99_tree/v99_g02/v99_2-n-18',
              children: [],
              id: 'v99_2-n-18',
              title: '2-n-18 能認識面積。(同2-s-04)',
            },
            {
              href: '/v99_tree/v99_g02/v99_2-s-01',
              children: [],
              id: 'v99_2-s-01',
              title:
                '2-s-01 能認識周遭物體上的角、直線與平面(含簡單立體形體)。',
            },
            {
              href: '/v99_tree/v99_g02/v99_2-s-02',
              children: [],
              id: 'v99_2-s-02',
              title: '2-s-02 能認識生活周遭中平行與垂直的現象。',
            },
            {
              href: '/v99_tree/v99_g02/v99_2-s-03',
              children: [],
              id: 'v99_2-s-03',
              title: '2-s-03 能使用直尺處理與線段有關的問題。',
            },
            {
              href: '/v99_tree/v99_g02/v99_2-s-04',
              children: [],
              id: 'v99_2-s-04',
              title: '2-s-04 能認識面積。(同2-n-18)',
            },
            {
              href: '/v99_tree/v99_g02/v99_2-s-05',
              children: [],
              id: 'v99_2-s-05',
              title: '2-s-05 認識簡單平面圖形的邊長關係。',
            },
            {
              href: '/v99_tree/v99_g02/v99_2-a-01',
              children: [],
              id: 'v99_2-a-01',
              title:
                '2-a-01 能用＜、＝與＞表示數量大小關係，並在具體情境中認識遞移律。(同2-n-03)',
            },
            {
              href: '/v99_tree/v99_g02/v99_2-a-02',
              children: [],
              id: 'v99_2-a-02',
              title:
                '2-a-02 能在具體情境中，認識加法順序改變並不影響其和的性質。',
            },
            {
              href: '/v99_tree/v99_g02/v99_2-a-03',
              children: [],
              id: 'v99_2-a-03',
              title: '2-a-03 能在具體情境中，認識乘法交換律。',
            },
            {
              href: '/v99_tree/v99_g02/v99_2-a-04',
              children: [],
              id: 'v99_2-a-04',
              title: '2-a-04 能理解加減互逆，並運用於驗算與解題。',
            },
          ],
          id: 'v99_g02',
          title: '二年級',
        },
        {
          href: '/v99_tree/v99_g03',
          children: [
            {
              href: '/v99_tree/v99_g03/v99_3-n-01',
              children: [],
              id: 'v99_3-n-01',
              title:
                '3-n-01 能認識10000以內的數及「千位」的位名，並進行位值單位換算。',
            },
            {
              href: '/v99_tree/v99_g03/v99_3-n-02',
              children: [],
              id: 'v99_3-n-02',
              title:
                '3-n-02 能熟練加減直式計算(四位數以內，和＜10000，含多重退位)。',
            },
            {
              href: '/v99_tree/v99_g03/v99_3-n-03',
              children: [],
              id: 'v99_3-n-03',
              title: '3-n-03 能用併式記錄加減兩步驟的問題。',
            },
            {
              href: '/v99_tree/v99_g03/v99_3-n-04',
              children: [],
              id: 'v99_3-n-04',
              title: '3-n-04 能熟練三位數乘以一位數的直式計算。',
            },
            {
              href: '/v99_tree/v99_g03/v99_3-n-05',
              children: [],
              id: 'v99_3-n-05',
              title:
                '3-n-05 能理解除法的意義，運用÷、＝做橫式紀錄(包括有餘數的情況)，並解決生活中的問題。',
            },
            {
              href: '/v99_tree/v99_g03/v99_3-n-06',
              children: [],
              id: 'v99_3-n-06',
              title: '3-n-06 能熟練三位數除以一位數的直式計算。',
            },
            {
              href: '/v99_tree/v99_g03/v99_3-n-07',
              children: [],
              id: 'v99_3-n-07',
              title:
                '3-n-07 能在具體情境中，解決兩步驟問題(加、減與除，不含併式)。',
            },
            {
              href: '/v99_tree/v99_g03/v99_3-n-08',
              children: [],
              id: 'v99_3-n-08',
              title: '3-n-08 能在具體情境中，解決兩步驟問題(連乘，不含併式)。',
            },
            {
              href: '/v99_tree/v99_g03/v99_3-n-09',
              children: [],
              id: 'v99_3-n-09',
              title:
                '3-n-09 能由長度測量的經驗來認識數線，標記整數值與一位小數，並在數線上做大小比較、加、減的操作。',
            },
            {
              href: '/v99_tree/v99_g03/v99_3-n-10',
              children: [],
              id: 'v99_3-n-10',
              title: '3-n-10 能做簡單的三位數加減估算。',
            },
            {
              href: '/v99_tree/v99_g03/v99_3-n-11',
              children: [],
              id: 'v99_3-n-11',
              title:
                '3-n-11 能在具體情境中，初步認識分數，並解決同分母分數的比較與加減問題。',
            },
            {
              href: '/v99_tree/v99_g03/v99_3-n-12',
              children: [],
              id: 'v99_3-n-12',
              title: '3-n-12 能認識一位小數，並做比較與加減計算。',
            },
            {
              href: '/v99_tree/v99_g03/v99_3-n-13',
              children: [],
              id: 'v99_3-n-13',
              title:
                '3-n-13 能認識時間單位「日」、「時」、「分」、「秒」及其間的關係，並做同單位時間量及時、分複名數的加減計算（不進、退位）。',
            },
            {
              href: '/v99_tree/v99_g03/v99_3-n-14',
              children: [],
              id: 'v99_3-n-14',
              title:
                '3-n-14 能認識長度單位「毫米」及「公尺」、「公分」、「毫米」間的關係，並做相關的實測、估測與計算。',
            },
            {
              href: '/v99_tree/v99_g03/v99_3-n-15',
              children: [],
              id: 'v99_3-n-15',
              title:
                '3-n-15 能認識容量單位「公升」、「毫公升」(簡稱「毫升」)及其關係，並做相關的實測、估測與計算。',
            },
            {
              href: '/v99_tree/v99_g03/v99_3-n-16',
              children: [],
              id: 'v99_3-n-16',
              title:
                '3-n-16 能認識重量單位「公斤」、「公克」及其關係，並做相關的實測、估測與計算。',
            },
            {
              href: '/v99_tree/v99_g03/v99_3-n-17',
              children: [],
              id: 'v99_3-n-17',
              title: '3-n-17 能認識角，並比較角的大小。(同3-s-04)',
            },
            {
              href: '/v99_tree/v99_g03/v99_3-n-18',
              children: [],
              id: 'v99_3-n-18',
              title:
                '3-n-18 能認識面積單位「平方公分」，並做相關的實測與計算。(同3-s-05)',
            },
            {
              href: '/v99_tree/v99_g03/v99_3-s-01',
              children: [],
              id: 'v99_3-s-01',
              title: '3-s-01 能認識平面圖形的內部、外部與其周界。',
            },
            {
              href: '/v99_tree/v99_g03/v99_3-s-02',
              children: [],
              id: 'v99_3-s-02',
              title: '3-s-02 能認識周長，並實測周長。',
            },
            {
              href: '/v99_tree/v99_g03/v99_3-s-03',
              children: [],
              id: 'v99_3-s-03',
              title:
                '3-s-03 能使用圓規畫圓，認識圓的「圓心」、「圓周」、「半徑」與「直徑」。',
            },
            {
              href: '/v99_tree/v99_g03/v99_3-s-04',
              children: [],
              id: 'v99_3-s-04',
              title: '3-s-04 能認識角，並比較角的大小。(同3-n-17)',
            },
            {
              href: '/v99_tree/v99_g03/v99_3-s-05',
              children: [],
              id: 'v99_3-s-05',
              title:
                '3-s-05 能認識面積單位「平方公分」，並做相關的實測與計算。(同3-n-18)',
            },
            {
              href: '/v99_tree/v99_g03/v99_3-s-06',
              children: [],
              id: 'v99_3-s-06',
              title:
                '3-s-06 能透過操作，將簡單圖形切割重組成另一已知簡單圖形。',
            },
            {
              href: '/v99_tree/v99_g03/v99_3-s-07',
              children: [],
              id: 'v99_3-s-07',
              title: '3-s-07 能由邊長和角的特性來認識正方形和長方形。',
            },
            {
              href: '/v99_tree/v99_g03/v99_3-a-01',
              children: [],
              id: 'v99_3-a-01',
              title: '3-a-01 能理解乘除互逆，並運用於驗算及解題。',
            },
            {
              href: '/v99_tree/v99_g03/v99_3-d-01',
              children: [],
              id: 'v99_3-d-01',
              title: '3-d-01 能報讀生活中常見的表格。',
            },
          ],
          id: 'v99_g03',
          title: '三年級',
        },
        {
          href: '/v99_tree/v99_g04',
          children: [
            {
              href: '/v99_tree/v99_g04/v99_4-n-01',
              children: [],
              id: 'v99_4-n-01',
              title:
                '4-n-01 能透過位值概念，延伸整數的認識到大數(含「億」、「兆」之位名)，並做位值單位的換算。',
            },
            {
              href: '/v99_tree/v99_g04/v99_4-n-02',
              children: [],
              id: 'v99_4-n-02',
              title: '4-n-02 能熟練整數加、減的直式計算。',
            },
            {
              href: '/v99_tree/v99_g04/v99_4-n-03',
              children: [],
              id: 'v99_4-n-03',
              title: '4-n-03 能熟練較大位數的乘除直式計算。',
            },
            {
              href: '/v99_tree/v99_g04/v99_4-n-04',
              children: [],
              id: 'v99_4-n-04',
              title:
                '4-n-04 能在具體情境中，解決兩步驟問題，並學習併式的記法與計算。',
            },
            {
              href: '/v99_tree/v99_g04/v99_4-n-05',
              children: [],
              id: 'v99_4-n-05',
              title: '4-n-05 能做整數四則混合計算(兩步驟)。',
            },
            {
              href: '/v99_tree/v99_g04/v99_4-n-06',
              children: [],
              id: 'v99_4-n-06',
              title:
                '4-n-06 能在具體情境中，對大數在指定位數取概數(含四捨五入法)，並做加、減之估算。',
            },
            {
              href: '/v99_tree/v99_g04/v99_4-n-07',
              children: [],
              id: 'v99_4-n-07',
              title: '4-n-07 能理解分數之「整數相除」的意涵。',
            },
            {
              href: '/v99_tree/v99_g04/v99_4-n-08',
              children: [],
              id: 'v99_4-n-08',
              title:
                '4-n-08 能認識真分數、假分數與帶分數，熟練假分數與帶分數的互換，並進行同分母分數的比較、加、減與整數倍的計算。',
            },
            {
              href: '/v99_tree/v99_g04/v99_4-n-09',
              children: [],
              id: 'v99_4-n-09',
              title:
                '4-n-09 能認識等值分數，進行簡單異分母分數的比較，並用來做簡單分數與小數的互換。',
            },
            {
              href: '/v99_tree/v99_g04/v99_4-n-10',
              children: [],
              id: 'v99_4-n-10',
              title: '4-n-10 能將簡單分數標記在數線上。',
            },
            {
              href: '/v99_tree/v99_g04/v99_4-n-11',
              children: [],
              id: 'v99_4-n-11',
              title: '4-n-11 能認識二位小數與百分位的位名，並做比較。',
            },
            {
              href: '/v99_tree/v99_g04/v99_4-n-12',
              children: [],
              id: 'v99_4-n-12',
              title:
                '4-n-12 能用直式處理二位小數加、減與整數倍的計算，並解決生活中的問題。',
            },
            {
              href: '/v99_tree/v99_g04/v99_4-n-13',
              children: [],
              id: 'v99_4-n-13',
              title: '4-n-13 能解決複名數的時間量的計算問題。',
            },
            {
              href: '/v99_tree/v99_g04/v99_4-n-14',
              children: [],
              id: 'v99_4-n-14',
              title: '4-n-14 能以複名數解決量(長度、容量、重量)的計算問題。',
            },
            {
              href: '/v99_tree/v99_g04/v99_4-n-15',
              children: [],
              id: 'v99_4-n-15',
              title:
                '4-n-15 能認識長度單位「公里」，及「公里」與其他長度單位的關係，並做相關計算。',
            },
            {
              href: '/v99_tree/v99_g04/v99_4-n-16',
              children: [],
              id: 'v99_4-n-16',
              title:
                '4-n-16 能認識角度單位「度」，並使用量角器實測角度或畫出指定的角。(同4-s-04)',
            },
            {
              href: '/v99_tree/v99_g04/v99_4-n-17',
              children: [],
              id: 'v99_4-n-17',
              title:
                '4-n-17 能認識面積單位「平方公尺」，及「平方公分」、「平方公尺」間的關係，並做相關計算。',
            },
            {
              href: '/v99_tree/v99_g04/v99_4-n-18',
              children: [],
              id: 'v99_4-n-18',
              title:
                '4-n-18 能理解長方形和正方形的面積公式與周長公式。(同4-s-09)',
            },
            {
              href: '/v99_tree/v99_g04/v99_4-n-19',
              children: [],
              id: 'v99_4-n-19',
              title: '4-n-19 能認識體積及體積單位「立方公分」。',
            },
            {
              href: '/v99_tree/v99_g04/v99_4-s-01',
              children: [],
              id: 'v99_4-s-01',
              title:
                '4-s-01 能運用「角」與「邊」等構成要素，辨認簡單平面圖形。',
            },
            {
              href: '/v99_tree/v99_g04/v99_4-s-02',
              children: [],
              id: 'v99_4-s-02',
              title: '4-s-02 能透過操作，認識基本三角形與四邊形的簡單性質。',
            },
            {
              href: '/v99_tree/v99_g04/v99_4-s-03',
              children: [],
              id: 'v99_4-s-03',
              title: '4-s-03 能認識平面圖形全等的意義。',
            },
            {
              href: '/v99_tree/v99_g04/v99_4-s-04',
              children: [],
              id: 'v99_4-s-04',
              title:
                '4-s-04 能認識「度」的角度單位，使用量角器實測角度或畫出指定的角。(同4-n-16)',
            },
            {
              href: '/v99_tree/v99_g04/v99_4-s-05',
              children: [],
              id: 'v99_4-s-05',
              title: '4-s-05 能理解旋轉角(包括平角和周角)的意義。',
            },
            {
              href: '/v99_tree/v99_g04/v99_4-s-06',
              children: [],
              id: 'v99_4-s-06',
              title: '4-s-06 能理解平面上直角、垂直與平行的意義。',
            },
            {
              href: '/v99_tree/v99_g04/v99_4-s-07',
              children: [],
              id: 'v99_4-s-07',
              title: '4-s-07 能認識平行四邊形和梯形。',
            },
            {
              href: '/v99_tree/v99_g04/v99_4-s-08',
              children: [],
              id: 'v99_4-s-08',
              title:
                '4-s-08 能利用三角板畫出直角與兩平行線段，並用來描繪平面圖形。',
            },
            {
              href: '/v99_tree/v99_g04/v99_4-s-09',
              children: [],
              id: 'v99_4-s-09',
              title:
                '4-s-09 能理解長方形和正方形的面積公式與周長公式。(同4-n-18)',
            },
            {
              href: '/v99_tree/v99_g04/v99_4-a-01',
              children: [],
              id: 'v99_4-a-01',
              title: '4-a-01 能在具體情境中，理解乘法結合律。',
            },
            {
              href: '/v99_tree/v99_g04/v99_4-a-02',
              children: [],
              id: 'v99_4-a-02',
              title: '4-a-02 能在四則混合計算中，運用數的運算性質。',
            },
            {
              href: '/v99_tree/v99_g04/v99_4-d-01',
              children: [],
              id: 'v99_4-d-01',
              title: '4-d-01 能報讀生活中常用的長條圖。',
            },
            {
              href: '/v99_tree/v99_g04/v99_4-d-02',
              children: [],
              id: 'v99_4-d-02',
              title: '4-d-02 能報讀生活中常用的折線圖。',
            },
          ],
          id: 'v99_g04',
          title: '四年級',
        },
        {
          href: '/v99_tree/v99_g05',
          children: [
            {
              href: '/v99_tree/v99_g05/v99_5-n-01',
              children: [],
              id: 'v99_5-n-01',
              title: '5-n-01 能熟練整數乘、除的直式計算。',
            },
            {
              href: '/v99_tree/v99_g05/v99_5-n-02',
              children: [],
              id: 'v99_5-n-02',
              title: '5-n-02 能在具體情境中，解決三步驟問題，並能併式計算。',
            },
            {
              href: '/v99_tree/v99_g05/v99_5-n-03',
              children: [],
              id: 'v99_5-n-03',
              title: '5-n-03 能熟練整數四則混合計算。',
            },
            {
              href: '/v99_tree/v99_g05/v99_5-n-04',
              children: [],
              id: 'v99_5-n-04',
              title: '5-n-04 能理解因數和倍數。',
            },
            {
              href: '/v99_tree/v99_g05/v99_5-n-05',
              children: [],
              id: 'v99_5-n-05',
              title:
                '5-n-05 能認識兩數的公因數、公倍數、最大公因數與最小公倍數。',
            },
            {
              href: '/v99_tree/v99_g05/v99_5-n-06',
              children: [],
              id: 'v99_5-n-06',
              title: '5-n-06 能用約分、擴分處理等值分數的換算。',
            },
            {
              href: '/v99_tree/v99_g05/v99_5-n-07',
              children: [],
              id: 'v99_5-n-07',
              title: '5-n-07 能用通分做簡單異分母分數的比較與加減。',
            },
            {
              href: '/v99_tree/v99_g05/v99_5-n-08',
              children: [],
              id: 'v99_5-n-08',
              title:
                '5-n-08 能理解分數乘法的意義，並熟練其計算，解決生活中的問題。',
            },
            {
              href: '/v99_tree/v99_g05/v99_5-n-09',
              children: [],
              id: 'v99_5-n-09',
              title:
                '5-n-09 能理解除數為整數的分數除法的意義，並解決生活中的問題。',
            },
            {
              href: '/v99_tree/v99_g05/v99_5-n-10',
              children: [],
              id: 'v99_5-n-10',
              title:
                '5-n-10 能認識多位小數，並做比較與加、減與整數倍的計算，以及解決生活中的問題。',
            },
            {
              href: '/v99_tree/v99_g05/v99_5-n-11',
              children: [],
              id: 'v99_5-n-11',
              title:
                '5-n-11 能用直式處理乘數是小數的計算，並解決生活中的問題。',
            },
            {
              href: '/v99_tree/v99_g05/v99_5-n-12',
              children: [],
              id: 'v99_5-n-12',
              title: '5-n-12 能用直式處理整數除以整數，商為三位小數的計算。',
            },
            {
              href: '/v99_tree/v99_g05/v99_5-n-13',
              children: [],
              id: 'v99_5-n-13',
              title: '5-n-13 能將分數、小數標記在數線上。',
            },
            {
              href: '/v99_tree/v99_g05/v99_5-n-14',
              children: [],
              id: 'v99_5-n-14',
              title:
                '5-n-14 能認識比率及其在生活中的應用(含「百分率」、「折」)。',
            },
            {
              href: '/v99_tree/v99_g05/v99_5-n-15',
              children: [],
              id: 'v99_5-n-15',
              title: '5-n-15 能解決時間的乘除計算問題。',
            },
            {
              href: '/v99_tree/v99_g05/v99_5-n-16',
              children: [],
              id: 'v99_5-n-16',
              title:
                '5-n-16 能認識重量單位「公噸」、「公噸」及「公斤」間的關係，並做相關計算。',
            },
            {
              href: '/v99_tree/v99_g05/v99_5-n-17',
              children: [],
              id: 'v99_5-n-17',
              title:
                '5-n-17 能認識面積單位「公畝」、「公頃」、「平方公里」及其關係，並做相關計算。',
            },
            {
              href: '/v99_tree/v99_g05/v99_5-n-18',
              children: [],
              id: 'v99_5-n-18',
              title:
                '5-n-18 能運用切割重組，理解三角形、平行四邊形與梯形的面積公式。(同5-s-05)',
            },
            {
              href: '/v99_tree/v99_g05/v99_5-n-19',
              children: [],
              id: 'v99_5-n-19',
              title:
                '5-n-19 能認識體積單位「立方公尺」、「立方公分」及「立方公尺」間的關係，並做相關計算。',
            },
            {
              href: '/v99_tree/v99_g05/v99_5-n-20',
              children: [],
              id: 'v99_5-n-20',
              title:
                '5-n-20 能理解長方體和正方體體積的計算公式，並能求出長方體和正方體的表面積。(同5-s-07)',
            },
            {
              href: '/v99_tree/v99_g05/v99_5-n-21',
              children: [],
              id: 'v99_5-n-21',
              title: '5-n-21 能理解容量、容積和體積間的關係。',
            },
            {
              href: '/v99_tree/v99_g05/v99_5-s-01',
              children: [],
              id: 'v99_5-s-01',
              title: '5-s-01 能透過操作，理解三角形三內角和為180度。',
            },
            {
              href: '/v99_tree/v99_g05/v99_5-s-02',
              children: [],
              id: 'v99_5-s-02',
              title: '5-s-02 能透過操作，理解三角形任意兩邊和大於第三邊。',
            },
            {
              href: '/v99_tree/v99_g05/v99_5-s-03',
              children: [],
              id: 'v99_5-s-03',
              title: '5-s-03 能認識圓心角，並認識扇形。',
            },
            {
              href: '/v99_tree/v99_g05/v99_5-s-04',
              children: [],
              id: 'v99_5-s-04',
              title: '5-s-04 能認識線對稱與簡單平面圖形的線對稱性質。',
            },
            {
              href: '/v99_tree/v99_g05/v99_5-s-05',
              children: [],
              id: 'v99_5-s-05',
              title:
                '5-s-05 能運用切割重組，理解三角形、平行四邊形與梯形的面積公式。(同5-n-18)',
            },
            {
              href: '/v99_tree/v99_g05/v99_5-s-06',
              children: [],
              id: 'v99_5-s-06',
              title: '5-s-06 能認識球、直圓柱、直圓錐、直角柱與正角錐。',
            },
            {
              href: '/v99_tree/v99_g05/v99_5-s-07',
              children: [],
              id: 'v99_5-s-07',
              title:
                '5-s-07 能理解長方體和正方體體積的計算公式，並能求出長方體和正方體的表面積。(同5-n-20)',
            },
            {
              href: '/v99_tree/v99_g05/v99_5-a-01',
              children: [],
              id: 'v99_5-a-01',
              title:
                '5-a-01 能在具體情境中，理解乘法對加法的分配律，並運用於簡化計算。',
            },
            {
              href: '/v99_tree/v99_g05/v99_5-a-02',
              children: [],
              id: 'v99_5-a-02',
              title:
                '5-a-02 能在具體情境中，理解先乘再除與先除再乘的結果相同，也理解連除兩數相當於除以此兩數之積。',
            },
            {
              href: '/v99_tree/v99_g05/v99_5-a-03',
              children: [],
              id: 'v99_5-a-03',
              title: '5-a-03 能熟練運用四則運算的性質，做整數四則混合計算。',
            },
            {
              href: '/v99_tree/v99_g05/v99_5-a-04',
              children: [],
              id: 'v99_5-a-04',
              title:
                '5-a-04 能將整數單步驟的具體情境問題列成含有未知數符號的算式，並能解釋算式、求解及驗算。',
            },
          ],
          id: 'v99_g05',
          title: '五年級',
        },
        {
          href: '/v99_tree/v99_g06',
          children: [
            {
              href: '/v99_tree/v99_g06/v99_6-n-01',
              children: [],
              id: 'v99_6-n-01',
              title:
                '6-n-01 能認識質數、合數，並用短除法做質因數的分解(質數＜20，質因數＜20，被分解數＜100)。',
            },
            {
              href: '/v99_tree/v99_g06/v99_6-n-02',
              children: [],
              id: 'v99_6-n-02',
              title: '6-n-02 能用短除法求兩數的最大公因數、最小公倍數。',
            },
            {
              href: '/v99_tree/v99_g06/v99_6-n-03',
              children: [],
              id: 'v99_6-n-03',
              title: '6-n-03 能認識兩數互質的意義，並將分數約成最簡分數。',
            },
            {
              href: '/v99_tree/v99_g06/v99_6-n-04',
              children: [],
              id: 'v99_6-n-04',
              title:
                '6-n-04 能理解分數除法的意義及熟練其計算，並解決生活中的問題。',
            },
            {
              href: '/v99_tree/v99_g06/v99_6-n-05',
              children: [],
              id: 'v99_6-n-05',
              title:
                '6-n-05 能在具體情境中，解決分數的兩步驟問題，並能併式計算。',
            },
            {
              href: '/v99_tree/v99_g06/v99_6-n-06',
              children: [],
              id: 'v99_6-n-06',
              title: '6-n-06 能用直式處理小數除法的計算，並解決生活中的問題。',
            },
            {
              href: '/v99_tree/v99_g06/v99_6-n-07',
              children: [],
              id: 'v99_6-n-07',
              title:
                '6-n-07 能在具體情境中，對整數及小數在指定位數取概數(含四捨五入法)，並做加、減、乘、除之估算。',
            },
            {
              href: '/v99_tree/v99_g06/v99_6-n-08',
              children: [],
              id: 'v99_6-n-08',
              title:
                '6-n-08 能在具體情境中，解決小數的兩步驟問題，並能併式計算。',
            },
            {
              href: '/v99_tree/v99_g06/v99_6-n-09',
              children: [],
              id: 'v99_6-n-09',
              title: '6-n-09 能認識比和比值，並解決生活中的問題。',
            },
            {
              href: '/v99_tree/v99_g06/v99_6-n-10',
              children: [],
              id: 'v99_6-n-10',
              title: '6-n-10 能理解正比的意義，並解決生活中的問題。',
            },
            {
              href: '/v99_tree/v99_g06/v99_6-n-11',
              children: [],
              id: 'v99_6-n-11',
              title: '6-n-11 能理解常用導出量單位的記法，並解決生活中的問題。',
            },
            {
              href: '/v99_tree/v99_g06/v99_6-n-12',
              children: [],
              id: 'v99_6-n-12',
              title: '6-n-12 能認識速度的意義及其常用單位。',
            },
            {
              href: '/v99_tree/v99_g06/v99_6-n-13',
              children: [],
              id: 'v99_6-n-13',
              title:
                '6-n-13 能利用常用的數量關係，列出恰當的算式，進行解題，並檢驗解的合理性。(同6-a-04)',
            },
            {
              href: '/v99_tree/v99_g06/v99_6-n-14',
              children: [],
              id: 'v99_6-n-14',
              title:
                '6-n-14 能理解圓面積與圓周長的公式，並計算簡單扇形的面積。。(同6-s-03)',
            },
            {
              href: '/v99_tree/v99_g06/v99_6-n-15',
              children: [],
              id: 'v99_6-n-15',
              title:
                '6-n-15 能理解簡單直柱體的體積為底面積與高的乘積。(同6-s-05)',
            },
            {
              href: '/v99_tree/v99_g06/v99_6-s-01',
              children: [],
              id: 'v99_6-s-01',
              title: '6-s-01 能利用幾何形體的性質解決簡單的幾何問題。',
            },
            {
              href: '/v99_tree/v99_g06/v99_6-s-02',
              children: [],
              id: 'v99_6-s-02',
              title:
                '6-s-02 能認識平面圖形放大、縮小對長度、角度與面積的影響，並認識比例尺。',
            },
            {
              href: '/v99_tree/v99_g06/v99_6-s-03',
              children: [],
              id: 'v99_6-s-03',
              title:
                '6-s-03 能理解圓面積與圓周長的公式，並計算簡單扇形的面積。(同6-n-14)',
            },
            {
              href: '/v99_tree/v99_g06/v99_6-s-04',
              children: [],
              id: 'v99_6-s-04',
              title:
                '6-s-04 能認識面與面的平行與垂直，線與面的垂直，並描述正方體與長方體中面與面、線與面的關係。',
            },
            {
              href: '/v99_tree/v99_g06/v99_6-s-05',
              children: [],
              id: 'v99_6-s-05',
              title:
                '6-s-05 能理解簡單直柱體的體積為底面積與高的乘積。(同6-n-15)',
            },
            {
              href: '/v99_tree/v99_g06/v99_6-a-01',
              children: [],
              id: 'v99_6-a-01',
              title: '6-a-01 能理解等量公理。',
            },
            {
              href: '/v99_tree/v99_g06/v99_6-a-02',
              children: [],
              id: 'v99_6-a-02',
              title:
                '6-a-02 能將分數單步驟的具體情境問題列成含有未知數符號的算式，並求解及驗算。',
            },
            {
              href: '/v99_tree/v99_g06/v99_6-a-03',
              children: [],
              id: 'v99_6-a-03',
              title: '6-a-03 能用符號表示常用的公式。',
            },
            {
              href: '/v99_tree/v99_g06/v99_6-a-04',
              children: [],
              id: 'v99_6-a-04',
              title:
                '6-a-04 能利用常用的數量關係，列出恰當的算式，進行解題，並檢驗解的合理性。(同6-n-13)',
            },
            {
              href: '/v99_tree/v99_g06/v99_6-d-01',
              children: [],
              id: 'v99_6-d-01',
              title: '6-d-01 能整理生活中的資料，並製成長條圖。',
            },
            {
              href: '/v99_tree/v99_g06/v99_6-d-02',
              children: [],
              id: 'v99_6-d-02',
              title: '6-d-02 能整理生活中的有序資料，並繪製成折線圖。',
            },
            {
              href: '/v99_tree/v99_g06/v99_6-d-03',
              children: [],
              id: 'v99_6-d-03',
              title:
                '6-d-03 能報讀生活中常用的圓形圖，並能整理生活中的資料，製成圓形圖。',
            },
          ],
          id: 'v99_g06',
          title: '六年級',
        },
        {
          href: '/v99_tree/v99_g07',
          children: [
            {
              href: '/v99_tree/v99_g07/v99_7-n-01',
              children: [],
              id: 'v99_7-n-01',
              title: '7-n-01 能理解質數的意義,並認識 100 以內的質數。',
            },
            {
              href: '/v99_tree/v99_g07/v99_7-n-02',
              children: [],
              id: 'v99_7-n-02',
              title:
                '7-n-02 能理解因數、質因數、倍數、公因數、公倍數及互質的概念,並熟練質因數分解的計算方法。',
            },
            {
              href: '/v99_tree/v99_g07/v99_7-n-03',
              children: [],
              id: 'v99_7-n-03',
              title:
                '7-n-03 能以最大公因數、最小公倍數熟練約分、擴分、最簡分數及分數加減的計算。',
            },
            {
              href: '/v99_tree/v99_g07/v99_7-n-04',
              children: [],
              id: 'v99_7-n-04',
              title:
                '7-n-04 能認識負數,並能以「正、負」表徵生活中性質相反的量。',
            },
            {
              href: '/v99_tree/v99_g07/v99_7-n-05',
              children: [],
              id: 'v99_7-n-05',
              title: '7-n-05 能認識絕對值,並能利用絕對值比較負數的大小。',
            },
            {
              href: '/v99_tree/v99_g07/v99_7-n-06',
              children: [],
              id: 'v99_7-n-06',
              title:
                '7-n-06 能理解負數的特性並熟練數(含小數、分數)的四則混合 運算。',
            },
            {
              href: '/v99_tree/v99_g07/v99_7-n-07',
              children: [],
              id: 'v99_7-n-07',
              title: '7-n-07 能熟練數的運算規則。',
            },
            {
              href: '/v99_tree/v99_g07/v99_7-n-08',
              children: [],
              id: 'v99_7-n-08',
              title:
                '7-n-08 能理解數線,數線上兩點的距離公式,及能藉數線上數的位置驗證數的大小關係。',
            },
            {
              href: '/v99_tree/v99_g07/v99_7-n-09',
              children: [],
              id: 'v99_7-n-09',
              title: '7-n-09 能以不等式標示數的範圍或數線上任一線段的範圍。',
            },
            {
              href: '/v99_tree/v99_g07/v99_7-n-10',
              children: [],
              id: 'v99_7-n-10',
              title: '7-n-10 能理解指數為非負整數的次方,並能運用到算式中。',
            },
            {
              href: '/v99_tree/v99_g07/v99_7-n-11',
              children: [],
              id: 'v99_7-n-11',
              title: '7-n-11 能理解同底數的相乘或相除的指數律。',
            },
            {
              href: '/v99_tree/v99_g07/v99_7-n-12',
              children: [],
              id: 'v99_7-n-12',
              title: '7-n-12 能用科學記號表示法表達很大的數或很小的數。',
            },
            {
              href: '/v99_tree/v99_g07/v99_7-n-13',
              children: [],
              id: 'v99_7-n-13',
              title:
                '7-n-13 能理解比、比例式、正比、反比的意義,並能解決生活中有關比例的問題。',
            },
            {
              href: '/v99_tree/v99_g07/v99_7-n-14',
              children: [],
              id: 'v99_7-n-14',
              title: '7-n-14 能熟練比例式的基本運算。',
            },
            {
              href: '/v99_tree/v99_g07/v99_7-n-15',
              children: [],
              id: 'v99_7-n-15',
              title:
                '7-n-15 能理解連比、連比例式的意義,並能解決生活中有關連比例的問題。',
            },
            {
              href: '/v99_tree/v99_g07/v99_7-a-01',
              children: [],
              id: 'v99_7-a-01',
              title: '7-a-01 能熟練符號的意義,及其代數運算。',
            },
            {
              href: '/v99_tree/v99_g07/v99_7-a-02',
              children: [],
              id: 'v99_7-a-02',
              title: '7-a-02 能用符號算式記錄生活情境中的數學問題。',
            },
            {
              href: '/v99_tree/v99_g07/v99_7-a-03',
              children: [],
              id: 'v99_7-a-03',
              title:
                '7-a-03 能理解一元一次方程式及其解的意義,並能由具體情境中列出一元一次方程式。',
            },
            {
              href: '/v99_tree/v99_g07/v99_7-a-04',
              children: [],
              id: 'v99_7-a-04',
              title: '7-a-04 能以等量公理解一元一次方程式,並做驗算。',
            },
            {
              href: '/v99_tree/v99_g07/v99_7-a-05',
              children: [],
              id: 'v99_7-a-05',
              title: '7-a-05 能利用移項法則來解一元一次方程式,並做驗算。',
            },
            {
              href: '/v99_tree/v99_g07/v99_7-a-06',
              children: [],
              id: 'v99_7-a-06',
              title:
                '7-a-06 能理解二元一次方程式及其解的意義,並能由具體情境中列出二元一次方程式。',
            },
            {
              href: '/v99_tree/v99_g07/v99_7-a-07',
              children: [],
              id: 'v99_7-a-07',
              title:
                '7-a-07 能理解二元一次聯立方程式,及其解的意義,並能由具 體情境中列出二元一次聯立方程式。',
            },
            {
              href: '/v99_tree/v99_g07/v99_7-a-08',
              children: [],
              id: 'v99_7-a-08',
              title:
                '7-a-08 能熟練使用代入消去法與加減消去法解二元一次方程式的解。',
            },
            {
              href: '/v99_tree/v99_g07/v99_7-a-09',
              children: [],
              id: 'v99_7-a-09',
              title: '7-a-09 能認識函數。',
            },
            {
              href: '/v99_tree/v99_g07/v99_7-a-10',
              children: [],
              id: 'v99_7-a-10',
              title: '7-a-10 能認識常數函數及一次函數。',
            },
            {
              href: '/v99_tree/v99_g07/v99_7-a-11',
              children: [],
              id: 'v99_7-a-11',
              title: '7-a-11 能理解平面直角坐標系。',
            },
            {
              href: '/v99_tree/v99_g07/v99_7-a-12',
              children: [],
              id: 'v99_7-a-12',
              title: '7-a-12 能在直角坐標平面上描繪常數函數及一次函數的圖形。',
            },
            {
              href: '/v99_tree/v99_g07/v99_7-a-13',
              children: [],
              id: 'v99_7-a-13',
              title: '7-a-13 能在直角坐標平面上描繪二元一次方程式的圖形。',
            },
            {
              href: '/v99_tree/v99_g07/v99_7-a-14',
              children: [],
              id: 'v99_7-a-14',
              title: '7-a-14 能理解二元一次聯立方程式解的幾何意義。',
            },
            {
              href: '/v99_tree/v99_g07/v99_7-a-15',
              children: [],
              id: 'v99_7-a-15',
              title: '7-a-15 能理解不等式的意義。',
            },
            {
              href: '/v99_tree/v99_g07/v99_7-a-16',
              children: [],
              id: 'v99_7-a-16',
              title: '7-a-16 能由具體情境中列出簡單的一元一次不等式。',
            },
            {
              href: '/v99_tree/v99_g07/v99_7-a-17',
              children: [],
              id: 'v99_7-a-17',
              title: '7-a-17 能解出一元一次不等式,並在數線上標示相關的線段。',
            },
            {
              href: '/v99_tree/v99_g07/v99_7-a-18',
              children: [],
              id: 'v99_7-a-18',
              title:
                '7-a-18 能說明 a ≤ x ≤ b 時 y=cx+d 的範圍,並在數線上圖示。',
            },
          ],
          id: 'v99_g07',
          title: '七年級',
        },
        {
          href: '/v99_tree/v99_g08',
          children: [
            {
              href: '/v99_tree/v99_g08/v99_8-a-01',
              children: [],
              id: 'v99_8-a-01',
              title: '8-a-01 能熟練二次式的乘法公式。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-a-02',
              children: [],
              id: 'v99_8-a-02',
              title: '8-a-02 能理解簡單根式的化簡及有理化。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-a-03',
              children: [],
              id: 'v99_8-a-03',
              title: '8-a-03 能認識多項式及相關名詞。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-a-04',
              children: [],
              id: 'v99_8-a-04',
              title: '8-a-04 能熟練多項式的加、減、乘、除四則運算。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-a-05',
              children: [],
              id: 'v99_8-a-05',
              title:
                '8-a-05 能理解畢氏定理(Pythagorean Theorem)及其應用。 (同 8-s-08)',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-a-06',
              children: [],
              id: 'v99_8-a-06',
              title: '8-a-06 能理解二次多項式因式分解的意義。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-a-07',
              children: [],
              id: 'v99_8-a-07',
              title: '8-a-07 能利用提公因式法分解二次多項式。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-a-08',
              children: [],
              id: 'v99_8-a-08',
              title: '8-a-08 能利用乘法公式與十字交乘法做因式分解。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-a-09',
              children: [],
              id: 'v99_8-a-09',
              title:
                '8-a-09 能在具體情境中認識一元二次方程式,並理解其解的意義。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-a-10',
              children: [],
              id: 'v99_8-a-10',
              title: '8-a-10 能利用因式分解來解一元二次方程式。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-a-11',
              children: [],
              id: 'v99_8-a-11',
              title: '8-a-11 能利用配方法解一元二次方程式。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-a-12',
              children: [],
              id: 'v99_8-a-12',
              title: '8-a-12 能利用一元二次方程式解應用問題。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-n-01',
              children: [],
              id: 'v99_8-n-01',
              title: '8-n-01 能理解二次方根的意義及熟練二次方根的計算。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-n-02',
              children: [],
              id: 'v99_8-n-02',
              title: '8-n-02 能求二次方根的近似值。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-n-03',
              children: [],
              id: 'v99_8-n-03',
              title: '8-n-03 能理解根式的化簡及四則運算。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-n-04',
              children: [],
              id: 'v99_8-n-04',
              title: '8-n-04 能在日常生活中,觀察有次序的數列,並理解其規則性。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-n-05',
              children: [],
              id: 'v99_8-n-05',
              title:
                '8-n-05 能觀察出等差數列的規則性,並能利用首項、公差計算 出等差數列的一般項。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-n-06',
              children: [],
              id: 'v99_8-n-06',
              title:
                '8-n-06 能理解等差級數求和的公式,並能解決生活中相關的問 題。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-s-01',
              children: [],
              id: 'v99_8-s-01',
              title:
                '8-s-01 能認識一些簡單圖形及其常用符號，如點、線、線段、射線、角、三角形的符號。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-s-02',
              children: [],
              id: 'v99_8-s-02',
              title: '8-s-02 能理解角的基本性質。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-s-03',
              children: [],
              id: 'v99_8-s-03',
              title: '8-s-03 能理解凸多邊形內角和以及外角和公式。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-s-04',
              children: [],
              id: 'v99_8-s-04',
              title: '8-s-04 能認識垂直以及相關的概念。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-s-05',
              children: [],
              id: 'v99_8-s-05',
              title:
                '8-s-05 能理解平行的意義，平行線截線性質，以及平行線判別性質。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-s-06',
              children: [],
              id: 'v99_8-s-06',
              title:
                '8-s-06 能理解線對稱的意義，以及能應用到理解平面圖形的幾何性質。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-s-07',
              children: [],
              id: 'v99_8-s-07',
              title: '8-s-07 能理解三角形全等性質。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-s-08',
              children: [],
              id: 'v99_8-s-08',
              title:
                '8-s-08 能理解畢氏定理(Pythagorean Theorem)及其應用。 (同 8-a-05)',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-s-09',
              children: [],
              id: 'v99_8-s-09',
              title: '8-s-09 能熟練直角坐標上任兩點的距離公式。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-s-10',
              children: [],
              id: 'v99_8-s-10',
              title: '8-s-10 能理解三角形的基本性質。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-s-11',
              children: [],
              id: 'v99_8-s-11',
              title: '8-s-11 能認識尺規作圖並能做基本的尺規作圖。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-s-12',
              children: [],
              id: 'v99_8-s-12',
              title: '8-s-12 能理解特殊的三角形與特殊的四邊形的性質。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-s-13',
              children: [],
              id: 'v99_8-s-13',
              title: '8-s-13 能理解平行四邊形及其性質。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-s-14',
              children: [],
              id: 'v99_8-s-14',
              title:
                '8-s-14 能用線對稱概念，理解等腰三角形、正方形、菱形、箏形等平面圖形。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-s-15',
              children: [],
              id: 'v99_8-s-15',
              title: '8-s-15 能理解梯形及其性質。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-s-16',
              children: [],
              id: 'v99_8-s-16',
              title:
                '8-s-16 能舉例說明，有一些敘述成立時，其逆敘述也會成立；但是，也有一些敘述成立時，其逆敘述卻不成立。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-s-17',
              children: [],
              id: 'v99_8-s-17',
              title: '8-s-17 能針對幾何推理中的步驟，寫出所依據的幾何性質。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-s-18',
              children: [],
              id: 'v99_8-s-18',
              title: '8-s-18 能從幾何圖形的判別性質，判斷圖形的包含關係。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-s-19',
              children: [],
              id: 'v99_8-s-19',
              title: '8-s-19 能熟練計算簡單圖形及其複合圖形的面積。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-s-20',
              children: [],
              id: 'v99_8-s-20',
              title:
                '8-s-20 能理解與圓相關的概念(如半徑、弦、弧、弓形等)的意義。',
            },
            {
              href: '/v99_tree/v99_g08/v99_8-s-21',
              children: [],
              id: 'v99_8-s-21',
              title: '8-s-21 能理解弧長的公式以及扇形面積的公式。',
            },
          ],
          id: 'v99_g08',
          title: '八年級',
        },
        {
          href: '/v99_tree/v99_g09',
          children: [
            {
              href: '/v99_tree/v99_g09/v99_9-s-01',
              children: [],
              id: 'v99_9-s-01',
              title: '9-s-01 能理解平面圖形縮放的意義。',
            },
            {
              href: '/v99_tree/v99_g09/v99_9-s-02',
              children: [],
              id: 'v99_9-s-02',
              title: '9-s-02 能理解多邊形相似的意義。',
            },
            {
              href: '/v99_tree/v99_g09/v99_9-s-03',
              children: [],
              id: 'v99_9-s-03',
              title: '9-s-03 能理解三角形的相似性質。',
            },
            {
              href: '/v99_tree/v99_g09/v99_9-s-04',
              children: [],
              id: 'v99_9-s-04',
              title: '9-s-04 能理解平行線截比例線段性質及其逆敘述。',
            },
            {
              href: '/v99_tree/v99_g09/v99_9-s-05',
              children: [],
              id: 'v99_9-s-05',
              title: '9-s-05 能利用相似三角形對應邊成比例的觀念，解應用問題。',
            },
            {
              href: '/v99_tree/v99_g09/v99_9-s-06',
              children: [],
              id: 'v99_9-s-06',
              title: '9-s-06 能理解圓的幾何性質。',
            },
            {
              href: '/v99_tree/v99_g09/v99_9-s-07',
              children: [],
              id: 'v99_9-s-07',
              title: '9-s-07 能理解直線與圓及兩圓的關係。',
            },
            {
              href: '/v99_tree/v99_g09/v99_9-s-08',
              children: [],
              id: 'v99_9-s-08',
              title: '9-s-08 能理解多邊形外心的意義和相關性質。',
            },
            {
              href: '/v99_tree/v99_g09/v99_9-s-09',
              children: [],
              id: 'v99_9-s-09',
              title: '9-s-09 能理解多邊形內心的意義和相關性質。',
            },
            {
              href: '/v99_tree/v99_g09/v99_9-s-10',
              children: [],
              id: 'v99_9-s-10',
              title: '9-s-10 能理解三角形重心的意義和相關性質。',
            },
            {
              href: '/v99_tree/v99_g09/v99_9-s-11',
              children: [],
              id: 'v99_9-s-11',
              title:
                '9-s-11 能理解正多邊形的幾何性質(含線對稱、內切圓、外接圓)。',
            },
            {
              href: '/v99_tree/v99_g09/v99_9-s-12',
              children: [],
              id: 'v99_9-s-12',
              title: '9-s-12 能認識證明的意義。',
            },
            {
              href: '/v99_tree/v99_g09/v99_9-s-13',
              children: [],
              id: 'v99_9-s-13',
              title: '9-s-13 能認識線與平面、平面與平面的垂直關係與平行關係。',
            },
            {
              href: '/v99_tree/v99_g09/v99_9-s-14',
              children: [],
              id: 'v99_9-s-14',
              title: '9-s-14 能理解簡單立體圖形。',
            },
            {
              href: '/v99_tree/v99_g09/v99_9-s-15',
              children: [],
              id: 'v99_9-s-15',
              title:
                '9-s-15 能理解簡單立體圖形的展開圖，並能利用展開圖來計算 立體圖形的表面積或側面積。',
            },
            {
              href: '/v99_tree/v99_g09/v99_9-s-16',
              children: [],
              id: 'v99_9-s-16',
              title: '9-s-16 能計算直角柱、直圓柱的體積。',
            },
            {
              href: '/v99_tree/v99_g09/v99_9-a-01',
              children: [],
              id: 'v99_9-a-01',
              title: '9-a-01 能理解二次函數的意義。',
            },
            {
              href: '/v99_tree/v99_g09/v99_9-a-02',
              children: [],
              id: 'v99_9-a-02',
              title: '9-a-02 能描繪二次函數的圖形。',
            },
            {
              href: '/v99_tree/v99_g09/v99_9-a-03',
              children: [],
              id: 'v99_9-a-03',
              title: '9-a-03 能計算二次函數的最大值或最小值。',
            },
            {
              href: '/v99_tree/v99_g09/v99_9-a-04',
              children: [],
              id: 'v99_9-a-04',
              title: '9-a-04 能解決二次函數的相關應用問題。',
            },
            {
              href: '/v99_tree/v99_g09/v99_9-d-01',
              children: [],
              id: 'v99_9-d-01',
              title:
                '9-d-01 能將原始資料整理成次數分配表，並製作統計圖形，來顯示資料蘊含的意義。',
            },
            {
              href: '/v99_tree/v99_g09/v99_9-d-02',
              children: [],
              id: 'v99_9-d-02',
              title: '9-d-02 認識平均數、中位數與眾數。',
            },
            {
              href: '/v99_tree/v99_g09/v99_9-d-03',
              children: [],
              id: 'v99_9-d-03',
              title: '9-d-03 能認識全距及四分位距，並製作盒狀圖。',
            },
            {
              href: '/v99_tree/v99_g09/v99_9-d-04',
              children: [],
              id: 'v99_9-d-04',
              title:
                '9-d-04 能認識百分位數的概念，並認識第 10、25、50、75、90 百分位數。',
            },
            {
              href: '/v99_tree/v99_g09/v99_9-d-05',
              children: [],
              id: 'v99_9-d-05',
              title: '9-d-05 能在具體情境中認識機率的概念。',
            },
          ],
          id: 'v99_g09',
          title: '九年級',
        },
      ],
      id: 'v99_tree',
      title: '數學（課程綱要）',
    },
    {
      href: '/junyi-english',
      children: [
        {
          href: '/junyi-english/junyi-english-lettersandphonics',
          children: [
            {
              href: '/junyi-english/junyi-english-lettersandphonics/e1a',
              children: [],
              id: 'e1a',
              title: '認識英文字母',
            },
            {
              href:
                '/junyi-english/junyi-english-lettersandphonics/lettersandphonics-namewrite',
              children: [],
              id: 'lettersandphonics-namewrite',
              title: '字母讀法與寫法',
            },
            {
              href:
                '/junyi-english/junyi-english-lettersandphonics/junyi-english-lettersandphonics-lettersound',
              children: [],
              id: 'junyi-english-lettersandphonics-lettersound',
              title: '從讀音到拼音',
            },
            {
              href: '/junyi-english/junyi-english-lettersandphonics/eephn',
              children: [],
              id: 'eephn',
              title: '自然發音的六十堂課',
            },
            {
              href:
                '/junyi-english/junyi-english-lettersandphonics/junyi-english-lettersandphonics-spellsound',
              children: [],
              id: 'junyi-english-lettersandphonics-spellsound',
              title: '英語拼讀規則',
            },
            {
              href:
                '/junyi-english/junyi-english-lettersandphonics/lettersandphonics-speaking',
              children: [],
              id: 'lettersandphonics-speaking',
              title: '其他發音規則',
            },
          ],
          id: 'junyi-english-lettersandphonics',
          title: '字母與發音',
        },
        {
          href: '/junyi-english/junyi-english-vocab',
          children: [
            {
              href:
                '/junyi-english/junyi-english-vocab/junyi-english-vocab-sightwords',
              children: [],
              id: 'junyi-english-vocab-sightwords',
              title: '常見單字',
            },
            {
              href:
                '/junyi-english/junyi-english-vocab/junyi-english-vocab-themewords',
              children: [],
              id: 'junyi-english-vocab-themewords',
              title: '主題單字',
            },
            {
              href:
                '/junyi-english/junyi-english-vocab/junyi-english-vocab-taiwanwords',
              children: [],
              id: 'junyi-english-vocab-taiwanwords',
              title: '臺灣主題單字',
            },
            {
              href:
                '/junyi-english/junyi-english-vocab/junyi-english-vocab-advancedwords',
              children: [],
              id: 'junyi-english-vocab-advancedwords',
              title: '學科英文單字',
            },
            {
              href:
                '/junyi-english/junyi-english-vocab/junyi-english-vocab-prefixsuffix',
              children: [],
              id: 'junyi-english-vocab-prefixsuffix',
              title: '字首與字尾',
            },
            {
              href:
                '/junyi-english/junyi-english-vocab/junyi-english-vocab-exam',
              children: [],
              id: 'junyi-english-vocab-exam',
              title: '學測英文單字',
            },
          ],
          id: 'junyi-english-vocab',
          title: '單字',
        },
        {
          href: '/junyi-english/junyi-english-context',
          children: [
            {
              href: '/junyi-english/junyi-english-context/temp3',
              children: [],
              id: 'temp3',
              title: '防疫英語',
            },
            {
              href:
                '/junyi-english/junyi-english-context/junyi-english-context-words',
              children: [],
              id: 'junyi-english-context-words',
              title: '靈活運用單字',
            },
            {
              href:
                '/junyi-english/junyi-english-context/junyi-english-context-phrases',
              children: [],
              id: 'junyi-english-context-phrases',
              title: '靈活運用句子',
            },
            {
              href:
                '/junyi-english/junyi-english-context/junyi-english-context-scenarios',
              children: [],
              id: 'junyi-english-context-scenarios',
              title: '生活情境應用',
            },
          ],
          id: 'junyi-english-context',
          title: '活用',
        },
        {
          href: '/junyi-english/junyi-english-reading',
          children: [
            {
              href: '/junyi-english/junyi-english-reading/eer',
              children: [],
              id: 'eer',
              title: '英文閱讀素養',
            },
            {
              href:
                '/junyi-english/junyi-english-reading/junyi-english-reading-topic',
              children: [],
              id: 'junyi-english-reading-topic',
              title: '英文主題閱讀',
            },
            {
              href:
                '/junyi-english/junyi-english-reading/junyi-english-reading-strategy',
              children: [],
              id: 'junyi-english-reading-strategy',
              title: '英文閱讀策略',
            },
            {
              href:
                '/junyi-english/junyi-english-reading/junyi-english-reading-exam',
              children: [],
              id: 'junyi-english-reading-exam',
              title: '英文閱讀試題解析',
            },
            {
              href:
                '/junyi-english/junyi-english-reading/junyi-english-reading-textbook',
              children: [],
              id: 'junyi-english-reading-textbook',
              title: '高中英文課文講解',
            },
          ],
          id: 'junyi-english-reading',
          title: '閱讀',
        },
        {
          href: '/junyi-english/junyi-english-grammar',
          children: [
            {
              href:
                '/junyi-english/junyi-english-grammar/junyi-english-grammar-tense',
              children: [],
              id: 'junyi-english-grammar-tense',
              title: '時態',
            },
            {
              href:
                '/junyi-english/junyi-english-grammar/junyi-english-grammar-lexicalunit',
              children: [],
              id: 'junyi-english-grammar-lexicalunit',
              title: '詞類',
            },
            {
              href:
                '/junyi-english/junyi-english-grammar/junyi-english-grammar-syntax',
              children: [],
              id: 'junyi-english-grammar-syntax',
              title: '句型',
            },
            {
              href:
                '/junyi-english/junyi-english-grammar/junyi-english-grammar-epaper',
              children: [],
              id: 'junyi-english-grammar-epaper',
              title: '從句型到寫作',
            },
          ],
          id: 'junyi-english-grammar',
          title: '文法',
        },
        {
          href: '/junyi-english/junyi-english-writing',
          children: [
            {
              href:
                '/junyi-english/junyi-english-writing/junyi-english-writing-senior',
              children: [],
              id: 'junyi-english-writing-senior',
              title: '高中英語寫作',
            },
            {
              href:
                '/junyi-english/junyi-english-writing/junyi-english-writing-msyu',
              children: [],
              id: 'junyi-english-writing-msyu',
              title: '余老師英文寫作',
            },
          ],
          id: 'junyi-english-writing',
          title: '寫作',
        },
        {
          href: '/junyi-english/junyi-english-others',
          children: [
            {
              href:
                '/junyi-english/junyi-english-others/happyenglishlearning_new1',
              children: [],
              id: 'happyenglishlearning_new1',
              title: '用中學：從零開始學英語',
            },
            {
              href:
                '/junyi-english/junyi-english-others/junyi-english-others-metacognition',
              children: [],
              id: 'junyi-english-others-metacognition',
              title: '學習策略',
            },
            {
              href:
                '/junyi-english/junyi-english-others/junyi-english-others-listening',
              children: [],
              id: 'junyi-english-others-listening',
              title: '聽力策略',
            },
            {
              href:
                '/junyi-english/junyi-english-others/junyi-english-others-songs',
              children: [],
              id: 'junyi-english-others-songs',
              title: '兒歌',
            },
            {
              href:
                '/junyi-english/junyi-english-others/junyi-english-others-storytelling',
              children: [],
              id: 'junyi-english-others-storytelling',
              title: '聽故事',
            },
            {
              href:
                '/junyi-english/junyi-english-others/junyi-english-others-others',
              children: [],
              id: 'junyi-english-others-others',
              title: '其他資源',
            },
          ],
          id: 'junyi-english-others',
          title: '更多課程',
        },
        {
          href: '/junyi-english/eng-elementary',
          children: [
            {
              href: '/junyi-english/eng-elementary/eng-elementary-bl',
              children: [],
              id: 'eng-elementary-bl',
              title: '基本學習',
            },
            {
              href: '/junyi-english/eng-elementary/adlele-e',
              children: [],
              id: 'adlele-e',
              title: '因材網',
            },
            {
              href: '/junyi-english/eng-elementary/educloudele-e',
              children: [],
              id: 'educloudele-e',
              title: '教育雲影音',
            },
            {
              href: '/junyi-english/eng-elementary/coocele-e',
              children: [],
              id: 'coocele-e',
              title: '酷課學習',
            },
          ],
          id: 'eng-elementary',
          title: '國小',
        },
        {
          href: '/junyi-english/eng-junior',
          children: [
            {
              href: '/junyi-english/eng-junior/eng-junior-bl',
              children: [],
              id: 'eng-junior-bl',
              title: '基本學習',
            },
            {
              href: '/junyi-english/eng-junior/adljun-e',
              children: [],
              id: 'adljun-e',
              title: '因材網',
            },
            {
              href: '/junyi-english/eng-junior/educloudjun-e',
              children: [],
              id: 'educloudjun-e',
              title: '教育雲影音',
            },
            {
              href: '/junyi-english/eng-junior/coocjun-e',
              children: [],
              id: 'coocjun-e',
              title: '酷課學習',
            },
          ],
          id: 'eng-junior',
          title: '國中',
        },
        {
          href: '/junyi-english/eng-senior',
          children: [
            {
              href: '/junyi-english/eng-senior/educloudsen-e',
              children: [],
              id: 'educloudsen-e',
              title: '教育雲影音',
            },
            {
              href: '/junyi-english/eng-senior/coocsen-e',
              children: [],
              id: 'coocsen-e',
              title: '酷課學習',
            },
            {
              href: '/junyi-english/eng-senior/esb',
              children: [],
              id: 'esb',
              title: '高中英文課文講解',
            },
            {
              href: '/junyi-english/eng-senior/es-paper',
              children: [],
              id: 'es-paper',
              title: '高中英文學習電子報',
            },
            {
              href: '/junyi-english/eng-senior/esw',
              children: [],
              id: 'esw',
              title: '高中英文作文',
            },
            {
              href: '/junyi-english/eng-senior/est',
              children: [],
              id: 'est',
              title: '高中英文中翻英',
            },
            {
              href: '/junyi-english/eng-senior/esr',
              children: [],
              id: 'esr',
              title: '高中英文閱讀',
            },
            {
              href: '/junyi-english/eng-senior/english-syntax1',
              children: [],
              id: 'english-syntax1',
              title: '高中英文文法',
            },
            {
              href: '/junyi-english/eng-senior/english-vocabulary1',
              children: [],
              id: 'english-vocabulary1',
              title: '高中英文單字',
            },
          ],
          id: 'eng-senior',
          title: '高中',
        },
      ],
      id: 'junyi-english',
      title: '英文',
    },
    {
      href: '/computing',
      children: [
        {
          href: '/computing/computer-science',
          children: [
            {
              href: '/computing/computer-science/howcomputerwork',
              children: [],
              id: 'howcomputerwork',
              title: '電腦如何運作',
            },
            {
              href: '/computing/computer-science/cs-hiw',
              children: [],
              id: 'cs-hiw',
              title: '網際網路如何運作',
            },
            {
              href: '/computing/computer-science/cs-haiw',
              children: [],
              id: 'cs-haiw',
              title: '人工智慧如何運作',
            },
            {
              href: '/computing/computer-science/open-data-iot',
              children: [],
              id: 'open-data-iot',
              title: '開放資料與物聯網應用',
            },
          ],
          id: 'computer-science',
          title: '科技科普',
        },
        {
          href: '/computing/programming',
          children: [
            {
              href: '/computing/programming/cs-unplug',
              children: [],
              id: 'cs-unplug',
              title: '不插電玩程式',
            },
            {
              href: '/computing/programming/codeorg',
              children: [],
              id: 'codeorg',
              title: 'Code.org玩程式',
            },
            {
              href: '/computing/programming/scratch',
              children: [],
              id: 'scratch',
              title: 'Scratch玩程式',
            },
            {
              href: '/computing/programming/webdev',
              children: [],
              id: 'webdev',
              title: 'HTML / CSS',
            },
            {
              href: '/computing/programming/python',
              children: [],
              id: 'python',
              title: 'Python',
            },
            {
              href: '/computing/programming/algorithm',
              children: [],
              id: 'algorithm',
              title: '演算法設計',
            },
            {
              href: '/computing/programming/programming-legacy',
              children: [],
              id: 'programming-legacy',
              title: '早期／程式設計課程',
            },
          ],
          id: 'programming',
          title: '程式設計',
        },
        {
          href: '/computing/cross-discipline',
          children: [
            {
              href: '/computing/cross-discipline/csfirst-storytelling',
              children: [],
              id: 'csfirst-storytelling',
              title: '電腦科學起步走｜一起說故事',
            },
            {
              href: '/computing/cross-discipline/interdisciplinary-math-cs',
              children: [],
              id: 'interdisciplinary-math-cs',
              title: '數電快閃教室',
            },
            {
              href: '/computing/cross-discipline/ct-math',
              children: [],
              id: 'ct-math',
              title: '運算思維融入數學教材',
            },
            {
              href: '/computing/cross-discipline/cs-physics',
              children: [],
              id: 'cs-physics',
              title: '物理模擬程式設計',
            },
          ],
          id: 'cross-discipline',
          title: '跨域學習',
        },
        {
          href: '/computing/cs-isl',
          children: [
            {
              href: '/computing/cs-isl/cs-isl-1',
              children: [],
              id: 'cs-isl-1',
              title: 'iSafe 全民資安素養',
            },
            {
              href: '/computing/cs-isl/cs-isl-2',
              children: [],
              id: 'cs-isl-2',
              title: '資訊安全與倫理',
            },
            {
              href: '/computing/cs-isl/cs-isl-3',
              children: [],
              id: 'cs-isl-3',
              title: '資訊素養與倫理',
            },
          ],
          id: 'cs-isl',
          title: '數位素養',
        },
        {
          href: '/computing/cs-ai',
          children: [
            {
              href: '/computing/cs-ai/ai-intro',
              children: [],
              id: 'ai-intro',
              title: '人工智慧如何運作',
            },
          ],
          id: 'cs-ai',
          title: '人工智慧',
        },
        {
          href: '/computing/electrical-engineering',
          children: [
            {
              href: '/computing/electrical-engineering/picoboard',
              children: [],
              id: 'picoboard',
              title: 'PicoBoard',
            },
            {
              href: '/computing/electrical-engineering/arduino',
              children: [],
              id: 'arduino',
              title: 'Arduino',
            },
            {
              href: '/computing/electrical-engineering/linkit',
              children: [],
              id: 'linkit',
              title: 'LinkIt',
            },
            {
              href: '/computing/electrical-engineering/ee-legacy',
              children: [],
              id: 'ee-legacy',
              title: '早期／機電整合課程',
            },
          ],
          id: 'electrical-engineering',
          title: '機電整合',
        },
        {
          href: '/computing/sw-app',
          children: [
            {
              href: '/computing/sw-app/gcourse-1',
              children: [],
              id: 'gcourse-1',
              title: '小學生的神奇口袋',
            },
            {
              href: '/computing/sw-app/sw-app-legacy',
              children: [],
              id: 'sw-app-legacy',
              title: '早期／軟體應用課程',
            },
          ],
          id: 'sw-app',
          title: '軟體應用',
        },
      ],
      id: 'computing',
      title: '電腦科學',
    },
    {
      href: '/junyi-competency',
      children: [
        {
          href: '/junyi-competency/2020-star-explore-01',
          children: [
            {
              href: '/junyi-competency/2020-star-explore-01/2020-se-math',
              children: [],
              id: '2020-se-math',
              title: '數學',
            },
            {
              href: '/junyi-competency/2020-star-explore-01/2020-se-nature',
              children: [],
              id: '2020-se-nature',
              title: '理化',
            },
            {
              href: '/junyi-competency/2020-star-explore-01/v1113-new-topic',
              children: [],
              id: 'v1113-new-topic',
              title: '生物',
            },
          ],
          id: '2020-star-explore-01',
          title: '狐狸貓數理素養',
        },
        {
          href: '/junyi-competency/logic',
          children: [
            {
              href: '/junyi-competency/logic/logictrainning_01',
              children: [],
              id: 'logictrainning_01',
              title: '01_水平思考VS垂直思考',
            },
            {
              href: '/junyi-competency/logic/logictrainning_02',
              children: [],
              id: 'logictrainning_02',
              title: '02_若P則Q',
            },
            {
              href: '/junyi-competency/logic/logictrainning_03',
              children: [],
              id: 'logictrainning_03',
              title: '03_稻草人謬誤',
            },
            {
              href: '/junyi-competency/logic/logictrainning_04',
              children: [],
              id: 'logictrainning_04',
              title: '04_合理不等於正確',
            },
            {
              href: '/junyi-competency/logic/logictrainning_05',
              children: [],
              id: 'logictrainning_05',
              title: '05_非黑即白的思考模式',
            },
            {
              href: '/junyi-competency/logic/logictrainning_06',
              children: [],
              id: 'logictrainning_06',
              title: '06_訴諸無知的思考謬誤',
            },
            {
              href: '/junyi-competency/logic/logictrainning_07',
              children: [],
              id: 'logictrainning_07',
              title: '07_廢話謬誤',
            },
            {
              href: '/junyi-competency/logic/logictrainning_08',
              children: [],
              id: 'logictrainning_08',
              title: '08_以偏概全',
            },
            {
              href: '/junyi-competency/logic/logictrainning_09',
              children: [],
              id: 'logictrainning_09',
              title: '09_歧義謬誤',
            },
            {
              href: '/junyi-competency/logic/logictrainning_10',
              children: [],
              id: 'logictrainning_10',
              title: '10_思考力的基礎',
            },
          ],
          id: 'logic',
          title: '思考力訓練',
        },
        {
          href: '/junyi-competency/financial-management',
          children: [],
          id: 'financial-management',
          title: '理財能力',
        },
        {
          href: '/junyi-competency/v972-new-topic',
          children: [],
          id: 'v972-new-topic',
          title: '科學議題',
        },
        {
          href: '/junyi-competency/v1093-new-topic-1',
          children: [
            {
              href: '/junyi-competency/v1093-new-topic-1/v1093-new-topic-2',
              children: [],
              id: 'v1093-new-topic-2',
              title: '謠言演化的比疫苗快',
            },
            {
              href: '/junyi-competency/v1093-new-topic-1/v1094-new-topic',
              children: [],
              id: 'v1094-new-topic',
              title: '解鎖時事科學:非洲豬瘟病毒 ',
            },
            {
              href: '/junyi-competency/v1093-new-topic-1/v1125-new-topic',
              children: [],
              id: 'v1125-new-topic',
              title: '大數據時代的來臨，媒體的下一步？',
            },
            {
              href: '/junyi-competency/v1093-new-topic-1/v1148-new-topic',
              children: [],
              id: 'v1148-new-topic',
              title: '神奇的酵素？！潛移默化的置入性行銷',
            },
          ],
          id: 'v1093-new-topic-1',
          title: '科學媒體素養',
        },
        {
          href: '/junyi-competency/2020-star-explore-02',
          children: [
            {
              href: '/junyi-competency/2020-star-explore-02/v1109-new-topic-7',
              children: [],
              id: 'v1109-new-topic-7',
              title: '單元1 - 找出文中大意',
            },
            {
              href: '/junyi-competency/2020-star-explore-02/v1109-new-topic-11',
              children: [],
              id: 'v1109-new-topic-11',
              title: '單元2 - 找出文中重點細節',
            },
            {
              href: '/junyi-competency/2020-star-explore-02/v1109-new-topic-15',
              children: [],
              id: 'v1109-new-topic-15',
              title: '單元3 - 預測',
            },
            {
              href: '/junyi-competency/2020-star-explore-02/v1109-new-topic-19',
              children: [],
              id: 'v1109-new-topic-19',
              title: '單元4 - 找出順序',
            },
            {
              href: '/junyi-competency/2020-star-explore-02/v1109-new-topic-23',
              children: [],
              id: 'v1109-new-topic-23',
              title: '單元5 - 描述場景、角色、情節',
            },
          ],
          id: '2020-star-explore-02',
          title: '英文閱讀素養',
        },
        {
          href: '/junyi-competency/v1051-new-topic',
          children: [
            {
              href: '/junyi-competency/v1051-new-topic/shiny-diabolo',
              children: [],
              id: 'shiny-diabolo',
              title: '扯鈴',
            },
            {
              href: '/junyi-competency/v1051-new-topic/dancing',
              children: [],
              id: 'dancing',
              title: '舞蹈',
            },
            {
              href: '/junyi-competency/v1051-new-topic/junyi-music',
              children: [],
              id: 'junyi-music',
              title: '音樂',
            },
          ],
          id: 'v1051-new-topic',
          title: '藝術與美感',
        },
      ],
      id: 'junyi-competency',
      title: '素養',
    },
    {
      href: '/junyi-science',
      children: [
        {
          href: '/junyi-science/coocele-s3',
          children: [
            {
              href: '/junyi-science/coocele-s3/coocele-s3-1',
              children: [],
              id: 'coocele-s3-1',
              title: '植物的身體',
            },
            {
              href: '/junyi-science/coocele-s3/coocele-s3-2',
              children: [],
              id: 'coocele-s3-2',
              title: '神奇磁力',
            },
            {
              href: '/junyi-science/coocele-s3/coocele-s3-3',
              children: [],
              id: 'coocele-s3-3',
              title: '空氣',
            },
            {
              href: '/junyi-science/coocele-s3/coocele-s3-4',
              children: [],
              id: 'coocele-s3-4',
              title: '溶解',
            },
            {
              href: '/junyi-science/coocele-s3/coocele-s3-5',
              children: [],
              id: 'coocele-s3-5',
              title: '小園丁學種菜',
            },
            {
              href: '/junyi-science/coocele-s3/coocele-s3-6',
              children: [],
              id: 'coocele-s3-6',
              title: '水的變化',
            },
            {
              href: '/junyi-science/coocele-s3/coocele-s3-7',
              children: [],
              id: 'coocele-s3-7',
              title: '認識天氣',
            },
            {
              href: '/junyi-science/coocele-s3/coocele-s3-8',
              children: [],
              id: 'coocele-s3-8',
              title: '動物大會師',
            },
          ],
          id: 'coocele-s3',
          title: '三年級',
        },
        {
          href: '/junyi-science/coocele-s4',
          children: [
            {
              href: '/junyi-science/coocele-s4/coocele-s4-1',
              children: [],
              id: 'coocele-s4-1',
              title: '月亮',
            },
            {
              href: '/junyi-science/coocele-s4/coocele-s4-2',
              children: [],
              id: 'coocele-s4-2',
              title: '水生家族',
            },
            {
              href: '/junyi-science/coocele-s4/coocele-s4-3',
              children: [],
              id: 'coocele-s4-3',
              title: '光的世界',
            },
            {
              href: '/junyi-science/coocele-s4/coocele-s4-4',
              children: [],
              id: 'coocele-s4-4',
              title: '燈泡亮了',
            },
            {
              href: '/junyi-science/coocele-s4/coocele-s4-5',
              children: [],
              id: 'coocele-s4-5',
              title: '時間',
            },
            {
              href: '/junyi-science/coocele-s4/coocele-s4-6',
              children: [],
              id: 'coocele-s4-6',
              title: '昆蟲王國',
            },
            {
              href: '/junyi-science/coocele-s4/coocele-s4-7',
              children: [],
              id: 'coocele-s4-7',
              title: '交通工具與能源',
            },
            {
              href: '/junyi-science/coocele-s4/coocele-s4-8',
              children: [],
              id: 'coocele-s4-8',
              title: '水的移動',
            },
          ],
          id: 'coocele-s4',
          title: '四年級',
        },
        {
          href: '/junyi-science/coocele-s5',
          children: [
            {
              href: '/junyi-science/coocele-s5/coocele-s5-1',
              children: [],
              id: 'coocele-s5-1',
              title: '觀測太陽',
            },
            {
              href: '/junyi-science/coocele-s5/coocele-s5-2',
              children: [],
              id: 'coocele-s5-2',
              title: '植物世界面面觀',
            },
            {
              href: '/junyi-science/coocele-s5/coocele-s5-3',
              children: [],
              id: 'coocele-s5-3',
              title: '水溶液的性質',
            },
            {
              href: '/junyi-science/coocele-s5/coocele-s5-4',
              children: [],
              id: 'coocele-s5-4',
              title: '力與運動',
            },
            {
              href: '/junyi-science/coocele-s5/coocele-s5-5',
              children: [],
              id: 'coocele-s5-5',
              title: '美麗的星空',
            },
            {
              href: '/junyi-science/coocele-s5/coocele-s5-6',
              children: [],
              id: 'coocele-s5-6',
              title: '燃燒和生鏽',
            },
            {
              href: '/junyi-science/coocele-s5/coocele-s5-7',
              children: [],
              id: 'coocele-s5-7',
              title: '動物大觀園',
            },
            {
              href: '/junyi-science/coocele-s5/coocele-s5-8',
              children: [],
              id: 'coocele-s5-8',
              title: '聲音與樂器',
            },
          ],
          id: 'coocele-s5',
          title: '五年級',
        },
        {
          href: '/junyi-science/coocele-s6',
          children: [
            {
              href: '/junyi-science/coocele-s6/coocele-s6-1',
              children: [],
              id: 'coocele-s6-1',
              title: '多變的天氣',
            },
            {
              href: '/junyi-science/coocele-s6/coocele-s6-2',
              children: [],
              id: 'coocele-s6-2',
              title: '地表的變化',
            },
            {
              href: '/junyi-science/coocele-s6/coocele-s6-3',
              children: [],
              id: 'coocele-s6-3',
              title: '熱對物質的影響',
            },
            {
              href: '/junyi-science/coocele-s6/coocele-s6-4',
              children: [],
              id: 'coocele-s6-4',
              title: '電磁作用',
            },
            {
              href: '/junyi-science/coocele-s6/coocele-s6-5',
              children: [],
              id: 'coocele-s6-5',
              title: '巧妙的施力工具',
            },
            {
              href: '/junyi-science/coocele-s6/coocele-s6-6',
              children: [],
              id: 'coocele-s6-6',
              title: '微生物與食品保存',
            },
            {
              href: '/junyi-science/coocele-s6/coocele-s6-7',
              children: [],
              id: 'coocele-s6-7',
              title: '生物.環境與自然資源',
            },
          ],
          id: 'coocele-s6',
          title: '六年級',
        },
        {
          href: '/junyi-science/junyi-middle-school-biology',
          children: [
            {
              href: '/junyi-science/junyi-middle-school-biology/s4bsw-',
              children: [],
              id: 's4bsw-',
              title: '【緒論】科學方法',
            },
            {
              href: '/junyi-science/junyi-middle-school-biology/s4bsm-',
              children: [],
              id: 's4bsm-',
              title: '【七上】生命世界',
            },
            {
              href: '/junyi-science/junyi-middle-school-biology/s4bsb-',
              children: [],
              id: 's4bsb-',
              title: '【七上】細胞',
            },
            {
              href: '/junyi-science/junyi-middle-school-biology/s4byy-',
              children: [],
              id: 's4byy-',
              title: '【七上】營養',
            },
            {
              href: '/junyi-science/junyi-middle-school-biology/s4bys-',
              children: [],
              id: 's4bys-',
              title: '【七上】運輸',
            },
            {
              href: '/junyi-science/junyi-middle-school-biology/s4bxd-',
              children: [],
              id: 's4bxd-',
              title: '【七上】協調',
            },
            {
              href: '/junyi-science/junyi-middle-school-biology/s4bhd-',
              children: [],
              id: 's4bhd-',
              title: '【七上】恆定',
            },
            {
              href: '/junyi-science/junyi-middle-school-biology/s4bsz-',
              children: [],
              id: 's4bsz-',
              title: '【七下】生殖',
            },
            {
              href: '/junyi-science/junyi-middle-school-biology/s4byc-',
              children: [],
              id: 's4byc-',
              title: '【七下】遺傳',
            },
            {
              href: '/junyi-science/junyi-middle-school-biology/s4byh-',
              children: [],
              id: 's4byh-',
              title: '【七下】演化',
            },
            {
              href: '/junyi-science/junyi-middle-school-biology/s4bfl-',
              children: [],
              id: 's4bfl-',
              title: '【七下】分類',
            },
            {
              href: '/junyi-science/junyi-middle-school-biology/s4bst-',
              children: [],
              id: 's4bst-',
              title: '【七下】生態',
            },
            {
              href: '/junyi-science/junyi-middle-school-biology/s4bhj-',
              children: [],
              id: 's4bhj-',
              title: '【七下】環境',
            },
          ],
          id: 'junyi-middle-school-biology',
          title: '國中生物',
        },
        {
          href: '/junyi-science/middle-school-physics-chemistry',
          children: [
            {
              href: '/junyi-science/middle-school-physics-chemistry/junyixlis',
              children: [],
              id: 'junyixlis',
              title: '【暖身】LIS自然系列－化學',
            },
            {
              href: '/junyi-science/middle-school-physics-chemistry/junyixlis2',
              children: [],
              id: 'junyixlis2',
              title: '【暖身】LIS自然系列－物理',
            },
            {
              href: '/junyi-science/middle-school-physics-chemistry/junyixlis3',
              children: [],
              id: 'junyixlis3',
              title: '【暖身】LIS實驗室',
            },
            {
              href:
                '/junyi-science/middle-school-physics-chemistry/tw_science_exp',
              children: [],
              id: 'tw_science_exp',
              title: '【暖身】台灣吧之實驗科學吧',
            },
            {
              href: '/junyi-science/middle-school-physics-chemistry/s4zgb-',
              children: [],
              id: 's4zgb-',
              title: '【八上】基本測量與科學概念',
            },
            {
              href: '/junyi-science/middle-school-physics-chemistry/s4zrs-',
              children: [],
              id: 's4zrs-',
              title: '【八上】認識物質',
            },
            {
              href: '/junyi-science/middle-school-physics-chemistry/s4zpd-',
              children: [],
              id: 's4zpd-',
              title: '【八上】波動與聲音',
            },
            {
              href: '/junyi-science/middle-school-physics-chemistry/s4zgi-',
              children: [],
              id: 's4zgi-',
              title: '【八上】光、影像與顏色',
            },
            {
              href: '/junyi-science/middle-school-physics-chemistry/s4zwd-',
              children: [],
              id: 's4zwd-',
              title: '【八上】溫度與熱',
            },
            {
              href: '/junyi-science/middle-school-physics-chemistry/s4zwz-',
              children: [],
              id: 's4zwz-',
              title: '【八上】物質的基本結構',
            },
            {
              href: '/junyi-science/middle-school-physics-chemistry/s4zhs-',
              children: [],
              id: 's4zhs-',
              title: '【八下】化學反應',
            },
            {
              href: '/junyi-science/middle-school-physics-chemistry/s4zyh-',
              children: [],
              id: 's4zyh-',
              title: '【八下】氧化還原反應',
            },
            {
              href: '/junyi-science/middle-school-physics-chemistry/s4zdg-',
              children: [],
              id: 's4zdg-',
              title: '【八下】電解質和酸鹼鹽',
            },
            {
              href: '/junyi-science/middle-school-physics-chemistry/s4zfi-',
              children: [],
              id: 's4zfi-',
              title: '【八下】反應速率與平衡',
            },
            {
              href: '/junyi-science/middle-school-physics-chemistry/s4zyg-',
              children: [],
              id: 's4zyg-',
              title: '【八下】有機化合物',
            },
            {
              href: '/junyi-science/middle-school-physics-chemistry/s4zyl-',
              children: [],
              id: 's4zyl-',
              title: '【八下】力與壓力',
            },
            {
              href: '/junyi-science/middle-school-physics-chemistry/s4zzs-',
              children: [],
              id: 's4zzs-',
              title: '【九上】直線運動',
            },
            {
              href: '/junyi-science/middle-school-physics-chemistry/s4zud-',
              children: [],
              id: 's4zud-',
              title: '【九上】力與運動',
            },
            {
              href: '/junyi-science/middle-school-physics-chemistry/s4zzl-',
              children: [],
              id: 's4zzl-',
              title: '【九上】功與能',
            },
            {
              href: '/junyi-science/middle-school-physics-chemistry/s4zdl-',
              children: [],
              id: 's4zdl-',
              title: '【九上】電流、電壓與歐姆定律',
            },
            {
              href: '/junyi-science/middle-school-physics-chemistry/s4zdz-',
              children: [],
              id: 's4zdz-',
              title: '【九下】電流的熱效應與化學反應',
            },
            {
              href: '/junyi-science/middle-school-physics-chemistry/s4zdt-',
              children: [],
              id: 's4zdt-',
              title: '【九下】電與磁',
            },
          ],
          id: 'middle-school-physics-chemistry',
          title: '國中理化',
        },
        {
          href: '/junyi-science/junyi-middle-earth-science',
          children: [
            {
              href: '/junyi-science/junyi-middle-earth-science/njesl',
              children: [],
              id: 'njesl',
              title: '水與陸地',
            },
            {
              href: '/junyi-science/junyi-middle-earth-science/njebd',
              children: [],
              id: 'njebd',
              title: '板塊運動與地球歷史',
            },
            {
              href: '/junyi-science/junyi-middle-earth-science/njeut',
              children: [],
              id: 'njeut',
              title: '運動中的天體',
            },
            {
              href: '/junyi-science/junyi-middle-earth-science/nject',
              children: [],
              id: 'nject',
              title: '千變萬化的天氣',
            },
            {
              href: '/junyi-science/junyi-middle-earth-science/njeuf',
              children: [],
              id: 'njeuf',
              title: '全球變遷',
            },
            {
              href: '/junyi-science/junyi-middle-earth-science/njeax',
              children: [],
              id: 'njeax',
              title: '歷屆試題',
            },
          ],
          id: 'junyi-middle-earth-science',
          title: '國中地科',
        },
        {
          href: '/junyi-science/junyi-biology',
          children: [
            {
              href: '/junyi-science/junyi-biology/junyi-properties-of-life',
              children: [],
              id: 'junyi-properties-of-life',
              title: '生命的特性',
            },
            {
              href: '/junyi-science/junyi-biology/s5byc-c',
              children: [],
              id: 's5byc-c',
              title: '遺傳',
            },
            {
              href: '/junyi-science/junyi-biology/junyi-evolution',
              children: [],
              id: 'junyi-evolution',
              title: '演化與生物多樣性',
            },
            {
              href: '/junyi-science/junyi-biology/junyi-botany',
              children: [],
              id: 'junyi-botany',
              title: '植物學',
            },
            {
              href: '/junyi-science/junyi-biology/junyi-animal-physiology',
              children: [],
              id: 'junyi-animal-physiology',
              title: '動物生理學',
            },
            {
              href: '/junyi-science/junyi-biology/Biological-and-environment',
              children: [],
              id: 'Biological-and-environment',
              title: '生物與環境',
            },
            {
              href: '/junyi-science/junyi-biology/organization-ofplants-animal',
              children: [],
              id: 'organization-ofplants-animal',
              title: '動植物的組織',
            },
            {
              href:
                '/junyi-science/junyi-biology/cells-cell-structure-compound',
              children: [],
              id: 'cells-cell-structure-compound',
              title: '細胞化合物與細胞構造',
            },
            {
              href: '/junyi-science/junyi-biology/photosynthesis',
              children: [],
              id: 'photosynthesis',
              title: '光合作用',
            },
            {
              href: '/junyi-science/junyi-biology/respiration',
              children: [],
              id: 'respiration',
              title: '呼吸作用',
            },
          ],
          id: 'junyi-biology',
          title: '高中生物',
        },
        {
          href: '/junyi-science/junyi-physics',
          children: [
            {
              href: '/junyi-science/junyi-physics/s5p-99',
              children: [],
              id: 's5p-99',
              title: '99課綱',
            },
            {
              href: '/junyi-science/junyi-physics/s5p-108-6',
              children: [],
              id: 's5p-108-6',
              title: '108課綱',
            },
          ],
          id: 'junyi-physics',
          title: '高中物理',
        },
        {
          href: '/junyi-science/junyi-chemistry',
          children: [
            {
              href: '/junyi-science/junyi-chemistry/j-d-che-u1',
              children: [],
              id: 'j-d-che-u1',
              title: '【台達磨課師】基礎化學(必修化學)',
            },
            {
              href: '/junyi-science/junyi-chemistry/j-d-che-u3',
              children: [],
              id: 'j-d-che-u3',
              title: '【台達磨課師】綠野仙蹤-化學宅急便(必修化學)',
            },
            {
              href: '/junyi-science/junyi-chemistry/j-d-che-u2',
              children: [],
              id: 'j-d-che-u2',
              title: '【台達磨課師】科學素養-化學宅急便(選修化學)',
            },
            {
              href:
                '/junyi-science/junyi-chemistry/junyi-composition-of-matter',
              children: [],
              id: 'junyi-composition-of-matter',
              title: '【基礎化學】物質的組成',
            },
            {
              href: '/junyi-science/junyi-chemistry/junyi-atomic-structure',
              children: [],
              id: 'junyi-atomic-structure',
              title: '【基礎化學】原子結構與元素週期表',
            },
            {
              href: '/junyi-science/junyi-chemistry/junyi-chemical-reactions',
              children: [],
              id: 'junyi-chemical-reactions',
              title: '【基礎化學】化學反應',
            },
            {
              href: '/junyi-science/junyi-chemistry/v931-new-topic',
              children: [],
              id: 'v931-new-topic',
              title: '【高二化學】常見化學反應',
            },
            {
              href: '/junyi-science/junyi-chemistry/v931-new-topic-1',
              children: [],
              id: 'v931-new-topic-1',
              title: '【高二化學】有機化合物',
            },
            {
              href: '/junyi-science/junyi-chemistry/v931-new-topic-2',
              children: [],
              id: 'v931-new-topic-2',
              title: '【高二化學】化學與化工',
            },
            {
              href: '/junyi-science/junyi-chemistry/v931-new-topic-3',
              children: [],
              id: 'v931-new-topic-3',
              title: '【高二化學】反應速率',
            },
            {
              href: '/junyi-science/junyi-chemistry/v935-new-topic-8',
              children: [],
              id: 'v935-new-topic-8',
              title: '【高二化學】化學平衡',
            },
            {
              href: '/junyi-science/junyi-chemistry/v935-new-topic-9',
              children: [],
              id: 'v935-new-topic-9',
              title: '【高三化學】原子結構',
            },
            {
              href: '/junyi-science/junyi-chemistry/v932-new-topic',
              children: [],
              id: 'v932-new-topic',
              title: '【高三化學】化學鍵結',
            },
            {
              href: '/junyi-science/junyi-chemistry/v935-new-topic',
              children: [],
              id: 'v935-new-topic',
              title: '【高三化學】水溶液中酸、鹼、鹽的平衡',
            },
            {
              href: '/junyi-science/junyi-chemistry/v935-new-topic-1',
              children: [],
              id: 'v935-new-topic-1',
              title: '【高三化學】氧化還原反應',
            },
            {
              href: '/junyi-science/junyi-chemistry/v935-new-topic-2',
              children: [],
              id: 'v935-new-topic-2',
              title: '【高三化學】元素與無機化合物',
            },
            {
              href: '/junyi-science/junyi-chemistry/v926-new-topic-2',
              children: [],
              id: 'v926-new-topic-2',
              title: '看影片學化學',
            },
          ],
          id: 'junyi-chemistry',
          title: '高中化學',
        },
        {
          href: '/junyi-science/dr-go-high-school-earth-science',
          children: [],
          id: 'dr-go-high-school-earth-science',
          title: '高中地科',
        },
        {
          href: '/junyi-science/lis-science',
          children: [
            {
              href: '/junyi-science/lis-science/lis-chemistry',
              children: [],
              id: 'lis-chemistry',
              title: 'LIS自然系列－化學',
            },
            {
              href: '/junyi-science/lis-science/lis-physics',
              children: [],
              id: 'lis-physics',
              title: 'LIS自然系列－物理',
            },
            {
              href: '/junyi-science/lis-science/v1112-new-topic',
              children: [],
              id: 'v1112-new-topic',
              title: 'LIS自然系列－生物',
            },
            {
              href: '/junyi-science/lis-science/v1112-new-topic-1',
              children: [],
              id: 'v1112-new-topic-1',
              title: 'LIS自然系列－地球科學',
            },
            {
              href: '/junyi-science/lis-science/lis-exp',
              children: [],
              id: 'lis-exp',
              title: 'LIS實驗室',
            },
            {
              href: '/junyi-science/lis-science/v1177-new-topic',
              children: [],
              id: 'v1177-new-topic',
              title: 'LIS 超級英雄系列影片',
            },
          ],
          id: 'lis-science',
          title: 'LIS 情境科學教材',
        },
      ],
      id: 'junyi-science',
      title: '自然',
    },
    {
      href: '/junyi-chinese',
      children: [
        {
          href: '/junyi-chinese/ele-c',
          children: [
            {
              href: '/junyi-chinese/ele-c/coocele-c',
              children: [],
              id: 'coocele-c',
              title: '酷課雲專區',
            },
            {
              href: '/junyi-chinese/ele-c/adlele-c',
              children: [],
              id: 'adlele-c',
              title: '因材網專區',
            },
          ],
          id: 'ele-c',
          title: '國小',
        },
        {
          href: '/junyi-chinese/jun-c',
          children: [
            {
              href: '/junyi-chinese/jun-c/coocjun-c1',
              children: [],
              id: 'coocjun-c1',
              title: '七年級',
            },
            {
              href: '/junyi-chinese/jun-c/coocjun-c2',
              children: [],
              id: 'coocjun-c2',
              title: '八年級',
            },
            {
              href: '/junyi-chinese/jun-c/coocjun-c3',
              children: [],
              id: 'coocjun-c3',
              title: '九年級',
            },
          ],
          id: 'jun-c',
          title: '國中',
        },
        {
          href: '/junyi-chinese/sen-c',
          children: [
            {
              href: '/junyi-chinese/sen-c/coocsen-c1',
              children: [],
              id: 'coocsen-c1',
              title: '一年級',
            },
            {
              href: '/junyi-chinese/sen-c/coocsen-c2',
              children: [],
              id: 'coocsen-c2',
              title: '二年級',
            },
            {
              href: '/junyi-chinese/sen-c/coocsen-c3',
              children: [],
              id: 'coocsen-c3',
              title: '三年級',
            },
          ],
          id: 'sen-c',
          title: '高中',
        },
      ],
      id: 'junyi-chinese',
      title: '國語文',
    },
    {
      href: '/junyi-society',
      children: [
        {
          href: '/junyi-society/coocele-t',
          children: [
            {
              href: '/junyi-society/coocele-t/coocele-t3',
              children: [],
              id: 'coocele-t3',
              title: '三年級',
            },
            {
              href: '/junyi-society/coocele-t/coocele-t4',
              children: [],
              id: 'coocele-t4',
              title: '四年級',
            },
            {
              href: '/junyi-society/coocele-t/coocele-t5',
              children: [],
              id: 'coocele-t5',
              title: '五年級',
            },
            {
              href: '/junyi-society/coocele-t/coocele-t6',
              children: [],
              id: 'coocele-t6',
              title: '六年級',
            },
          ],
          id: 'coocele-t',
          title: '國小社會',
        },
        {
          href: '/junyi-society/middle-school-civics',
          children: [
            {
              href: '/junyi-society/middle-school-civics/new-topic-181',
              children: [],
              id: 'new-topic-181',
              title: '【七上】家庭與社區篇',
            },
            {
              href: '/junyi-society/middle-school-civics/h4csh',
              children: [],
              id: 'h4csh',
              title: '【七下】社會篇',
            },
            {
              href: '/junyi-society/middle-school-civics/h4czz',
              children: [],
              id: 'h4czz',
              title: '【八上】政治篇',
            },
            {
              href: '/junyi-society/middle-school-civics/h4cfl',
              children: [],
              id: 'h4cfl',
              title: '【八下】法律篇',
            },
            {
              href: '/junyi-society/middle-school-civics/h4cjj-',
              children: [],
              id: 'h4cjj-',
              title: '【九上】經濟篇',
            },
            {
              href: '/junyi-society/middle-school-civics/h4cqq',
              children: [],
              id: 'h4cqq',
              title: '【九下】全球篇',
            },
            {
              href: '/junyi-society/middle-school-civics/h4cjt',
              children: [],
              id: 'h4cjt',
              title: '基測會考試題',
            },
          ],
          id: 'middle-school-civics',
          title: '國中公民',
        },
        {
          href: '/junyi-society/junyi-geography',
          children: [
            {
              href: '/junyi-society/junyi-geography/geographical-skill',
              children: [],
              id: 'geographical-skill',
              title: '地理技術',
            },
            {
              href: '/junyi-society/junyi-geography/physical-geography',
              children: [],
              id: 'physical-geography',
              title: '自然地理',
            },
            {
              href: '/junyi-society/junyi-geography/human-geography',
              children: [],
              id: 'human-geography',
              title: '人文地理',
            },
            {
              href: '/junyi-society/junyi-geography/world-geography',
              children: [],
              id: 'world-geography',
              title: '世界地理',
            },
          ],
          id: 'junyi-geography',
          title: '高中地理',
        },
        {
          href: '/junyi-society/history',
          children: [
            {
              href: '/junyi-society/history/tw_oceans',
              children: [],
              id: 'tw_oceans',
              title: '大航海時代的台灣',
            },
            {
              href: '/junyi-society/history/tw_jp',
              children: [],
              id: 'tw_jp',
              title: '日本帝國時期的臺灣',
            },
            {
              href: '/junyi-society/history/tw_modern',
              children: [],
              id: 'tw_modern',
              title: '當代臺灣',
            },
          ],
          id: 'history',
          title: '歷史',
        },
        {
          href: '/junyi-society/coocind',
          children: [
            {
              href: '/junyi-society/coocind/coocind-song',
              children: [],
              id: 'coocind-song',
              title: '歌謠教學',
            },
            {
              href: '/junyi-society/coocind/coocind-animation',
              children: [],
              id: 'coocind-animation',
              title: '動畫',
            },
            {
              href: '/junyi-society/coocind/coocind-mv',
              children: [],
              id: 'coocind-mv',
              title: 'MV',
            },
          ],
          id: 'coocind',
          title: '原住民文化',
        },
      ],
      id: 'junyi-society',
      title: '社會',
    },
    {
      href: '/junyi-test',
      children: [
        {
          href: '/junyi-test/junyi-remedial',
          children: [
            {
              href: '/junyi-test/junyi-remedial/remedial-math-grade-3',
              children: [],
              id: 'remedial-math-grade-3',
              title: '數學 小三',
            },
            {
              href: '/junyi-test/junyi-remedial/remedial-math-grade-4',
              children: [],
              id: 'remedial-math-grade-4',
              title: '數學 小四',
            },
            {
              href: '/junyi-test/junyi-remedial/remedial-math-grade-5',
              children: [],
              id: 'remedial-math-grade-5',
              title: '數學 小五',
            },
            {
              href: '/junyi-test/junyi-remedial/remedial-math-grade-6',
              children: [],
              id: 'remedial-math-grade-6',
              title: '數學 小六',
            },
            {
              href: '/junyi-test/junyi-remedial/remedial-math-grade-7',
              children: [],
              id: 'remedial-math-grade-7',
              title: '數學 國一',
            },
            {
              href: '/junyi-test/junyi-remedial/remedial-math-grade-8',
              children: [],
              id: 'remedial-math-grade-8',
              title: '數學 國二',
            },
          ],
          id: 'junyi-remedial',
          title: '學習扶助複習測驗',
        },
        {
          href: '/junyi-test/hka-prep',
          children: [
            {
              href: '/junyi-test/hka-prep/hka',
              children: [],
              id: 'hka',
              title: '會考數學複習包',
            },
            {
              href: '/junyi-test/hka-prep/cool-jdchk',
              children: [],
              id: 'cool-jdchk',
              title: '酷課雲會考複習',
            },
            {
              href: '/junyi-test/hka-prep/jdchk',
              children: [],
              id: 'jdchk',
              title: '會考考古題與詳解',
            },
            {
              href: '/junyi-test/hka-prep/jdcjc',
              children: [],
              id: 'jdcjc',
              title: '基測考古題與詳解',
            },
          ],
          id: 'hka-prep',
          title: '國三會考複習',
        },
        {
          href: '/junyi-test/sdcxc-prep',
          children: [
            {
              href: '/junyi-test/sdcxc-prep/xcama',
              children: [],
              id: 'xcama',
              title: '學測主題式複習(數學)',
            },
            {
              href: '/junyi-test/sdcxc-prep/sdcxc-ma',
              children: [],
              id: 'sdcxc-ma',
              title: '高中學測數學',
            },
            {
              href: '/junyi-test/sdcxc-prep/sdcxc-na',
              children: [],
              id: 'sdcxc-na',
              title: '高中學測自然',
            },
          ],
          id: 'sdcxc-prep',
          title: '高三學測複習',
        },
        {
          href: '/junyi-test/sdczk-prep',
          children: [
            {
              href: '/junyi-test/sdczk-prep/cooc-sdczk',
              children: [],
              id: 'cooc-sdczk',
              title: '酷課雲指考複習',
            },
            {
              href: '/junyi-test/sdczk-prep/sdczk',
              children: [],
              id: 'sdczk',
              title: ' 指考考古題與詳解',
            },
          ],
          id: 'sdczk-prep',
          title: '高三指考複習',
        },
        {
          href: '/junyi-test/sdctc',
          children: [
            {
              href: '/junyi-test/sdctc/sdctc-ma103-1a',
              children: [],
              id: 'sdctc-ma103-1a',
              title: '高職統測數學 103',
            },
          ],
          id: 'sdctc',
          title: '高職統測',
        },
      ],
      id: 'junyi-test',
      title: '評量專區',
    },
    {
      href: '/junyi-teacher-resources',
      children: [
        {
          href: '/junyi-teacher-resources/tr01-guide',
          children: [
            {
              href: '/junyi-teacher-resources/tr01-guide/tr01-guide-covid',
              children: [],
              id: 'tr01-guide-covid',
              title: '防疫在家線上學習',
            },
            {
              href: '/junyi-teacher-resources/tr01-guide/tr01-guide-01',
              children: [],
              id: 'tr01-guide-01',
              title: '基礎操作',
            },
            {
              href: '/junyi-teacher-resources/tr01-guide/tr01-guide-02',
              children: [],
              id: 'tr01-guide-02',
              title: '進階功能',
            },
            {
              href: '/junyi-teacher-resources/tr01-guide/tr01-guide-03',
              children: [],
              id: 'tr01-guide-03',
              title: '學習扶助',
            },
            {
              href: '/junyi-teacher-resources/tr01-guide/tr01-guide-04',
              children: [],
              id: 'tr01-guide-04',
              title: '小測驗「你是均一平台達人嗎？」',
            },
          ],
          id: 'tr01-guide',
          title: '均一的操作指南',
        },
        {
          href: '/junyi-teacher-resources/tr02-application',
          children: [
            {
              href:
                '/junyi-teacher-resources/tr02-application/tr02-application-01',
              children: [],
              id: 'tr02-application-01',
              title: '在第一堂課開始之前',
            },
            {
              href:
                '/junyi-teacher-resources/tr02-application/tr02-application-02',
              children: [],
              id: 'tr02-application-02',
              title: '正式開始第一堂課',
            },
          ],
          id: 'tr02-application',
          title: '均一的應用方法',
        },
        {
          href: '/junyi-teacher-resources/tr03-practice',
          children: [
            {
              href: '/junyi-teacher-resources/tr03-practice/tr03-practice-01',
              children: [],
              id: 'tr03-practice-01',
              title: '公開課',
            },
            {
              href: '/junyi-teacher-resources/tr03-practice/tr03-practice-02',
              children: [],
              id: 'tr03-practice-02',
              title: '教師研習',
            },
          ],
          id: 'tr03-practice',
          title: '均一的課堂實踐',
        },
        {
          href: '/junyi-teacher-resources/tr08-afterschoolcareagency',
          children: [
            {
              href:
                '/junyi-teacher-resources/tr08-afterschoolcareagency/tr08-afterschoolcareagency-00',
              children: [],
              id: 'tr08-afterschoolcareagency-00',
              title: '認識磨數營',
            },
            {
              href:
                '/junyi-teacher-resources/tr08-afterschoolcareagency/tr08-afterschoolcareagency-01',
              children: [],
              id: 'tr08-afterschoolcareagency-01',
              title: '引起動機的學習活動',
            },
            {
              href:
                '/junyi-teacher-resources/tr08-afterschoolcareagency/tr08-afterschoolcareagency-02',
              children: [],
              id: 'tr08-afterschoolcareagency-02',
              title: '線上平台',
            },
            {
              href:
                '/junyi-teacher-resources/tr08-afterschoolcareagency/tr08-afterschoolcareagency-03',
              children: [],
              id: 'tr08-afterschoolcareagency-03',
              title: '班級經營',
            },
            {
              href:
                '/junyi-teacher-resources/tr08-afterschoolcareagency/tr08-afterschoolcareagency-04',
              children: [],
              id: 'tr08-afterschoolcareagency-04',
              title: '人的陪伴',
            },
          ],
          id: 'tr08-afterschoolcareagency',
          title: '課輔班老師的好幫手',
        },
        {
          href: '/junyi-teacher-resources/tr04-wsq',
          children: [
            {
              href: '/junyi-teacher-resources/tr04-wsq/tr04-wsq-01',
              children: [],
              id: 'tr04-wsq-01',
              title: 'WSQ 學習單是什麼 ?',
            },
            {
              href: '/junyi-teacher-resources/tr04-wsq/tr04-wsq-02',
              children: [],
              id: 'tr04-wsq-02',
              title: 'WSQ 學習單模板分享',
            },
            {
              href: '/junyi-teacher-resources/tr04-wsq/tr04-wsq-03',
              children: [],
              id: 'tr04-wsq-03',
              title: 'WSQ 學習單課堂實例',
            },
          ],
          id: 'tr04-wsq',
          title: '影片教學的好幫手 - WSQ 學習單',
        },
        {
          href: '/junyi-teacher-resources/tr05-share',
          children: [
            {
              href: '/junyi-teacher-resources/tr05-share/tr05-share-01',
              children: [],
              id: 'tr05-share-01',
              title: '現身說法',
            },
            {
              href: '/junyi-teacher-resources/tr05-share/tr05-share-02',
              children: [],
              id: 'tr05-share-02',
              title: '文字故事',
            },
          ],
          id: 'tr05-share',
          title: '親師生使用心得分享',
        },
        {
          href: '/junyi-teacher-resources/tr06-conference',
          children: [
            {
              href:
                '/junyi-teacher-resources/tr06-conference/tr06-conference-01',
              children: [],
              id: 'tr06-conference-01',
              title: '論壇文章',
            },
            {
              href:
                '/junyi-teacher-resources/tr06-conference/tr06-conference-02',
              children: [],
              id: 'tr06-conference-02',
              title: '演講影片',
            },
            {
              href:
                '/junyi-teacher-resources/tr06-conference/tr06-conference-03',
              children: [],
              id: 'tr06-conference-03',
              title: '課堂實例',
            },
          ],
          id: 'tr06-conference',
          title: '個人化學習論壇',
        },
        {
          href: '/junyi-teacher-resources/tr07-video',
          children: [
            {
              href: '/junyi-teacher-resources/tr07-video/tr07-video-01',
              children: [],
              id: 'tr07-video-01',
              title: '基礎篇',
            },
            {
              href: '/junyi-teacher-resources/tr07-video/tr07-video-02',
              children: [],
              id: 'tr07-video-02',
              title: '選修篇',
            },
            {
              href: '/junyi-teacher-resources/tr07-video/tr07-video-03',
              children: [],
              id: 'tr07-video-03',
              title: '進階篇',
            },
          ],
          id: 'tr07-video',
          title: '均一教學影片的錄製方法',
        },
      ],
      id: 'junyi-teacher-resources',
      title: '教師資源區',
    },
  ],
  id: 'root',
  title: '知識樹',
}

export default data
