/**
 * Copyright (c) 2021 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import { makeStyles } from '@material-ui/core/styles'

// custom types

// utils

// assets

// actions

// components
import Header from './Header'
import Footer from './Footer'

// self-defined-components
const useStyles = makeStyles({
  root: {
    minHeight: '100vh',
    display: 'grid',
    gridTemplateRows: 'auto 1fr auto',
  },
})

const LayoutBase = ({ children }: Props) => {
  const classes = useStyles()

  return (
    <div className={classes.root}>
      <Header />
      <div>{children}</div>
      <Footer />
    </div>
  )
}

type Props = {
  children: React.ReactNode
}

export default LayoutBase
