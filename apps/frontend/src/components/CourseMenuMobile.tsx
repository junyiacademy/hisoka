/**
 * Copyright (c) 2021 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import * as React from 'react'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'
import {
  Grid,
  Popover,
  List,
  ListItem,
  ListItemText,
  Collapse,
  Divider,
} from '@material-ui/core'
import ExpandLessIcon from '@material-ui/icons/ExpandLess'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

// custom types
import type { ITopicTreeNode } from '@hisoka/frontend/interfaces'

// utils

// assets

// hooks

// components

// self-defined-components

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    popoverPaper: {
      maxWidth: '100%',
    },
    listContainer: {
      width: '100vw',
      maxHeight: '506px',
    },
    listRoot: {
      width: '100%',
      backgroundColor: theme.palette.background.paper,
      padding: 0,
    },
    activeListItem: {
      background: '#4990E21A !important',
    },
    nestedFirstLayer: {
      paddingLeft: theme.spacing(4),
    },
    nestedSecondLayerContainer: {
      background: '#FAFAFA',
    },
    nestedSecondLayer: {
      paddingLeft: theme.spacing(8),
    },
    expandLessIcon: {
      color: '#4990E2',
    },
    expandMoreIcon: {
      color: '#00000061',
    },
  })
)

type Props = {
  data: ITopicTreeNode
  courseMenuAnchorEl: null | HTMLElement
  onCourseMenuClose: () => void
}

const CourseMenuMobile = ({
  data,
  courseMenuAnchorEl,
  onCourseMenuClose,
}: Props) => {
  const isCourseMenuOpen = Boolean(courseMenuAnchorEl)

  const classes = useStyles()
  const [selectedTopicIds, setSelectedTopicIds] = React.useState<string[]>([])

  const navigateToTopicPage = (node: ITopicTreeNode) => {
    console.log(`you will be redirect to topic page: ${node.title}`)
  }

  const handleClick = (node: ITopicTreeNode) => {
    if (selectedTopicIds.includes(node.id)) {
      setSelectedTopicIds((prev) =>
        prev.filter(
          (id) => id !== node.id && !node.children.map((x) => x.id).includes(id)
        )
      )
    } else {
      setSelectedTopicIds((prev) => [...prev, node.id])
    }
  }

  const handleCourseMenuClose = () => {
    onCourseMenuClose()
    setSelectedTopicIds([])
  }

  const getExpandableListItem = (node, isExpanded, listItemClassName) => (
    <ListItem
      button
      selected={isExpanded}
      classes={{
        root: listItemClassName,
        selected: classes.activeListItem,
      }}
      onClick={() => handleClick(node)}
    >
      <ListItemText primary={node.title} />
      {isExpanded ? (
        <ExpandLessIcon className={classes.expandLessIcon} />
      ) : (
        <ExpandMoreIcon className={classes.expandMoreIcon} />
      )}
    </ListItem>
  )

  /**
   * mobile menu is layer structure, so it has two collapse
   * <List>
   *   <ListItem>
   *   <Collapse>
   *     <ListItem>
   *     <Collapse>
   *       <ListItem> <- If we want to have the forth layer, put a collapse below it
   */
  return (
    <Popover
      id='course-menu-mobile'
      open={isCourseMenuOpen}
      anchorEl={courseMenuAnchorEl}
      onClose={handleCourseMenuClose}
      anchorReference='anchorPosition'
      anchorPosition={{ top: 56, left: 0 }}
      marginThreshold={0}
      classes={{
        paper: classes.popoverPaper,
      }}
    >
      <Grid container direction='row' className={classes.listContainer}>
        <List
          component='nav'
          aria-labelledby='nested-list-subheader'
          className={classes.listRoot}
        >
          {data.children.map((node) => {
            if (node.children.length === 0) {
              return (
                <ListItem
                  key={`listItem-${node.id}`}
                  button
                  onClick={() => navigateToTopicPage(node)}
                >
                  <ListItemText primary={node.title} />
                </ListItem>
              )
            }

            const isExpanded = selectedTopicIds.includes(node.id)
            return (
              <div key={`collapse-${node.id}`}>
                {getExpandableListItem(node, isExpanded, '')}
                <Collapse in={isExpanded} timeout='auto' unmountOnExit>
                  {node.children.map((node) => {
                    if (node.children.length === 0) {
                      return (
                        <ListItem
                          key={`listItem-${node.id}`}
                          button
                          className={classes.nestedFirstLayer}
                          onClick={() => navigateToTopicPage(node)}
                        >
                          <ListItemText primary={node.title} />
                        </ListItem>
                      )
                    }

                    const isExpanded = selectedTopicIds.includes(node.id)
                    return (
                      <div key={`collapse-${node.id}`}>
                        {getExpandableListItem(
                          node,
                          isExpanded,
                          classes.nestedFirstLayer
                        )}
                        <Collapse in={isExpanded} timeout='auto' unmountOnExit>
                          <List
                            component='div'
                            disablePadding
                            className={classes.nestedSecondLayerContainer}
                          >
                            {node.children.map((node) => (
                              <ListItem
                                key={`listItem-${node.id}`}
                                button
                                className={classes.nestedSecondLayer}
                                onClick={() => navigateToTopicPage(node)}
                              >
                                <ListItemText primary={node.title} />
                              </ListItem>
                            ))}
                          </List>
                        </Collapse>
                      </div>
                    )
                  })}
                  <Divider />
                </Collapse>
              </div>
            )
          })}
        </List>
      </Grid>
    </Popover>
  )
}

export default CourseMenuMobile
