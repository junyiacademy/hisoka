/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import { Grid } from '@material-ui/core'
import type { GridProps } from '@material-ui/core/Grid'

// custom types

// utils

// assets

// components

// self-defined-configs

// self-defined-components
const Container = (props: GridProps) => <Grid container {...props} />
const Item = (props: GridProps) => <Grid item {...props} />

export { Container, Item }
