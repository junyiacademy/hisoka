/**
 * Copyright (c) 2021 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'

// custom types
import type { IMenuConfig } from '@hisoka/frontend/interfaces'

// utils

// assets

// actions

// components
import BaseNextLink from './BaseNextLink'

// self-defined-components
const LeftMenuItems = ({ navItems }: Props) => (
  <>
    {navItems.map(({ route, title, icon }) => (
      <BaseNextLink href={route} key={`menu-item-${title}`}>
        <ListItem button>
          <ListItemIcon>{icon}</ListItemIcon>
          <ListItemText primary={title} />
        </ListItem>
      </BaseNextLink>
    ))}
  </>
)

type Props = {
  navItems: IMenuConfig[]
}

export default LeftMenuItems
