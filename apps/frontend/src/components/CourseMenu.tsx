/**
 * Copyright (c) 2021 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import * as React from 'react'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'
import {
  Grid,
  Popover,
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  Button,
} from '@material-ui/core'
import ChevronRightIcon from '@material-ui/icons/ChevronRight'

// custom types
import type { ITopicTreeNode } from '@hisoka/frontend/interfaces'

// utils

// assets

// hooks

// components

// self-defined-components
const useStyles = makeStyles<
  Theme,
  { menuHeight: number; showBigButton: boolean }
>((theme: Theme) =>
  createStyles({
    listsContainer: ({ menuHeight }) => ({
      height: `${menuHeight}px`,
      overflow: 'hidden',
      position: 'relative',
    }),
    bigButton: {
      position: 'absolute',
      right: theme.spacing(2),
      bottom: theme.spacing(2),
      background: '#4990E2',
      color: '#FFFFFF',
      width: '289px',

      '&:hover': {
        background: '#2a7ad6',
      },
    },
    list: ({ menuHeight, showBigButton }) => ({
      overflow: 'auto',
      height: `${menuHeight}px`,
      position: 'relative',
      width: '160px',
      paddingTop: 0,
      paddingBottom: 0,
      '&:not(:last-children)': {
        borderRight: '1px solid #EEEEEE',
        width: '161px', // to cover the 1px of right border
      },
      '&:not(:first-children)': {
        height: showBigButton ? `${menuHeight - 48 - 16}px` : `${menuHeight}px`,
      },
    }),
    listItem: {
      cursor: 'pointer',
      '&:hover': {
        background: '#FAFAFA',
      },
    },
    listActive: {
      background: '#4990E21A !important',
      color: '#4990E2',
    },
    listText: {
      whiteSpace: 'nowrap',
      textOverflow: 'ellipsis',
      overflow: 'hidden',
    },
    expandMoreIcon: {
      color: '#4990E2',
    },
    iconRoot: {
      height: theme.spacing(3),
      width: theme.spacing(3),
      cursor: 'pointer',
      pointerEvents: 'none',
    },
  })
)

type Props = {
  data: ITopicTreeNode
  courseMenuAnchorEl: null | HTMLElement
  onCourseMenuClose: () => void
}

const CourseMenu = ({ data, courseMenuAnchorEl, onCourseMenuClose }: Props) => {
  const [menu, setMenu] = React.useState<ITopicTreeNode[][]>([data.children])
  const [selectedTopicIds, setSelectedTopicIds] = React.useState<string[]>([])

  const menuHeight = Math.min(data.children.length * 48, 480)
  const isCourseMenuOpen = Boolean(courseMenuAnchorEl)
  const showBigButton = selectedTopicIds.length >= 1

  const classes = useStyles({
    menuHeight,
    showBigButton,
  })

  const handleClick = (node: ITopicTreeNode, listIdx: number): void => {
    if (node.children.length === 0) {
      setMenu((menu) => [...menu.slice(0, listIdx + 1)])
      setSelectedTopicIds((selectedTopicIds) => [
        ...selectedTopicIds.slice(0, listIdx),
      ])
      return
    }

    setMenu((menu) => [...menu.slice(0, listIdx + 1), node.children])

    setSelectedTopicIds((selectedTopicIds) => [
      ...selectedTopicIds.slice(0, listIdx),
      node.id,
    ])
  }

  return (
    <Popover
      id={'course-menu'}
      open={isCourseMenuOpen}
      anchorEl={courseMenuAnchorEl}
      onClose={onCourseMenuClose}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'left',
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'left',
      }}
    >
      <Grid container direction='row' className={classes.listsContainer}>
        {menu.map((list, listIdx) => (
          <List key={`list-${listIdx}`} className={classes.list}>
            {list.map((node) => (
              <ListItem
                key={node.id}
                className={classes.listItem}
                selected={selectedTopicIds.includes(node.id)}
                classes={{
                  selected: classes.listActive,
                }}
                onClick={() => handleClick(node, listIdx)}
              >
                <ListItemText
                  primary={node.title}
                  classes={{ primary: classes.listText }}
                />
                {node.children.length !== 0 && (
                  <ListItemSecondaryAction className={classes.iconRoot}>
                    <ChevronRightIcon
                      className={
                        selectedTopicIds.includes(node.id)
                          ? classes.expandMoreIcon
                          : ''
                      }
                    />
                  </ListItemSecondaryAction>
                )}
              </ListItem>
            ))}
          </List>
        ))}
        {menu.length === 2 && showBigButton && (
          <List className={classes.list} />
        )}
        {showBigButton && (
          <Button className={classes.bigButton}>前往數學主題頁面</Button>
        )}
      </Grid>
    </Popover>
  )
}

export default CourseMenu
