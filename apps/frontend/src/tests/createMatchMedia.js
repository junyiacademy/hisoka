import mediaQuery from 'css-mediaquery'

function createMatchMedia(width) {
  return (query) => ({
    matches: mediaQuery.match(query, { width }),
    addListener: () => {
      return
    },
    removeListener: () => {
      return
    },
  })
}

export { createMatchMedia }
