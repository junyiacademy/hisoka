/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { useMemo } from 'react'
import { configureStore, EnhancedStore } from '@reduxjs/toolkit'
import { createEpicMiddleware } from 'redux-observable'
import rootReducer from './rootReducer'
import rootEpic from './rootEpic'
import { get, post } from './dependencies'

let store: EnhancedStore | undefined

export const initStore = (initialState: Record<string, unknown>) => {
  const epicMiddleware = createEpicMiddleware({ dependencies: { get, post } })

  const store = configureStore({
    reducer: rootReducer,
    middleware: [epicMiddleware],
    preloadedState: initialState,
    enhancers: [],
  })

  epicMiddleware.run(rootEpic)

  return store
}

const initializeStore = (preloadedState: Record<string, unknown>) => {
  let _store = store ?? initStore(preloadedState)

  // After navigating to a page with an initial Redux state, merge that state
  // with the current state in the store, and create a new store
  if (preloadedState && store) {
    _store = initStore({
      ...store.getState(),
      ...preloadedState,
    })
    // Reset the current store
    store = undefined
  }

  // For SSG and SSR always create a new store
  if (typeof window === 'undefined') return _store
  // Create the store once in the client
  if (!store) store = _store

  return _store
}

export function useStore(initialState: Record<string, unknown>) {
  const store = useMemo(() => initializeStore(initialState), [initialState])
  return store
}
