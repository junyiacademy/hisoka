/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { combineReducers } from 'redux'

// reducers
const rootReducer = combineReducers({})

export type IRootState = ReturnType<typeof rootReducer>

export default rootReducer
