import { renderHook } from '@testing-library/react-hooks'
import { graphql } from 'msw'
import createReactQueryWrapper from '../../tests/createReactQueryWrapper'
import { server } from '../../tests/setupTests'

import useBadge from '../useBadge'

const mockData = 'badges data'

test('should get badges', async () => {
  server.use(
    graphql.query('BadgeMenu', (_req, res, ctx) => {
      return res(
        ctx.data({
          badgeMenu: mockData,
        })
      )
    })
  )

  const { result, waitFor } = renderHook(() => useBadge(), {
    wrapper: createReactQueryWrapper(),
  })

  await waitFor(() => result.current.isSuccess)

  expect(result.current.data).toEqual(mockData)
})

test('should get errors', async () => {
  server.use(
    graphql.query('BadgeMenu', (_req, res, ctx) => {
      return res(
        ctx.errors([
          {
            message: 'Oh oh, something went wrong! Q__Q',
          },
        ])
      )
    })
  )

  const { result, waitFor } = renderHook(() => useBadge(), {
    wrapper: createReactQueryWrapper(),
  })

  await waitFor(() => result.current.isError)

  expect(result.current.error).toBeDefined()
})
