import { gql } from 'graphql-request'
import { useQuery } from 'react-query'
import { graphQLClient } from '@hisoka/frontend/utils/graphQLClient'

export const permissionQueryKey = 'permission'

export const PERMISSION = gql`
  query Permission {
    me {
      verified
      roles
    }
  }
`

export const fetchPermission = async () => {
  // simulate the  network delay, free to be deleted
  await new Promise((resolve) => setTimeout(resolve, 1500))

  const response = await graphQLClient.request(PERMISSION)

  return response.me
}

export default function usePermission() {
  return useQuery(permissionQueryKey, fetchPermission, {
    retry: 0, // for development, delete it after well tested
  })
}
