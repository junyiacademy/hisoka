import { gql } from 'graphql-request'
import { useQuery } from 'react-query'
import { graphQLClient } from '@hisoka/frontend/utils/graphQLClient'

export const badgeMenuQueryKey = 'badgeMenu'

export const BADGE_MENU = gql`
  query BadgeMenu {
    badgeMenu {
      category
      categoryIcon
      badges {
        description
        icon
        isOwned
        safeExtendedDescription
      }
      typeLabel
      categoryDescription
      userBadges {
        description
        icon
        isOwned
        lastEarnedDate
        safeExtendedDescription
      }
    }
  }
`

export const fetchBadgeMenu = () =>
  graphQLClient.request(BADGE_MENU).then((x) => x.badgeMenu)

export default function useBadge() {
  return useQuery(badgeMenuQueryKey, fetchBadgeMenu)
}
