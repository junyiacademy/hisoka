import { gql } from 'graphql-request'
import { useRouter } from 'next/router'
import { useMutation } from 'react-query'
import { graphQLClient } from '@hisoka/frontend/utils/graphQLClient'

export const SIGNIN = gql`
  mutation Signin($input: SigninInput!) {
    signin(signinInput: $input) {
      token
      user {
        roles
      }
    }
  }
`

export const mutateSignin = async (input) => {
  const response = await graphQLClient.request(SIGNIN, {
    input,
  })

  return response.signin
}

export default function useSignin() {
  const router = useRouter()

  return useMutation(mutateSignin, {
    onSuccess: () => router.push('/'),
  })
}
