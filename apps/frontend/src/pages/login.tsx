/**
 * Copyright (c) 2021 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import { LayoutBase, Login } from '@hisoka/frontend/components'

function LoginPage() {
  return (
    <LayoutBase>
      <Login />
    </LayoutBase>
  )
}

export default LoginPage
