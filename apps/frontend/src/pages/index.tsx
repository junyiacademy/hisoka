/**
 * Copyright (c) 2021 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import * as React from 'react'
import Link from 'next/link'
import { Typography } from '@material-ui/core'
import { LayoutBase } from '@hisoka/frontend/components'

function Index() {
  return (
    <LayoutBase>
      <Typography component='h1' variant='h3'>
        Everyone can access our Homepage
      </Typography>
      <Link href='/dashboard'>Go to Dashboard</Link>
    </LayoutBase>
  )
}

export default Index
