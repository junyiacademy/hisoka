/**
 * Copyright (c) 2020 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import { LayoutStudent } from '@hisoka/frontend/components'
import { BadgeMenu } from '@hisoka/frontend/components/badgeMenu'

function Badge() {
  return (
    <LayoutStudent>
      <BadgeMenu />
    </LayoutStudent>
  )
}

export default Badge
