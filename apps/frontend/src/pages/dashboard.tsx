/**
 * Copyright (c) 2021 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import { Typography } from '@material-ui/core'
import { LayoutBase } from '@hisoka/frontend/components'
import { requireMember } from '@hisoka/frontend/hocs'

function Dashboard() {
  return (
    <LayoutBase>
      <Typography component='h1' variant='h3'>
        Welcome to Dashboard
      </Typography>
    </LayoutBase>
  )
}

export default requireMember(Dashboard)
