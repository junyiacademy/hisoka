export interface IMenuConfig {
  route: string
  title: string
  icon: React.ReactNode
}
export interface ITopicTreeNode {
  title: string
  id: string
  href: string
  children: ITopicTreeNode[]
}
