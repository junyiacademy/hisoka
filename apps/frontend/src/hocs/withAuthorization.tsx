/**
 * Copyright (c) 2021 Junyi Academy.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import * as React from 'react'
import Link from 'next/link'
import { Typography } from '@material-ui/core'
import { Role } from '@hisoka/shared/util/graphql-typings'
import { usePermission } from '@hisoka/frontend/hooks'
import { LayoutBase } from '@hisoka/frontend/components'

function authHOCFactory(allowedRole: string) {
  return function withAuthorization(WrappedComponent: React.ComponentType) {
    return () => {
      const { data, isLoading } = usePermission()

      if (isLoading && !data) {
        return (
          <LayoutBase>
            <Typography component='h1' variant='h3'>
              Checking Your Permission...
            </Typography>
          </LayoutBase>
        )
      }

      if (!data?.roles?.includes(allowedRole)) {
        return (
          <LayoutBase>
            <Typography component='h1' variant='h3'>
              You don't have permission for this page Q_Q
            </Typography>
            <Link href='/login'>click here to Login</Link>
          </LayoutBase>
        )
      }

      return <WrappedComponent />
    }
  }
}

export const requireAdmin = authHOCFactory(Role.ADMIN)
export const requireMember = authHOCFactory(Role.MEMBER)
