module.exports = {
  displayName: 'frontend',
  preset: '../../jest.preset.js',
  transform: {
    '^(?!.*\\.(js|jsx|ts|tsx|css|json)$)': '@nrwl/react/plugins/jest',
    '^.+\\.[tj]sx?$': 'babel-jest',
  },
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx'],
  coverageDirectory: '../../coverage/apps/frontend',
  setupFilesAfterEnv: ['./src/tests/setupTests.js'],
  coverageThreshold: {
    global: {
      branches: 64,
      functions: 75,
      lines: 80,
      statements: 79,
    },
  },
  reporters: [
    'default',
    [
      'jest-junit',
      {
        outputName: './coverage/apps/frontend/junit-jest.xml',
      },
    ],
  ],
}
