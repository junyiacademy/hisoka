<div align="center">
<h1>Junyi Academy Frontend</h1>

<a href="https://junyiacademy.org">
  <img
    width="300px"
    alt="junyi academy logo"
    src="https://www.junyiacademy.org/images/logo_256.png"
  />
</a>

<p>Junyi Academy is a nonprofit organization. This project is our open source project for Frontend.</p>

<br />
</div>

<hr />

<!-- prettier-ignore-start -->
<!-- ALL-CONTRIBUTORS-BADGE:START - Do not remove or modify this section -->
[![All Contributors](https://img.shields.io/badge/all_contributors-6-orange.svg?style=flat-square)](#contributors-)
<!-- ALL-CONTRIBUTORS-BADGE:END -->
<!-- prettier-ignore-end -->

## Projects 🚀

- [Separation of frontend and backend](https://gitlab.com/junyiacademy/hisoka/-/issues/2)

## Documentation 📖

- [Folders][folders]
- [Style Guide][style-guide]
- [Tutorial - Container][tutorial-container]
- [Tutorial - Epic][tutorial-epic]
- [Tutorial - React Query][tutorial-react-query]
- [Tutorial - Slice][tutorial-slice]
- [Tutorial - Store][tutorial-store]
- [Tutorial - Type Guard][tutorial-type-guard]
- [Tutorial - Create Module][tutorial-create-module]
- [Tutorial - Generate Typings][tutorial-generate-typings]

## Contributors ✨

Thanks goes to these wonderful people ([emoji key][emoji-key]):

<!-- ALL-CONTRIBUTORS-LIST:START - Do not remove or modify this section -->
<!-- prettier-ignore-start -->
<!-- markdownlint-disable -->
<table>
  <tr>
    <td align="center"><a href="https://gitlab.com/leo.lin1"><img src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/6003090/avatar.png?s=100" width="100px;" alt=""/><br /><sub><b>林暐唐</b></sub></a><br /><a href="https://gitlab.com/junyiacademy/hisoka/commits/master" title="Code">💻</a> <a href="https://gitlab.com/junyiacademy/hisoka/commits/master" title="Documentation">📖</a> <a href="#infra-leo.lin1" title="Infrastructure (Hosting, Build-Tools, etc)">🚇</a> <a href="https://gitlab.com/junyiacademy/hisoka/merge_requests?scope=all&state=all&approver_usernames[]=leo.lin1" title="Reviewed Pull Requests">👀</a> <a href="https://gitlab.com/junyiacademy/hisoka/commits/master" title="Tests">⚠️</a></td>
    <td align="center"><a href="https://gitlab.com/yisheng.cpbr"><img src="https://gitlab.com/uploads/-/system/user/avatar/1708340/avatar.png?s=100" width="100px;" alt=""/><br /><sub><b>江宜陞</b></sub></a><br /><a href="https://gitlab.com/junyiacademy/hisoka/commits/master" title="Documentation">📖</a> <a href="https://gitlab.com/junyiacademy/hisoka/merge_requests?scope=all&state=all&approver_usernames[]=yisheng.cpbr" title="Reviewed Pull Requests">👀</a></td>
    <td align="center"><a href="https://gitlab.com/belong61"><img src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/5256063/avatar.png?s=100" width="100px;" alt=""/><br /><sub><b>YuJen.Chiu</b></sub></a><br /><a href="https://gitlab.com/junyiacademy/hisoka/commits/master" title="Code">💻</a> <a href="https://gitlab.com/junyiacademy/hisoka/commits/master" title="Tests">⚠️</a></td>
    <td align="center"><a href="https://gitlab.com/PJCHENder"><img src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/762437/avatar.png?s=100" width="100px;" alt=""/><br /><sub><b>pjchender</b></sub></a><br /><a href="https://gitlab.com/junyiacademy/hisoka/merge_requests?scope=all&state=all&approver_usernames[]=PJCHENder" title="Reviewed Pull Requests">👀</a></td>
    <td align="center"><a href="https://gitlab.com/ivanchiou"><img src="https://secure.gravatar.com/avatar/d98b091182275d36f918499cf91931e9?s=80&d=identicon?s=100" width="100px;" alt=""/><br /><sub><b>ivan chiou</b></sub></a><br /><a href="https://gitlab.com/junyiacademy/hisoka/commits/master" title="Code">💻</a></td>
    <td align="center"><a href="https://gitlab.com/yining.chen"><img src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/6955526/avatar.png?s=100" width="100px;" alt=""/><br /><sub><b>陳逸寧</b></sub></a><br /><a href="https://gitlab.com/junyiacademy/hisoka/commits/master" title="Code">💻</a> <a href="https://gitlab.com/junyiacademy/hisoka/commits/master" title="Tests">⚠️</a></td>
  </tr>
</table>

<!-- markdownlint-restore -->
<!-- prettier-ignore-end -->

<!-- ALL-CONTRIBUTORS-LIST:END -->

This project follows the [all-contributors][all-contributors] specification.
Contributions of any kind welcome!

## LICENSE

[MIT][license]

<!-- prettier-ignore-start -->
[license-badge]: https://img.shields.io/badge/license-MIT-green?style=flat-square
[license]: https://gitlab.com/junyiacademy/hisoka/-/blob/master/LICENSE
[emoji-key]: https://allcontributors.org/docs/en/emoji-key
[all-contributors]: https://github.com/all-contributors/all-contributors
[folders]: https://gitlab.com/junyiacademy/hisoka/-/blob/staging/apps/frontend/docs/FOLDERS.md
[style-guide]: https://gitlab.com/junyiacademy/hisoka/-/blob/staging/apps/frontend/docs/STYLE_GUIDE.md
[tutorial-container]: https://gitlab.com/junyiacademy/hisoka/-/blob/staging/apps/frontend/docs/TUTORIAL_CONTAINER.md
[tutorial-epic]: https://gitlab.com/junyiacademy/hisoka/-/blob/staging/apps/frontend/docs/TUTORIAL_EPIC.md
[tutorial-react-query]: https://gitlab.com/junyiacademy/hisoka/-/blob/staging/apps/frontend/docs/TUTORIAL_REACT_QUERY.md
[tutorial-slice]: https://gitlab.com/junyiacademy/hisoka/-/blob/staging/apps/frontend/docs/TUTORIAL_SLICE.md
[tutorial-store]: https://gitlab.com/junyiacademy/hisoka/-/blob/staging/apps/frontend/docs/TUTORIAL_STORE.md
[tutorial-type-guard]: https://gitlab.com/junyiacademy/hisoka/-/blob/staging/apps/frontend/docs/TUTORIAL_TYPE_GUARD.md
[tutorial-create-module]: https://gitlab.com/junyiacademy/hisoka/-/blob/staging/apps/backend/docs/TUTORIAL_CREATE_MODULE.md
[tutorial-generate-typings]: https://gitlab.com/junyiacademy/hisoka/-/blob/staging/apps/backend/docs/TUTORIAL_GENERATE_TYPINGS.md

<!-- prettier-ignore-end -->
