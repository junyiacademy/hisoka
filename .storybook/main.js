// Workaround: refrenced from https://mui.com/material-ui/guides/migration-v4/#storybook-emotion-with-v5
const path = require('path');
const toPath = (filePath) => path.join(process.cwd(), filePath);
// Workaround: refrenced from https://mui.com/material-ui/guides/migration-v4/#storybook-emotion-with-v5

module.exports = {
  stories: [],
  addons: ['@storybook/addon-essentials', '@storybook/addon-controls'],
  // Workaround: refrenced from https://mui.com/material-ui/guides/migration-v4/#storybook-emotion-with-v5
  webpackFinal: async (config) => {
    return {
      ...config,
      resolve: {
        ...config.resolve,
        alias: {
          ...config.resolve.alias,
          '@emotion/core': toPath('libs/ui/node_modules/@emotion/react'),
          'emotion-theming': toPath('libs/ui/node_modules/@emotion/react'),
        },
      },
    };
  },
  // Workaround: refrenced from https://mui.com/material-ui/guides/migration-v4/#storybook-emotion-with-v5
};
